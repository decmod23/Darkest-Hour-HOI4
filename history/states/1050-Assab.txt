state={
	id=1050
	name="STATE_1050"
	
	history={
		owner = ITA
		victory_points = {
		    13439 1
		}
		1942.11.22 = {
			owner = ITA
			controller = ENG

		}
		1944.6.20 = {
			owner = ENG
			controller = ENG

		}
		1953.3.7 = {
			owner = ETH
			controller = ETH

		}
		2000.1.1 = {
			owner = ERI
			controller = ERI

		}
		buildings = {
			infrastructure = 3

		}
	    add_core_of = ERI

	}
	
	provinces={
		8043 13439 
	}
	manpower=398073
	buildings_max_level_factor=1.000
	state_category=pastoral
}
