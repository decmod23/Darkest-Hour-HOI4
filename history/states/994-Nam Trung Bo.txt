
state={
	id=994
	name="STATE_994"
	resources={
		rubber=20.000
	}

	history={
		owner = VIN
		add_core_of = VIN
		buildings = {
			infrastructure = 2

		}
		victory_points = {
			10309 4 
		}
		victory_points = {
			4379 3 
		}
		victory_points = {
			10162 3
		}
		victory_points = {
			4334 2
		}
		victory_points = {
			1285 2
		}
		1942.11.22 = {
			owner = JAP
			controller = JAP

		}
		1946.1.1 = {
			owner = VIN
			controller = VIN

		}
		1980.1.1 = {
			owner = VIE
			controller = VIE

		}

	}

	provinces={
		1285 1300 4255 4334 4379 4405 7229 10137 10162 10180 10309 12204 13295 
	}
	manpower=1622000
	buildings_max_level_factor=1.000
	state_category=rural
}
