
state={
	id=868
	name="STATE_868"

	history={
		owner = CHI
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 5
			dockyard = 1
			7014 = {
				naval_base = 6
				coastal_bunker = 1
				bunker = 1
			}

		}
		victory_points = {
			7014 10 
		}
		1944.6.20 = {
			controller = JAP
		}
		1946.1.1 = {
			owner = CHI
			controller = CHI
		}
		1950.1.1 = {
			owner = PRC
		}
	}

	provinces={
		7014 12052 12076 10034
	}
	manpower=17320000
	buildings_max_level_factor=1.000
	state_category=large_city
}
