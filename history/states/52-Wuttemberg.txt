
state={
	id=52
	name="STATE_52"
	resources={
		aluminium=18.000
	}

	history={
		owner = GER
		victory_points = {
			692 30 
		}
		victory_points = {
			708 3
		}
		buildings = {
			infrastructure = 7
			arms_factory = 2
			industrial_complex = 1
			air_base = 6

		}
		add_core_of = GER
		1939.1.1 = {
			buildings = {
				air_base = 10

			}
		}
		1946.1.1 = {
			owner = USA
			controller = USA
		}
		1950.1.1 = {
			owner = WGR
		}		
	}

	provinces={
		692 707 708 3688 3690 3705 6540 6693 9652 9666 11620 11638 11653 
	}
	manpower=2979413
	buildings_max_level_factor=1.000
	state_category=city
}
