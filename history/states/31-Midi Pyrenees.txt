
state={
	id=31
	name="STATE_31"

	history={
		owner = FRA
		victory_points = {
			6766 5 
		}
		victory_points = {
			9853 2 
		}
		victory_points = {
			11702 1 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 1
			industrial_complex = 1
			air_base = 2

		}
		add_core_of = FRA
		1941.6.22 = {
			owner = VIC
			controller = VIC
			add_core_of = VIC

		}
		1943.7.26 = {
			owner = FRA
			controller = GER

		}
		1945.1.1 = {
			owner = FRA
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		929 943 3740 3749 3915 3946 3948 6759 6766 6915 6917 6931 9853 9882 9884 11697 11699 11702 12169 
	}
	manpower=1825000
	buildings_max_level_factor=1.000
	state_category=city
}
