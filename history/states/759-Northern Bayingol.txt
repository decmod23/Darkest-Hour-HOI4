
state={
	id=759
	name="STATE_759"

	history={
		1933.1.1 = {
			owner = SIK
			add_core_of = SIK
			add_core_of = CHI
			add_core_of = PRC
			TNG = {
				set_province_controller = 12524
				set_province_controller = 4947
				set_province_controller = 1698
			}
		}
		1936.1.1 = {
			owner = SIK
		}
		1950.1.1 = {
			owner = PRC
		}		
		buildings = {
			infrastructure = 1
		}
		victory_points = {
			5736 1 
		}
	}

	provinces={
		1698 4709 4947 5736 7792 10545 10614 12524
	}
	manpower=330810
	buildings_max_level_factor=1.000
	state_category=wasteland
}
