state={
	id=960
	name="STATE_960"
	
	history={
		owner = HOL
		add_core_of = HOL
		victory_points = {
			391 10
		}
		victory_points = {
			6241 5
		}
		victory_points = {
			11456 5
		}
		victory_points = {
			6286 3
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 2
		}
		1940.5.10 = {
			GER = {
				set_province_controller = 6241
				set_province_controller = 9363
				set_province_controller = 9403
				set_province_controller = 6286

			}

		}
		1941.6.22 = {
			owner = HOL
			controller = GER

		}
		1946.1.1 = {
			owner = HOL
			controller = HOL

		}

	}
	
	provinces={
		391 6241 6286 9363 9403 11456 
	}
	manpower=1464000
	buildings_max_level_factor=1.000
	state_category=city
}
