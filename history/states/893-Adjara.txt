state={
	id=893
	name="STATE_893"
	
	history={
		owner = SOV
		buildings = {
			infrastructure = 2
			3653 = {
				naval_base = 2
				coastal_bunker = 1

			}

		}
		victory_points = {
			3653 3 
		}
		add_core_of = SOV
		add_core_of = GEO
		2000.1.1 = {
			owner = GEO
		}
	}
	
	provinces={
		3653 
	}
	manpower=26000
	buildings_max_level_factor=1.000
	state_category=pastoral
}
