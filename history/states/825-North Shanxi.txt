state={
	id=825
	name="STATE_825"

	resources = {
		steel = 14
	}
	
	history={
		owner = SHX
		add_core_of = SHX
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 2
			industrial_complex = 1
		}
		victory_points = {
			12432 1 
		}
		1937.1.1 = {
			controller = JAP
			add_core_of = MEN
		}
		1940.1.1 = {
			controller = MEN
			add_core_of = MEN
		}
		1946.1.1 = {
			owner = SHX
			remove_core_of = MEN
		}
		1950.1.1 = {
			owner = PRC
			remove_core_of = SHX
		}		
	}
	
	provinces={
		7158 10480 12432 
	}
	manpower=1550000
	buildings_max_level_factor=1.000
	state_category=town
}
