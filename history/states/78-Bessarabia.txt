state={
	id=78
	name="STATE_78"

	history={
		owner = ROM
		buildings = {
			infrastructure = 4
			industrial_complex = 2
			air_base = 2

		}
		add_core_of = ROM
		add_core_of = MOL
		1944.6.20 = {
			SOV = {
				set_province_controller = 78
				set_province_controller = 3577
				set_province_controller = 6600
				set_province_controller = 565
				set_province_controller = 414
			}
		}
		1946.1.1 = {
			owner = SOV
			add_core_of = SOV
		}
		2000.1.1 = {
			owner = MOL
			remove_core_of = SOV
		}	
	}

	provinces={
		414 565 3577 3707 3724 6600 6743 11686 11705 
	}
	manpower=2470970
	buildings_max_level_factor=1.000
	state_category=rural
}
