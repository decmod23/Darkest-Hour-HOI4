state={
	id=754
	name="STATE_754"
	provinces={
		1882 4313 4536 4888 7270 10490 10817 11448 12732 12815 
	}
	manpower=400000
	buildings_max_level_factor=1.000
	state_category = rural
	history={
		owner = XSM
		add_core_of = XSM
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			arms_factory = 1

		}
		victory_points = {
			10490 1 
		}
		1950.1.1 = {
			owner = PRC
		}		
	}
}
