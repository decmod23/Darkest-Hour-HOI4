
state={
	id=60
	name="STATE_60"
	resources={
		steel=20.000
		chromium=4.000
		tungsten=4.000
	}

	history={
		owner = GER
		victory_points = {
			6524 5 
		}
		victory_points = {
			538 2
		}
		buildings = {
			infrastructure = 7
			arms_factory = 1
			industrial_complex = 1

		}
		add_core_of = GER
		1939.1.1 = {
			buildings = {
				synthetic_refinery = 3
				arms_factory = 2

			}

		}
		1946.1.1 = {
			owner = SOV

		}
		1950.1.1 = {
			owner = DDR

		}
		1991.1.1 = {
			owner = WGR

		}

	}

	provinces={
		425 482 538 3500 3561 6501 6524 9497 
	}
	manpower=2765214
	buildings_max_level_factor=1.000
	state_category=large_city
}
