
state={
	id=1047
	name="STATE_1047"
	
	history={
		owner = FRA
		victory_points = {
		    7077 2
		}
		victory_points = {
		    11935 1
		}
		buildings = {
			infrastructure = 3

		}
		add_core_of = TUN
		1941.6.20 = {
			owner = VIC
			controller = VIC

		}
		1942.11.22 = {
			owner = FRA
			controller = ITA

		}
		1943.7.26 = {
			owner = FRA
			controller = FRA

		}
		1960.1.1 = {
			owner = TUN
			controller = TUN

		}

	}
	
	provinces={
		1037 1046 4061 4129 4163 7077 9994 11935 13436 13437 
	}
	manpower=570559
	buildings_max_level_factor=1.000
	state_category=rural
}
