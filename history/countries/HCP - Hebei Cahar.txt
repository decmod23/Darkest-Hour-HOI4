﻿capital = 608

oob = "HCP_1936"

# Starting tech
set_technology = {
	Small_Arms_1916 = 1
	Mass_Assault = 1
}

add_ideas = {
}
set_politics = {
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	fascist = 0
	authoritarian = 100
	democratic = 0
	socialist = 0
	communist = 0
}
create_country_leader = {
	name = "Song Zheyuan"
	desc = ""
	picture = "Heibei_Cahar_A_Song_Zheyuan.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}