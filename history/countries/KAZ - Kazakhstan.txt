﻿capital = 586 #Almaty

oob = "KAZ_1936"

set_research_slots = 3

# Starting tech
set_technology = {
	Small_Arms_1916 = 1
	Small_Arms_1935 = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_military_police = 1
	tech_mountaineers = 1
	truck_1936 = 1
	paratroopers = 1
	gw_artillery = 1
	
	  # PLACEHOLDER
	#  # PLACEHOLDER
	# # PLACEHOLDER
	Fighter_1933 = 1
	Fighter_1936 = 1
	Tactical_Bomber_1933 = 1
	Strategic_Bomber_1933 = 1
	Strategic_Bomber_1936 = 1
	Naval_Bomber_1936 = 1

	transport = 1
	Mass_Assault = 1
	fleet_in_being = 1
}

set_politics = {
	ruling_party = communist
	last_election = "1932.11.8"
	election_frequency = 48
	elections_allowed = no	
}
set_popularities = {
	fascist = 0
	authoritarian = 0
	democratic = 40
	socialist = 0
	communist = 60
}
create_country_leader = {
	name = "Nikolay Aleksandrovich Skvortsov"
	desc = ""
	picture = ""
	expire = "1953.3.1"
	ideology = marxism_leninism
	traits = {
		
	}
}

create_country_leader = {
	name = "Ilyas Zhansugurov"
	desc = ""
	picture = ""
	expire = "1953.3.1"
	ideology = social_conservatism
	traits = {
		
	}
}




