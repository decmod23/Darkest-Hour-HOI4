﻿capital = 615

oob = "SHX_1936"

# Starting tech
set_technology = {
	Small_Arms_1916 = 1
}
add_ideas = {
}

set_politics = {
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	fascist = 0
	authoritarian = 100
	democratic = 0
	socialist = 0
	communist = 0
}
create_country_leader = {
	name = "Yan Xishan"
	desc = ""
	picture = "P_A_Yan_Xishan.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Yan Xishan"
	picture = "P_A_Yan_Xishan.tga"
	traits = { defensive_doctrine }
	skill = 4
	attack_skill = 3
	defense_skill = 4
	planning_skill = 2
	logistics_skill = 3
}
create_corps_commander = {
	name = "Fu Zuoyi"
	picture = "M_Fu_Zuoyi.tga"
	traits = { offensive_doctrine }
	skill = 3
	attack_skill = 3
	defense_skill = 4
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Xu Yongchang"
	picture = "Xu_Yongchang.tga"
	traits = { }
	skill = 2
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Yang Aiyuan"
	picture = "Yang_Aiyuan.tga"
	traits = { }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 2
}

create_corps_commander = {
	name = "Shang Zhen"
	picture = "M_Shang_Zhen.tga"
	traits = { }
	skill = 1
	attack_skill = 3
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}






























