﻿division_template = {
	name = "Infantry Division"				# Infantry Division
    division_names_group = SAF_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
}

division_template = {
	name = "Cavalry Division"               # Cavalry Division
    division_names_group = SAF_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
}

units = {
    ##### South African Army #####
    division = { # 1st South African Infantry Division
        division_name = {
            is_name_ordered = yes
            name_order = 1
        }
        location = 10400 # Johannesburg (Praetoria)
        division_template = "Infantry Division"
        start_experience_factor = 0.1
        start_equipment_factor = 0.05
    }

    division = { # 2nd South African Infantry Division
        division_name = {
            is_name_ordered = yes
            name_order = 2
        }
        location = 10400 # Johannesburg (Praetoria)
        division_template = "Infantry Division"
        start_experience_factor = 0.1
        start_equipment_factor = 0.05
    }

    division = { # 3rd South African Infantry Division
        division_name = {
            is_name_ordered = yes
            name_order = 3
        }
        location = 10400 # Johannesburg (Praetoria)
        division_template = "Infantry Division"
        start_experience_factor = 0.1
        start_equipment_factor = 0.05
    }

    division = {
        name = "1st Mounted Commando Division"
        location = 10400 # Johannesburg (Praetoria)
        division_template = "Cavalry Division"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
    }
}
### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1900
			creator = "SAF"
		}
		requested_factories = 1
		progress = 0.14
		efficiency = 100
	}
}