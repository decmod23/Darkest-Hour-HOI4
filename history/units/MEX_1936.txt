﻿division_template = {
	name = "División de Infanteria"			# Infantry Division
	division_names_group = SPAN_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
}

division_template = {
	name = "División de infantería con artillería"			# Infantry Division with Artillery
	division_names_group = SPAN_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
		artillery_brigade = { x = 2 y = 2 }
	}
}

division_template = {
	name = "División de Caballería"       	# Cavalry Division
	division_names_group = SPAN_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
	}
}

units = {
	##### Ejercito de Mexico #####
	division = {
		name = "Guardia del Presidente"
		location = 1965 # Mexico City 
		division_template = "División de Infanteria"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}

	division = { # I División de Infantería
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 1965 # Mexico City
		division_template = "División de Infanteria"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5 
	}

	division = { # II División de Infantería
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 1965 # Mexico City
		division_template = "División de Infanteria"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5 
	}

	division = { # III División de Infantería
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 1965 # Mexico City
		division_template = "División de infantería con artillería"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5 
	}
	##### Caballería de Mexico #####
	division = { # I División de Caballería
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 12835 # Saltillo (Monterrey)
		division_template = "División de Caballería"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # II División de Caballería
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 12835 # Saltillo (Monterrey)
		division_template = "División de Caballería"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	##### Naval OOB #####
	fleet = {
		name = "Armada de la Republica Mexicana"
		naval_base = 4965 # Yucatan
		task_force = {				
			name = "Armada de la Republica Mexicana"
			location = 4965 # Yucatan
			ship = { name = "ARM Anahuac" definition = heavy_cruiser equipment = { CA_equipment_1906 = { amount = 1 owner = MEX } } }
		}
	}				
}


air_wings = {
	### Fuerza Aérea Mexicana -- Mexico City
	277 = {
		# 1a Grupo Aéreo de Caza
		Fighter_equipment_1933 = { # Ansaldo A.1 Balilla
			owner = "MEX"
			amount = 35
		}
	}
}
### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1936
			creator = "MEX"
		}
		requested_factories = 2
		progress = 0.28
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "MEX"
		}
		requested_factories = 2
		progress = 0.22
		efficiency = 100
	}
}