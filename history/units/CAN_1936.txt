﻿division_template = {
	name = "Infantry Division"
	division_names_group = CAN_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
}
division_template = {
	name = "Cavalry Militia"  
	# Note: all forces at reserve levels in 1936

	division_names_group = CAN_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Armored-Division"

	division_names_group = CAN_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
}


units = {
	##### Canadian Army #####
	division = { # 1st Canadian Infantry Division
		division_name = {    
			is_name_ordered = yes
			name_order = 1
		}
		location = 3778 # Ottawa
		division_template = "Infantry Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.25
	}

	division = { # 2nd Canadian Infantry Division
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 3778 # Ottawa
		division_template = "Infantry Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}

	##### Naval OOB #####
	fleet = {
		name = "Royal Canadian Navy"
		naval_base = 7361 # Halifax
		task_force = {
			name = "Royal Canadian Navy"
			location = 7361 # Halifax
			ship = { name = "Destroyer Flotilla 1" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = CAN } } }
			ship = { name = "Destroyer Flotilla 2" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = CAN } } }
			ship = { name = "Destroyer Flotilla 3" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = CAN } } }
			ship = { name = "Destroyer Flotilla 4" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = CAN } } }
		}
	}			
}

air_wings = {
	### Royal Canadian Air Force (RCAF)-- New South Wales
	276 = {
		Fighter_Bomber_equipment_1933 =  { # Improved WW1 Fighter
			owner = "CAN" 
			amount = 15
		}
	}
}

##### Starting Production #####
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1936
			creator = "CAN"
		}
		requested_factories = 3
		progress = 0.73
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = Tank_equipment_1918
			creator = "CAN"
		}
		requested_factories = 1
		progress = 0.85
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "CAN"
		}
		requested_factories = 1
		progress = 0.64
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = DD_equipment_1922
			creator = "CAN"
		}
		requested_factories = 1
		progress = 0.44
		amount = 3
	}
}