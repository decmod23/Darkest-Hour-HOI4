﻿division_template = {
	name = "Alfurqat Almasha"		# 'Infantry Division' - Represents local tribal levies (militia)

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
}

division_template = {
	name = "Alfurqat Alfursan"		# Camel-mounted levies (militia)

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Qasam Alhamia"		# Garrison Division

	regiments = {
		garrison = { x = 0 y = 0 }
		garrison = { x = 0 y = 1 }
		garrison = { x = 1 y = 0 }
		garrison = { x = 1 y = 1 }
    }
    priority = 0
}


units = {
	##### Royal Saudi Army #####
	division = {
		name = "firqat almushat alsewdyt al'uwlaa"
		location = 12727 # Riyadh
		division_template = "Alfurqat Almasha"
		start_experience_factor = 0.1
	}

	division = {
		name = "alharis almalakiu"
		location = 12727 # Riyadh
		division_template = "Alfurqat Almasha"
		start_experience_factor = 0.2
	}

	division = {
		name = "silah alfurasan alsaeudiu 'almadinat'"
		location = 12727 # Riyadh
		division_template = "Alfurqat Alfursan"
		start_experience_factor = 0.2
	}
}

	######## No notable air force ########

instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1900
			creator = "SAU"
		}
		requested_factories = 1
		progress = 0.38
		efficiency = 100
	}
}