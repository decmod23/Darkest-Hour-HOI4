﻿##### Division Templates #####
division_template = {
	name = "División de Infantería"         # Infantry Division	
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 1 y = 3 }
	}
}

division_template = {
	name = "División de infantería con brigada de tanques"         # Infantry Division with Tank Brigade		
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 1 y = 0 }
		medium_armor = { x = 1 y = 1 }
		medium_armor = { x = 1 y = 2 }
		medium_armor = { x = 1 y = 3 }
	}
}

division_template = {
	name = "División de infantería con artillería"         # Infantry Division	with Artillery
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		artillery_brigade = { x = 1 y = 2 }
		artillery_brigade = { x = 1 y = 3 }
	}
}

division_template = {
	name = "División de infantería con caballería"         # Infantry Division	with Cavalry
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
		cavalry = { x = 1 y = 3 }
	}
}

division_template = {
	name = "División de Caballería"  		# Cavalry Division 

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 0 y = 3 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
		cavalry = { x = 1 y = 3 }
		cavalry = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
		artillery_brigade = { x = 2 y = 2 }
		artillery_brigade = { x = 2 y = 3 }
	}
}

division_template = {
	name = "Brigada Montaña"  		# Mountain Brigades 

	regiments = {
		mountaineers = { x = 0 y = 0 }	
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
	}
}

division_template = {
	name = "División Guarnición" 		# Garrison Division 

	regiments = {
		garrison = { x = 0 y = 0 }
		garrison = { x = 0 y = 1 }
		garrison = { x = 0 y = 2 }
		garrison = { x = 0 y = 3 }
		garrison = { x = 1 y = 0 }
		garrison = { x = 1 y = 1 }
		artillery_brigade = { x = 1 y = 2 }
		artillery_brigade = { x = 1 y = 3 }
	}
	priority = 0
}

division_template = {
	name = "Brigada de milicia"         # Militia Brigade
	
	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 0 y = 2 }
		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }
		militia = { x = 1 y = 2 }
	}
	priority = 0
}


###### OOB ######
units = {
	##### 33 Regimiento Infanteria 'Cadiz' #####
	division = {
		name = "33 Regimiento Infanteria 'Cadiz'"
		location = 1048 # Cadiz
		division_template = "División Guarnición"
		start_experience_factor = 0.1
		start_equipment_factor = 0.2
	}
	##### 34 Regimiento Infanteria 'Sevilla' #####
	division = {
		name = "34 Regimiento Infanteria 'Sevilla'"
		location = 4098 # Alicante
		division_template = "División Guarnición"
		start_experience_factor = 0.1
		start_equipment_factor = 0.2
	}
	##### 35 Regimiento Infanteria 'Merida' #####
	division = {
		name = "35 Regimiento Infanteria 'Merida'"
		location = 758 # Coruna
		division_template = "División Guarnición"
		start_experience_factor = 0.1
		start_equipment_factor = 0.2
	}
	##### 1 Division Infanteria #####
	division = {
		name = "1a Division Infanteria"
		location = 3938 # Madrid
		division_template = "División de infantería con brigada de tanques"
		start_experience_factor = 0.2
		start_equipment_factor = 0.75
	}
	##### 2 Division Infanteria #####
	division = {
		name = "2a Division Infanteria"
		location = 7183 # Sevilla
		division_template = "División de infantería con artillería"
		start_experience_factor = 0.2
		start_equipment_factor = 0.75
	}
	##### 3 Division Infanteria #####
	division = {
		name = "3a Division Infanteria"
		location = 6906 # Valencia
		division_template = "División de infantería con caballería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}
	##### 4 Division Infanteria #####
	division = {
		name = "4a Division Infanteria"
		location = 9764 # Barcelona
		division_template = "División de infantería con artillería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}
	##### 5 Division Infanteria #####
	division = {
		name = "5a Division Infanteria"
		location = 3816 # Zaragoza
		division_template = "División de infantería con caballería"
		start_experience_factor = 0.2
		start_equipment_factor = 0.75
	}
	##### 6 Division Infanteria #####
	division = {
		name = "6a Division Infanteria"
		location = 885 # Burgos
		division_template = "División de infantería con artillería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}
	##### 7 Division Infanteria #####
	division = {
		name = "7a Division Infanteria"
		location = 3918 # Leon
		division_template = "División de infantería con artillería"
		start_experience_factor = 0.2
		start_equipment_factor = 0.75 
	}
	##### 8 Division Infanteria #####
	division = {
		name = "8a Division Infanteria"
		location = 758 # Coruna
		division_template = "División de infantería con artillería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.75
	}
	##### Division Caballeria #####
	division = {
		name = "Division Caballeria"
		location = 3874 # Salamanca
		division_template = "División de Caballería"
		start_experience_factor = 0.2
		start_equipment_factor = 0.75
	}
	##### 1 Brigada Montana #####
	division = {
		name = "1a Brigada Montana"
		location = 9857 # Pamplona
		division_template = "Brigada Montaña"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}
	##### 2 Brigada Montana #####
	division = {
		name = "2a Brigada Montana"
		location = 854 # Gerona
		division_template = "Brigada Montaña"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}
	##### Comandancia General de Baleares #####
	division = {
		name = "Comandancia General de Baleares"
		location = 9793 # Palma de Mallorc
		division_template = "División de Infantería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.45
	}
	##### Comandancia General de Canarias #####
	division = {
		name = "Comandancia General de Canarias"
		location = 13071 # Las Palmas de Gran
		division_template = "División de Infantería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.45
	}
	##### Fuerzas del Sahara Espanol #####
	division = {
		name = "Fuerzas del Sahara Espanol"
		location = 8038 # El Aiun
		division_template = "Brigada de milicia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.1
	}
	##### Circonscripcion Occidental #####
	division = {
		name = "Ceuta-Tetuan Sub-Division"
		location = 9945 # Tangiers (Ceuta)
		division_template = "División de Infantería"
		start_experience_factor = 0.2
	}
	
	division = {
		name = "Larache Sub-Division"
		location = 9945 # Tangiers (Ceuta)
		division_template = "División de Infantería"
		start_experience_factor = 0.2
	}

	division = {
		name = "Tercio 'Duque de Alba' 2 de la Legion"
		location = 9945 # Tangiers (Ceuta)
		division_template = "División de Infantería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}

	division = {
		name = "Mehal-La 1"
		location = 9945 # Tangiers (Ceuta)
		division_template = "Brigada de milicia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}

	division = {
		name = "Mehal-La 3"
		location = 9945 # Tangiers (Ceuta)
		division_template = "Brigada de milicia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}

	division = {
		name = "Mehal-La 4"
		location = 9945 # Tangiers (Ceuta)
		division_template = "Brigada de milicia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	##### Circonscripcion Oriental #####
	division = {
		name = "Fuerzas Regulares Indigenas"
		location = 12100 # Nador
		division_template = "División de Infantería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}

	division = {
		name = "Tercio 'Gran Capitan' 1 de la Legion"
		location = 12100 # Nador
		division_template = "División de Infantería"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}

	division = {
		name = "Mehal-La 2"
		location = 12100 # Nador
		division_template = "Brigada de milicia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}

	division = {
		name = "Mehal-La 5"
		location = 12100 # Nador
		division_template = "Brigada de milicia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	##### navy oob #####
	fleet = {
		name = "Base Naval Cádiz"
		naval_base = 1048 # Cadiz
		task_force = {
			name = "Base Naval Cádiz"
			location = 1048 # Cadiz
			ship = { name = "República" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = SPR } } }
			ship = { name = "Flotilla de Torpederos No.1" definition = destroyer equipment = { DD_equipment_1885 = { amount = 1 owner = SPR } } }
		}
	}

	fleet = {
		name = "Base Naval Cartagena"
		naval_base = 6906 # Valencia
		task_force = {
			name = "Base Naval Cartagena"
			location = 6906 # Valencia
			ship = { name = "Dédalo" definition = escort_carrier equipment = { CV_equipment_1912 = { amount = 1 owner = SPR } } air_wings = {} }
			ship = { name = "Flotilla de Destructores No.2" definition = destroyer equipment = { DD_equipment_1900 = { amount = 1 owner = SPR } } }
			ship = { name = "Flotilla de Destructores No.1" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = SPR } } }
			ship = { name = "Flotilla de Torpederos No.2" definition = destroyer equipment = { DD_equipment_1885 = { amount = 1 owner = SPR } } }
			ship = { name = "Flotilla de Submarinos No.4" definition = submarine equipment = { SS_equipment_1922 = { amount = 1 owner = SPR } } }
			ship = { name = "Flotilla de Submarinos No.3" definition = submarine equipment = { SS_equipment_1922 = { amount = 1 owner = SPR } } }
			ship = { name = "Flotilla de Submarinos No.2" definition = submarine equipment = { SS_equipment_1916 = { amount = 1 owner = SPR } } }
		}
	}

	fleet = {
		name = "Base Naval El Ferrol"
		naval_base = 758 # Coruna
		task_force = {
			name = "Base Naval El Ferrol"
			location = 758 # Coruna
			ship = { name = "España" definition = battleship equipment = { BB_equipment_1895 = { amount = 1 owner = SPR } } }
			ship = { name = "Jaime I" definition = battleship equipment = { BB_equipment_1895 = { amount = 1 owner = SPR } } }
			ship = { name = "Libertad" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = SPR } } }
			ship = { name = "Almirante Cervera" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = SPR } } }
			ship = { name = "Miguel de Cervantes" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = SPR } } }
			ship = { name = "Flotilla de Torpederos No.3" definition = destroyer equipment = { DD_equipment_1885 = { amount = 1 owner = SPR } } }
		}
	}

	fleet = {
		name = "Base Naval Mahon"
		naval_base = 9793 # Palma de Mallorc
		task_force = {
			name = "Base Naval Mahon"
			location = 9793 # Palma de Mallorc
			ship = { name = "Flotilla de Submarinos No.1" definition = submarine equipment = { SS_equipment_1916 = { amount = 1 owner = SPR } } }
		}
	}

	fleet = {
		name = "Naval Estacion Canarias"
		naval_base = 13071 # Las Palmas de Gran
		task_force = {
			name = "Naval Estacion Canarias"
			location = 13071 # Las Palmas de Gran
			ship = { name = "Méndez Núñez" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = SPR } } }
		}
	}
}

### Air Wings
air_wings = {
	41 = {
		# Escuadra No.1
		Fighter_equipment_1933 = { # Nieuport-Delage NiD.29
			owner = "SPR"
			amount = 25
		}
		# Escuadra No.2
		Fighter_equipment_1933 = { # Nieuport-Delage NiD.29
			owner = "SPR"
			amount = 25
		}
	}

	167 = {
		# Escuadra No.3
		Naval_Bomber_equipment_1936 = { # Dornier Do J Wal
			owner = "SPR"
			amount = 15
		}
	}
}
### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1900
			creator = "SPR"
		}
		requested_factories = 3
		progress = 0.38
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = Tank_equipment_1926
			creator = "SPR"
		}
		requested_factories = 2
		progress = 0.22
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "SPR"
		}
		requested_factories = 2
		progress = 0.24
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = Fighter_equipment_1933
			creator = "SPR"
		}
		requested_factories = 1
		progress = 0.74
		efficiency = 100
	}
	### Under Contruction Notes ###
	# DD: Churucca class (x3) ("ARE Almirante Miranda" "ARE Gravina" "ARE Escaño" "ARE Císcar" "ARE Jorge Juan" "ARE Ulloa")
	add_equipment_production = {
		equipment = {
			type = DD_equipment_1912
			creator = "SPR"
		}
		requested_factories = 1
		progress = 0.80
		amount = 3
	}
}