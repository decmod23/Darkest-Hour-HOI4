#########################################################################
#  British Decisions
#########################################################################
ENG_invest_dominions_category = {
	###########################
	# Invest in South Africa  
	###########################
	ENG_Invest_South_Africa = {
		icon = placeholder
		
		fire_only_once = yes
		
		cost = 50

		available = {
            original_tag = ENG
		}

		ai_will_do = {
			factor = 25 
		}

		complete_effect = {
			SAF = {
				add_opinion_modifier = { target = ENG modifier = aided_industry }
				random_owned_controlled_state = {
					limit = {
						free_building_slots = {
							building = industrial_complex
							size > 1
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 2
					add_building_construction = {
						type = industrial_complex
						level = 2
						instant_build = yes
					}
					set_state_flag = ENG_uk_south_africa_focus_2IC
				}
				random_owned_controlled_state = {
					limit = {
						free_building_slots = {
							building = industrial_complex
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = industrial_complex
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_south_africa_focus_1IC
				}
				random_owned_controlled_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = dockyard
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_south_africa_focus_1dockyard
				}
				random_owned_controlled_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = dockyard
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_south_africa_focus_1dockyard
				}
				add_autonomy_ratio = {
					value = -0.05
					localization = uk_south_africa_focus
				}
			}
		}
	}
	
	###########################
	# Invest in Australia  
	###########################
	ENG_Invest_Australia = {
		icon = placeholder
		
		fire_only_once = yes
		
		cost = 50

		available = {
            original_tag = ENG
		}

		ai_will_do = {
			factor = 25 
		}

		complete_effect = {
			AST = {
				add_opinion_modifier = { target = ENG modifier = aided_industry }
				random_owned_controlled_state = {
					limit = {
						free_building_slots = {
							building = industrial_complex
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = industrial_complex
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_australia_focus_1IC
				}
				random_owned_controlled_state = {
					limit = {
						free_building_slots = {
							building = industrial_complex
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = industrial_complex
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_australia_focus_1IC
				}
				random_owned_controlled_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 1
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 2
					add_building_construction = {
						type = dockyard
						level = 2
						instant_build = yes
					}
					set_state_flag = ENG_uk_australia_focus_2dockyards
				}
				random_owned_controlled_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = dockyard
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_australia_focus_1dockyard
				}
				add_autonomy_ratio = {
					value = -0.05
					localization = uk_australia_focus
				}
			}
		}
	}	
	
	###########################
	# Invest in New Zealand  
	###########################
	ENG_Invest_New_Zealand = {
		icon = placeholder
		
		fire_only_once = yes
		
		cost = 50

		available = {
            original_tag = ENG
		}

		ai_will_do = {
			factor = 25 
		}

		complete_effect = {
			NZL = {
				add_opinion_modifier = { target = ENG modifier = aided_industry }
				random_owned_controlled_state = {
					limit = {
						free_building_slots = {
							building = industrial_complex
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = industrial_complex
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_new_zealand_focus_1IC
				}
				random_owned_controlled_state = {
					limit = {
						free_building_slots = {
							building = industrial_complex
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = industrial_complex
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_new_zealand_focus_1IC
				}
				random_owned_controlled_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 1
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 2
					add_building_construction = {
						type = dockyard
						level = 2
						instant_build = yes
					}
					set_state_flag = ENG_uk_new_zealand_focus_2dockyards
				}
				random_owned_controlled_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = dockyard
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_new_zealand_focus_1dockyard
				}
				add_autonomy_ratio = {
					value = -0.05
					localization = uk_new_zealand_focus
				}
			}
		}
	}
	
	###########################
	# Invest in Canada  
	###########################
	ENG_Invest_Canada = {
		icon = placeholder
		
		fire_only_once = yes
		
		cost = 50

		available = {
            original_tag = ENG
		}

		ai_will_do = {
			factor = 25 
		}
		
		complete_effect = {
			CAN = {
				add_opinion_modifier = { target = ENG modifier = aided_industry }
				random_owned_controlled_state = {
					limit = {
						free_building_slots = {
							building = industrial_complex
							size > 1
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 2
					add_building_construction = {
						type = industrial_complex
						level = 2
						instant_build = yes
					}
					set_state_flag = ENG_uk_canada_focus_2IC
				}
				random_owned_controlled_state = {
					limit = {
						free_building_slots = {
							building = industrial_complex
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = industrial_complex
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_canada_focus_1IC
				}
				random_owned_controlled_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = dockyard
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_canada_focus_1dockyard
				}
				random_owned_controlled_state = {
					limit = {
						is_coastal = yes
						free_building_slots = {
							building = dockyard
							size > 0
							include_locked = yes
						}
					}
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = dockyard
						level = 1
						instant_build = yes
					}
					set_state_flag = ENG_uk_canada_focus_1dockyard
				}
				add_autonomy_ratio = {
					value = -0.05
					localization = uk_canada_focus
				}
			}
		}
	}
}