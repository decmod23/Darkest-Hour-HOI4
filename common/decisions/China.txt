#########################################################################
#  Chinese Diplomactic Decisions
#########################################################################
CHI_factions_category = {

    CHI_Generalissimo_Law = {
		icon = generic_independence
		fire_only_once = yes
		cost = 260
		#days_re_enable = 0
		#days_remove = 0
		
		visible = {
			original_tag = CHI		
		}
        available = {
			#has_completed_focus = CHI_Generalissimo_Law
        }		
		
    	complete_effect = {
    		add_stability = 0.1
    	}
    }
    CHI_Purge_Generals = {
    	icon = generic_independence
    	fire_only_once = no
    	days_remove = 30
    	cost = 150

    	visible = {
    		original_tag = CHI
    		has_completed_focus = CHI_Remove_Opposition
    	}
    	available = {has_completed_focus = CHI_Remove_Opposition}

    	complete_effect = {
    		add_stability = -0.1
    	}

    	remove_effect = {
    		add_to_variable = {
    			var = faction_loyalty_kuomintang
    			value = 10
    		}
    		add_timed_idea = {
    			idea = CHI_Purge_1
    			days = 100
    		}
    	}
    }
    CHI_Reconcile_Zhang_Fakui = {
    	icon = decision_ger_military_buildup
    	fire_only_once = yes
    	cost = 150

    	visible = {
    		original_tag = CHI
    		has_completed_focus = CHI_Reconcile_Opposition
    	}
    	available = {has_completed_focus = CHI_Reconcile_Opposition}

    	complete_effect = {
    		create_corps_commander = {
    			name = "Zhang Fakui"
    			skill = 4
    			attack_skill = 4
    			defense_skill = 3
    			planning_skill = 2
    			logistics_skill = 3
    		}
    		custom_effect_tooltip = CHI_Reconcile_Opposition_Risk_Low
    	}
    }
    CHI_Reconcile_Tang_Shengzi = {
    	icon = decision_ger_military_buildup
    	fire_only_once = yes
    	cost = 150

    	visible = {
    		original_tag = CHI
    		has_completed_focus = CHI_Reconcile_Opposition
    	}
    	available = {has_completed_focus = CHI_Reconcile_Opposition}

    	complete_effect = {
    		create_corps_commander = {
    			name = "Tang Shengzi"
    			skill = 4
    			attack_skill = 4
    			defense_skill = 3
    			planning_skill = 2
    			logistics_skill = 3
    		}
    	}
    }
    CHI_Reconcile_Li_Jishen = {
    	icon = decision_ger_military_buildup
    	fire_only_once = yes
    	cost = 150

    	visible = {
    		original_tag = CHI
    		has_completed_focus = CHI_Reconcile_Opposition
    	}
    	available = {has_completed_focus = CHI_Reconcile_Opposition}

    	complete_effect = {
    		create_corps_commander = {
    			name = "Li Jishen"
    			skill = 4
    			attack_skill = 4
    			defense_skill = 3
    			planning_skill = 2
    			logistics_skill = 3
    		}
    		custom_effect_tooltip = CHI_Reconcile_Opposition_Risk_High
    	}
    }
}

#########################################################################
#  Tutelage
#########################################################################
CHI_Peoples_Tutelage_Missions = {

	###########################
	# Welfare Education
	###########################
	CHI_Welfare_Mission = {
		icon = decision_ger_military_buildup
		fire_only_once = yes
		days_remove = 365
		cost = 200

		visible = {
			original_tag = CHI
			has_completed_focus = CHI_Political_Tutelage
		}
		available = {
			# has_completed_focus = CHI_Welfare_Principle	
			NOT = { has_country_flag = CHI_tutelage_campaign_running }
		}	

		complete_effect = {
			set_country_flag = CHI_tutelage_campaign_running
		}
		
		remove_effect = {
			clr_country_flag = CHI_tutelage_campaign_running
			add_political_power = 10
		}

		ai_will_do = {
			factor = 1
		}		
	}
	###########################
	# Nationalism Education
	###########################
	CHI_Nationalism_Mission = {
		icon = decision_ger_military_buildup
		fire_only_once = yes
		days_remove = 365
		cost = 200

		visible = {
			original_tag = CHI
			has_completed_focus = CHI_Political_Tutelage
		}

		available = {
			# has_completed_focus = CHI_Nationalism_Principle	
			NOT = { has_country_flag = CHI_tutelage_campaign_running }
		}	
		
		complete_effect = {
			set_country_flag = CHI_tutelage_campaign_running
		}

		remove_effect = {
			clr_country_flag = CHI_tutelage_campaign_running
			add_political_power = 10
		}
		
		ai_will_do = {
			factor = 1
		}		
	}
	###########################
	# Democracy Education
	###########################
	CHI_Democracy_Mission = {
		icon = decision_ger_military_buildup
		fire_only_once = yes
		days_remove = 365
		cost = 200

		visible = {
			original_tag = CHI
			has_completed_focus = CHI_Political_Tutelage
		}

		available = {
			# has_completed_focus = CHI_Democracy_Principle	
			NOT = { has_country_flag = CHI_tutelage_campaign_running }
		}	

		complete_effect = {
			set_country_flag = CHI_tutelage_campaign_running
		}
		
		remove_effect = {
			clr_country_flag = CHI_tutelage_campaign_running
			add_political_power = 10
		}
		
		ai_will_do = {
			factor = 1
		}		
	}
}

#########################################################################
#  Military Reform
#########################################################################

CHI_Army_Reform_Actions = {

	###########################
	# Foriegn Assistance
	###########################
	CHI_American_Assistance = {
		icon = decision_ger_military_buildup
		fire_only_once = yes
		days_remove = 30
		cost = 150

		visible = {
			original_tag = CHI
			has_completed_focus = CHI_Foreign_Military_Assistance
		}

		available = {
			has_completed_focus = CHI_Foreign_Military_Assistance	
		}
		
		remove_effect = {
			add_political_power = 10
		}
		
		ai_will_do = {
			factor = 1
		}		
	}

	CHI_German_Assistance = {
		icon = decision_ger_military_buildup
		fire_only_once = yes
		days_remove = 30
		cost = 150

		visible = {
			original_tag = CHI
			has_completed_focus = CHI_Foreign_Military_Assistance
		}

		available = {
			has_completed_focus = CHI_Foreign_Military_Assistance	
		}
		
		remove_effect = {
			add_political_power = 10
		}
		
		ai_will_do = {
			factor = 1
		}		
	}

	CHI_Soviet_Assistance = {
		icon = decision_ger_military_buildup
		fire_only_once = yes
		days_remove = 30
		cost = 150

		visible = {
			original_tag = CHI
			has_completed_focus = CHI_Foreign_Military_Assistance
		}

		available = {
			has_completed_focus = CHI_Foreign_Military_Assistance	
		}
		
		remove_effect = {
			add_political_power = 10
		}
		
		ai_will_do = {
			factor = 1
		}		
	}
}