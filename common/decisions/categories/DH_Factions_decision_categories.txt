factions_decision_category = {
	icon = generic_political_actions
	priority = 85
	visible = {
		country_has_factions = yes
		check_variable = { active_factions_array^num > 0 }
	}
	scripted_gui = factions_decision_category_scripted_gui
	
	visible_when_empty = yes
}