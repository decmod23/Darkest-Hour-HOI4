JAP_bribe_the_tokko = {
	allowed = {
		original_tag = JAP
	}
}

JAP_Form_Puppet_Governments = {
	allowed = {
		original_tag = JAP
	}
}

JAP_southern_expansion = {
	allowed = {
		original_tag = JAP
	}

	visible = {
		is_puppet = no
	}
}
JAP_Northern_Expansion_Doctrine = {
	allowed = {
		original_tag = JAP
	}
}

JAP_Interservice_Rivalry = {
	allowed = {
		original_tag = JAP
	}
}

JAP_Form_Provisional_Government_of_China = {
	allowed = {
		original_tag = JAP
	}
}
JAP_Establish_Reformed_Government_of_China = {
	allowed = {
		original_tag = JAP
	}
}
JAP_Establish_Reorganized_China = {
	allowed = {
		original_tag = JAP
	}
}