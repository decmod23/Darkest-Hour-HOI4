#########################################################################
#  Occupation Policy
#########################################################################
Occupation_Policy = {
	###########################
	# German Occupation of the US
	###########################
	Occupation_GER_USA = {
		icon = border_war
		fire_only_once = no
		selectable_mission = yes
		days_mission_timeout = 30
		is_good = no
		cost = freedec?0
		allowed = {
			tag = GER
		}
		activation = {
			# Washington D.C
			784 = {
				is_owned_and_controlled_by = ROOT
			}
			784 = {
				NOT = {
					is_claimed_by = ROOT
					is_core_of = ROOT
				}
			}
			is_subject = no
			NOT = {
				country_exists = USA
			}
		}
		cancel_trigger = {
			NOT = {
				784 = {
					is_owned_and_controlled_by = ROOT
				}
			}
		}
		complete_effect = {
			country_event = {
				id = DH_Occupation_GER_USA.1
				hours = 2
			}
		}
		timeout_effect = {
			country_event = {
				id = DH_Occupation_GER_USA.1
				hours = 2
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}
