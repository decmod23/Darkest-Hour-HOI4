#	Example:
# 
#	example_trigger = {
#		tag = GER
#		is_ai = no
#	}
#
#
#	In a script file:
#
#	trigger = {
#		exampel_trigger = yes
#	}
#

can_ROOT_get_wargoal_on_THIS = {
	exists = yes
	NOT = { is_in_faction_with = ROOT }
	NOT = { is_subject_of = ROOT }
}

#Country scope
is_JAP_or_ally_of_JAP = {
	OR = {
		tag = JAP
		is_in_faction_with = JAP
		is_subject_of = JAP
	}
}

is_border_conflict_defender_vs_FROM = {
	has_variable = ROOT.defender_state_vs_@FROM
}

has_not_initiated_border_incident_with_FROM = {
	custom_trigger_tooltip = {
		tooltip = not_initiated_border_incident_with_FROM
		NOT = {		
			any_state = {
				check_variable = { FROM.defender_state_vs_@PREV = id }
			}
		}
	}
}

has_not_initiated_border_incident_with_ROOT = {
	custom_trigger_tooltip = {
		tooltip = not_initiated_border_incident_with_ROOT
		NOT = {
			any_state = {
				check_variable = { ROOT.defender_state_vs_@PREV = id }
			}
		}
	}
}

#State scope
has_ROOT_at_least_1_div_in_current_state_scope = {
	custom_trigger_tooltip = {
		tooltip = at_least_one_division_in_state
		ROOT = { divisions_in_state = { state = PREV size > 0 } }
	}
}

check_has_focus_tree_to_switch_to_fascism = {
	NOT = { tag = JAP }
	NOT = { tag = GER }
	NOT = { tag = MAN }
	NOT = { tag = ITA }
	NOT = {
		AND = {
			tag = SAF
			has_dlc = "Together for Victory"
		}
	}
	NOT = {
		AND = {
			tag = RAJ
			has_dlc = "Together for Victory"
		}
	}
}

check_has_focus_tree_to_switch_to_democratic = {
	NOT = { tag = MAN }
	NOT = { tag = FRA }
	NOT = { tag = USA }
	NOT = { tag = ENG }
	NOT = { tag = CAN }
	NOT = { tag = SAF }
	NOT = { tag = AUS }
	NOT = { tag = NZL }
	NOT = { tag = RAJ }
	NOT = { tag = CZE }
	NOT = {
		AND = {
			tag = HUN
			has_dlc = "Death or Dishonor"
		}
	}
	NOT = {
		AND = {
			tag = JAP
			has_dlc = "Waking the Tiger"
		}
	}
	NOT = {
		AND = {
			tag = GER
			has_dlc = "Waking the Tiger"
		}
	}
	NOT = {
		AND = {
			tag = YUN
			has_dlc = "Waking the Tiger"
			NOT = { has_focus_tree = china_nationalist_focus }
		}
	}
	NOT = {
		AND = {
			tag = GXC
			has_dlc = "Waking the Tiger"
			NOT = { has_focus_tree = china_nationalist_focus }
		}
	}
	NOT = {
		AND = {
			tag = XSM
			has_dlc = "Waking the Tiger"
			NOT = { has_focus_tree = china_nationalist_focus }
		}
	}
	NOT = {
		AND = {
			tag = SHX
			has_dlc = "Waking the Tiger"
			NOT = { has_focus_tree = china_nationalist_focus }
		}
	}
	NOT = {
		AND = {
			tag = SIK
			has_dlc = "Waking the Tiger"
			NOT = { has_focus_tree = china_nationalist_focus }
		}
	}
}

check_has_focus_tree_to_switch_to_communism = {
	NOT = { tag = PRC }
	NOT = { tag = SIK }
	NOT = { tag = MAN }
	NOT = { tag = SOV }
	NOT = {
		AND = {
			tag = RAJ
			has_dlc = "Together for Victory"
		}
	}
	NOT = {
		AND = {
			tag = JAP
			has_dlc = "Waking the Tiger"
		}
	}
	NOT = {
		AND = {
			tag = YUN
			has_dlc = "Waking the Tiger"
		}
	}
	NOT = {
		AND = {
			tag = GXC
			has_dlc = "Waking the Tiger"
		}
	}
	NOT = {
		AND = {
			tag = XSM
			has_dlc = "Waking the Tiger"
		}
	}
	NOT = {
		AND = {
			tag = SHX
			has_dlc = "Waking the Tiger"
		}
	}
}

#State scope
is_controlled_by_ROOT_or_subject = {
	CONTROLLER = {
		OR = {
			tag = ROOT
			is_subject_of = ROOT
		}
	}
}

#New MtG AI doctrine triggers
ai_air_doctrine_tier_1_trigger = {
	OR = {
		has_tech = naval_strike_tactics
		has_tech = direct_ground_support
		has_tech = low_echelon_support
	}
}

ai_air_doctrine_tier_2_trigger = {
	OR = {
		has_tech = multialtitude_flying
		has_tech = hunt_and_destroy
		has_tech = operational_destruction
	}
}
ai_air_doctrine_tier_3_trigger = {
	OR = {
		has_tech = flying_fortress
		has_tech = naval_strike_torpedo_tactics
		has_tech = infiltration_bombing
	}
}

ai_land_doctrine_tier_1_trigger = {
	OR = {
		has_tech = Firepower_Focus
		has_tech = Mobility_Focus
		has_tech = Planning_Focus
		has_tech = Manpower_Focus
	}
}

ai_land_doctrine_tier_2_trigger = {
	OR = {
		has_tech = Central_Planning
		has_tech = Regimental_Combat_Teams
		has_tech = Kampfgruppe
		has_tech = Large_Formation_SOP
		has_tech = Defense_in_Depth_2
		has_tech = Revolutionary_Base_Areas_Expansion
		has_tech = Mobile_Guerilla_Warfare
	}
}