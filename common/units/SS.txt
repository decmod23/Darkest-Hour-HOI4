##############################################################
## SS Units
###############################################################
sub_units = {
#### SS Infantry Batallions
	SS_infantry = {
		sprite = infantry
		map_icon_category = infantry
		
		priority = 600
		ai_priority = 200
		active = no

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 28 #SS Units Stronk
		max_organisation = 55 #They were rather disorganized tho
		default_morale = 0.35 #Fanatics
		manpower = 1000

		#Misc Abilities
		training_time = 90
		suppression = 1.2 #Better at Suppressing stuff
		weight = 0.5
		
		supply_consumption = 0.09 #High Supply Consumption
	
		need = {
			infantry_equipment = 100 #x10
		}
		
		urban = { #Rather decent in CQB combat
			attack = 0.05
			defence = 0.1
		}		
	}
#### SS Cavalry Batallions
	SS_cavalry = {
		sprite = cavalry
		map_icon_category = other
		priority = 599
		ai_priority = 200
		active = no
		cavalry = yes

		type = { infantry }
		
		group = infantry
		
		categories = {
			category_front_line
			category_army
		}

		
		combat_width = 2
		
		#Size Definitions
		max_strength = 28 #SS Units Stronk
		max_organisation = 65 #They were rather disorganized tho
		default_morale = 0.4 #Fanatics
		manpower = 1000

		#Misc Abilities
		maximum_speed = 0.6
		training_time = 140 #Elite Units
		weight = 0.5
		
		supply_consumption = 0.12 #High Supply Consumption

		#Offensive Abilities
		suppression = 2.2 #Better at Suppressing rebels
	
		need = {
			infantry_equipment = 100 #x10
		}

		forest = {
			attack = -0.05
		}

		hills = {
			attack = -0.05
		}

		mountain = {
			attack = -0.1
			movement = -0.05
		}

		jungle = {
			attack = -0.1
		}

		urban = {
			attack = -0.05
		}

		amphibious = {
			attack = -0.4
		}
	}	
#### SS Motorized Infantry Batallions	
	SS_motorized = {
		sprite = motorized
		map_icon_category = infantry

		priority = 599
		ai_priority = 200
		active = no

		type = {
			motorized
		}
		
		group = mobile
		
		categories = {
			category_front_line
			category_all_infantry
			category_army
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 28 #SS Units Stronk
		max_organisation = 55 #They were rather disorganized tho
		default_morale = 0.4 #Fanatics
		manpower = 1200

		#Misc Abilities
		training_time = 120 #Elite Units
		suppression = 1.3 #Better at Suppressing rebels
		weight = 0.75
		supply_consumption = 0.15 #High Supply Consumption
	
		# this is what moves us and sets speed
		transport = motorized_equipment
	
		need = {
			infantry_equipment = 100 #x10
			motorized_equipment = 50
		}

		forest = {
			attack = -0.1
			movement = -0.5
		}
		mountain = {
			attack = -0.05
		}
		jungle = {
			attack = -0.2
			movement = -0.5
		}
		marsh = {
			attack = -0.1
			movement = -0.3
		}
		urban = {
			#attack = -0.1  #Rather decent in CQB combat
		}
		river = {
			attack = -0.1
			movement = -0.1
		}
		amphibious = {
			attack = -0.2
		}
	}
	
#### SS Mechanized Infantry Batallions
	SS_mechanized = {
		sprite = mechanized
		map_icon_category = infantry

		priority = 610
		ai_priority = 200
		active = no

		type = {
			motorized
		}
		
		group = mobile
		
		categories = {
			category_front_line
			category_all_infantry
			category_army
		}

		combat_width = 2

		#Offensive Abilities
		soft_attack = 0.1
		hard_attack = 4.0
		
		#Size Definitions
		max_strength = 35 #SS Units Stronk 
		max_organisation = 55 #They were rather disorganized tho
		default_morale = 0.4 #Fanatics
		manpower = 1200

		#Misc Abilities
		training_time = 150 #Elite Units
		suppression = 1.3 #Better at Suppressing rebels
		weight = 1
		
		supply_consumption = 0.24 #High Supply Consumption
		
		# needed since we give so much bonus to infantry even with no mech equipment
		essential = {
			infantry_equipment
			APC_equipment
		}

		# this is what moves us and sets speed
		transport = APC_equipment

		need = {
			APC_equipment = 50
			infantry_equipment = 100 #x10
		}

		forest = {
			attack = -0.2
		}
		mountain = {
			attack = -0.05
		}
		jungle = {
			attack = -0.3
		}
		marsh = {
			attack = -0.1
		}
		urban = { #Rather decent in CQB combat
			attack = -0.1
			defence = 0.05
		}
		river = {
			attack = -0.2
			movement = -0.2
		}
		amphibious = {
			attack = -0.4
		}

		hardness = 0.2
	}	
#### SS Panzer Batallions
	SS_armor = {
		sprite = "medium_armor"
		map_icon_category = armored
		priority = 2502
		ai_priority = 3500
		
		type = {
			armor
		}
		
		group = armor
		
		categories = {
			category_tanks
			category_front_line
			category_all_armor
			category_army
		}

		combat_width = 2

		need = {
			armor_equipment = 50
		}
		manpower = 500
		max_organisation = 9 #They were rather disorganized tho
		max_strength = 3 #SS Stronk
		default_morale = 0.4 #Fanatics
		training_time = 210 #Elite Units
		weight = 1.25
		supply_consumption = 0.25 #High Supply Consumption

		#Tanks are awesome
		soft_attack = 0.5

		forest = {
		    attack = -0.3
			movement = -0.4
		}
		hills = 	{
		    attack = -0.1
		}
		mountain = 	{
		    attack = -0.2
		}
		jungle = {
		    attack = -0.4
			movement = -0.4
		}
		marsh = {
		    attack = -0.1 
			movement = -0.1
		}
		urban = {
			attack = -0.4
			defence = -0.1
		}
		river = { 
			attack = -0.2 
			movement = -0.2
		}
		amphibious = { 
			attack = -0.7
		}
	}	
#### SS Schwere Panzer Batallions
	SS_heavy_armor = {
		sprite = "heavy_armor"
		map_icon_category = armored
		priority = 2502
		ai_priority = 3500
		
		type = {
			armor
		}
		
		group = armor
		
		categories = {
			category_tanks
			category_front_line
			category_all_armor
			category_army
		}

		combat_width = 2

		need = {
			heavy_tank_equipment = 40
		}
		manpower = 500
		max_organisation = 9 #They were rather disorganized tho
		max_strength = 3 #SS Stronk
		default_morale = 0.4 #Fanatics
		training_time = 210 #Elite Units
		weight = 1.25
		supply_consumption = 0.25 #High Supply Consumption

		#Tanks are awesome
		soft_attack = 0.5

		forest = {
		    attack = -0.40
			movement = -0.40
		}
		hills = {
		    attack = -0.20
		}
		mountain = 	{
		    attack = -0.3
		}
		jungle = {
		    attack = -0.6
			movement = -0.4
		}
		marsh = {
		    attack = -0.4 
			movement = -0.2
		}
		urban = {
			attack = -0.5
			defence = -0.2
		}
		river = { 
			attack = -0.4 
			movement = -0.4
		}
		amphibious = { 
			attack = -0.8 
		}
		fort = {
			attack = 0.1
		}
	}	
}
