equipments = {
######################################################################
###################### Heavy Submarine  (HSS) ##################################
######################################################################
	heavy_submarine = {
		year = 1922

		is_archetype = yes
		is_buildable = no
		type = submarine
		group_by = archetype
		
		# upgrades = {
		# 	ship_reliability_upgrade
		# 	sub_engine_upgrade
		# 	sub_stealth_upgrade
		# 	sub_torpedo_upgrade
		# }
		
		interface_category = interface_category_other_ships

#Misc Stats
		build_cost_ic = 5000
		manpower = 200
		# port_capacity_usage = 0.5
		naval_range = 15000
		naval_speed = 14
#Detection Stats
		surface_detection = 30
		sub_detection = 0
		surface_visibility = 10
		sub_visibility = 30
#Defensive Stats
		# evasion = 25
		max_strength = 10
		reliability = 0.60
#Offensive Stats
		# fire_range = 6
		# attack = 0
		torpedo_attack = 10
		anti_air_attack = 0.5
		#sub_attack = 10	
		## shore_bombardment = 10	
		armor_value = 0
		ap_attack = 0
		#carrier_size = 45		
#Resources
		resources = { steel = 1 }
		fuel_consumption = 2
#
	}

################################
# 1933 Heavy Sub
################################
	HSS_equipment_1933 = {
		year = 1933

		archetype = heavy_submarine
		priority = 80

#Misc Stats
		build_cost_ic = 600
		manpower = 200
		# port_capacity_usage = 0.5
		naval_range = 15000
		naval_speed = 14
#Detection Stats
		surface_detection = 30
		sub_detection = 0
		surface_visibility = 10
		sub_visibility = 30
#Defensive Stats
		# evasion = 25
		max_strength = 10
		reliability = 0.60
#Offensive Stats
		# fire_range = 6
		# attack = 0
		torpedo_attack = 10
		anti_air_attack = 0.5
		#sub_attack = 10	
		## shore_bombardment = 10	
		armor_value = 0
		ap_attack = 0
		#carrier_size = 45		
#Resources
		resources = { steel = 1 }
#
	}
	
################################
# 1940 Heavy Sub
################################
	HSS_equipment_1940 = {
		year = 1940

		archetype = heavy_submarine
		parent = HSS_equipment_1933
		priority = 80

		naval_range = 20000
		naval_speed = 20
		# port_capacity_usage = 0.5
		manpower = 80
		
		surface_detection = 20
		surface_visibility = 5
		sub_visibility = 6
		
		max_strength = 30
		# evasion = 30
		reliability = 0.70
		
		# attack = 2
		# fire_range = 6
		torpedo_attack = 20
		anti_air_attack = 2 

		build_cost_ic = 945

		resources = {
			steel = 2
			chromium = 1
		}
		type = { 
			submarine
		}
	}
	
################################
# 1943 SuperHeavy Sub
################################
	SHSS_equipment_1945 = {
		year = 1943

		archetype = heavy_submarine
		parent = HSS_equipment_1940
		priority = 80
		
		naval_speed = 19
		naval_range = 24000
		# port_capacity_usage = 0.5
		manpower = 140
		
		surface_detection = 25
		surface_visibility = 5
		sub_visibility = 4
		
		max_strength = 50
		# evasion = 40
		reliability = 0.85
		
		# attack = 2
		# fire_range = 8
		torpedo_attack = 26
		anti_air_attack = 3
		
		build_cost_ic = 1320
		resources = {
			steel = 4 
			chromium = 1
		}
		type = { 
			submarine
		}		
	}
################################	
}