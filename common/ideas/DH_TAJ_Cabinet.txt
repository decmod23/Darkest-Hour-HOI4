ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Yevgenij Mamedov
		TAJ_HoG_Yevgenij_Mamedov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Yevgenij_Mamedov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Backroom_Backstabber }
		}
	# Shadan Lavrenets
		TAJ_HoG_Shadan_Lavrenets = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shadan_Lavrenets_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Nasirdin Isanov
		TAJ_HoG_Nasirdin_Isanov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nasirdin_Isanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Bobodzhan Gafurov
		TAJ_HoG_Bobodzhan_Gafurov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Bobodzhan_Gafurov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Tursun Uldzhabayev
		TAJ_HoG_Tursun_Uldzhabayev = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Tursun_Uldzhabayev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Silent_Workhorse }
		}
	# Nazarsho Dodkhudoyev
		TAJ_HoG_Nazarsho_Dodkhudoyev = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nazarsho_Dodkhudoyev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Political_Protege }
		}
	# Abdulakhad Kakharov
		TAJ_HoG_Abdulakhad_Kakharov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1961.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Abdulakhad_Kakharov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_Admiral }
		}
	# Makhmadullo Kholov
		TAJ_HoG_Makhmadullo_Kholov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1963.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Makhmadullo_Kholov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Smiling_Oilman }
		}
	# Gaibnasar Pallayev
		TAJ_HoG_Gaibnasar_Pallayev = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1964.1.1
				date < 2001.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Gaibnasar_Pallayev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Naive_Optimist }
		}
	# Isatullo Khayoyev
		TAJ_HoG_Isatullo_Khayoyev = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1986.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Isatullo_Khayoyev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Abdullo Rakhimbayev
		TAJ_HoG_Abdullo_Rakhimbayev = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1936.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Abdullo_Rakhimbayev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Silent_Workhorse }
		}
	# Mamdali Kurbanov
		TAJ_HoG_Mamdali_Kurbanov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mamdali_Kurbanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_Admiral }
		}
	# Jabbor Rasulov
		TAJ_HoG_Jabbor_Rasulov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Jabbor_Rasulov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Ambitious_Union_Boss }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Paghahan Asanov
		TAJ_FM_Paghahan_Asanov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Paghahan_Asanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Ideological_Crusader }
		}
	# Freba Pashkov
		TAJ_FM_Freba_Pashkov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Freba_Pashkov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Iron_Fisted_Brute }
		}
	# Feda Ronzin
		TAJ_FM_Feda_Ronzin = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Feda_Ronzin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Garri Abelev
		TAJ_FM_Garri_Abelev = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Garri_Abelev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Biased_Intellectual }
		}
	# Urunboy Ushorov
		TAJ_FM_Urunboy_Ushorov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Urunboy_Ushorov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_General_Staffer }
		}
	# Shirinsho Shotemor
		TAJ_FM_Shirinsho_Shotemor = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Shirinsho_Shotemor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Iron_Fisted_Brute }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Alexander Berkmann
		TAJ_MoS_Alexander_Berkmann = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Alexander_Berkmann_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Prince_Of_Terror }
		}
	# Mahasti Gafurov
		TAJ_MoS_Mahasti_Gafurov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mahasti_Gafurov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Silent_Lawyer }
		}
	# Father Gennadij
		TAJ_MoS_Father_Gennadij = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Father_Gennadij_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Mamtun Kurbanov
		TAJ_MoS_Mamtun_Kurbanov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mamtun_Kurbanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Grigorij Balanchine
		TAJ_MoS_Grigorij_Balanchine = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Grigorij_Balanchine_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Grigorij Broydo
		TAJ_MoS_Grigorij_Broydo = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Grigorij_Broydo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Back_Stabber }
		}
	# Mamdali Kurbanov
		TAJ_MoS_Mamdali_Kurbanov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mamdali_Kurbanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Compassionate_Gentleman }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Ivan Prosit
		TAJ_AM_Ivan_Prosit = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ivan_Prosit_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Air_To_Ground_Proponent }
		}
	# Ilya Chernyaev
		TAJ_AM_Ilya_Chernyaev = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ilya_Chernyaev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Theoretical_Scientist }
		}
	# Zand Korzh
		TAJ_AM_Zand_Korzh = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Zand_Korzh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Naz Gindis
		TAJ_AM_Naz_Gindis = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Naz_Gindis_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Mirza ogly Husseynov
		TAJ_AM_Mirza_ogly_Husseynov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mirza_ogly_Husseynov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Tank_Proponent }
		}
	# Kamshad Genkin
		TAJ_AM_Kamshad_Genkin = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Kamshad_Genkin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Resource_Industrialist }
		}
	# Abdullo Rakhimbayev
		TAJ_AM_Abdullo_Rakhimbayev = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Abdullo_Rakhimbayev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Military_Entrepreneur }
		}
	# Aleksandr Baikov
		TAJ_AM_Aleksandr_Baikov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksandr_Baikov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Theoretical_Scientist }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Valerij Barsukov
		TAJ_HoI_Valerij_Barsukov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Valerij_Barsukov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Technical_Specialist }
		}
	# Feliks Ilyan
		TAJ_HoI_Feliks_Ilyan = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Feliks_Ilyan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Shayan Barishnikov
		TAJ_HoI_Shayan_Barishnikov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shayan_Barishnikov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Igor Balienkov
		TAJ_HoI_Igor_Balienkov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Igor_Balienkov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Technical_Specialist }
		}
	# Suren Shchadunts
		TAJ_HoI_Suren_Shchadunts = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Suren_Shchadunts_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Dismal_Enigma }
		}
	# Mumtoz Raseltov
		TAJ_HoI_Mumtoz_Raseltov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mumtoz_Raseltov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Logistics_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Urunboy Ushorov
		TAJ_CoStaff_Urunboy_Ushorov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Urunboy_Ushorov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Psychology }
		}
	# Yurij Perminov
		TAJ_CoStaff_Yurij_Perminov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Yurij_Perminov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Ara Gafurov
		TAJ_CoStaff_Ara_Gafurov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Ara_Gafurov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Fire_Support }
		}
	# Nikilai Provotvorov
		TAJ_CoStaff_Nikilai_Provotvorov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikilai_Provotvorov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Mass_Combat }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Ara Gafurov
		TAJ_CoArmy_Ara_Gafurov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ara_Gafurov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Nikita Proskuriakov
		TAJ_CoArmy_Nikita_Proskuriakov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nikita_Proskuriakov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Veniamin Protopopov
		TAJ_CoArmy_Veniamin_Protopopov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Veniamin_Protopopov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Decisive_Battle_Doctrine }
		}
	# Neelufar Kazachestevo
		TAJ_CoArmy_Neelufar_Kazachestevo = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Neelufar_Kazachestevo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Urunboy Ushorov
		TAJ_CoNavy_Urunboy_Ushorov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Urunboy_Ushorov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Siamak Sinaiski
		TAJ_CoNavy_Siamak_Sinaiski = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Siamak_Sinaiski_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# Pendar Ryndenko
		TAJ_CoNavy_Pendar_Ryndenko = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Pendar_Ryndenko_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Indirect_Approach_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Walter Surén
		TAJ_CoAir_Walter_Suren = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Walter_Suren_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Carpet_Bombing_Doctrine }
		}
	# Pendar Ryndenko
		TAJ_CoAir_Pendar_Ryndenko = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pendar_Ryndenko_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Mikhail Barsukov
		TAJ_CoAir_Mikhail_Barsukov = {
			picture = Generic_Portrait
			allowed = { tag = TAJ }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mikhail_Barsukov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
