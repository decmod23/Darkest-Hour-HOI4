ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Jacinto Bienvenido Peynado
		DOM_HoG_Jacinto_Bienvenido_Peynado = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jacinto_Bienvenido_Peynado_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Naive_Optimist }
		}
	# Manuel de Jesús Troncoso
		DOM_HoG_Manuel_de_Jesus_Troncoso = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Manuel_de_Jesus_Troncoso_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Silent_Workhorse }
		}
	# Rafél Trujillo
		DOM_HoG_Rafel_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Rafel_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Backroom_Backstabber }
		}
	# Hector Trujillo
		DOM_HoG_Hector_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1952.1.1
				date < 2003.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hector_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_Admiral }
		}
	# Joaquin Balaguer
		DOM_HoG_Joaquin_Balaguer = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1957.1.1
				date < 2003.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Joaquin_Balaguer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Flamboyant_Tough_Guy }
		}
	# Rafael Filiberto Bonnelly
		DOM_HoG_Rafael_Filiberto_Bonnelly = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1960.1.1
				date < 1980.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_Filiberto_Bonnelly_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Political_Protege }
		}
	# Nicolas Pichardo
		DOM_HoG_Nicolas_Pichardo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1962.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Nicolas_Pichardo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# Elias Wessin
		DOM_HoG_Elias_Wessin = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1963.1.1
				date < 2010.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Elias_Wessin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_Airmarshal }
		}
	# Héctor García Godoy
		DOM_HoG_Hector_Garcia_Godoy = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1965.1.1
				date < 1971.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hector_Garcia_Godoy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Smiling_Oilman }
		}
	# Francisco Augusto Lora
		DOM_HoG_Francisco_Augusto_Lora = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1966.1.1
				date < 1994.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_Augusto_Lora_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_Admiral }
		}
	# Alberto Garcia Vienda
		DOM_HoG_Alberto_Garcia_Vienda = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alberto_Garcia_Vienda_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Rafael Espaillat
		DOM_HoG_Rafael_Espaillat = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_Espaillat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Rafael Bonelly Fondeur
		DOM_HoG_Rafael_Bonelly_Fondeur = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_Bonelly_Fondeur_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# José Fernando Caminero
		DOM_HoG_Jose_Fernando_Caminero = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1963.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jose_Fernando_Caminero_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Carlos Rafael Goico
		DOM_HoG_Carlos_Rafael_Goico = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1970.1.1
				date < 1993.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Rafael_Goico_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Carlos Morales Troncoso
		DOM_HoG_Carlos_Morales_Troncoso = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1986.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Morales_Troncoso_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# José Bordas Valdés
		DOM_HoG_Jose_Bordas_Valdes = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1913.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jose_Bordas_Valdes_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Ramón Báez Machado
		DOM_HoG_Ramon_Baez_Machado = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ramon_Baez_Machado_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Francisco Henríquez
		DOM_HoG_Francisco_Henriquez = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_Henriquez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Armando Gonzalez Tamayo
		DOM_HoG_Armando_Gonzalez_Tamayo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1963.1.1
				date < 1965.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Armando_Gonzalez_Tamayo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Juan Isidro Jimenes
		DOM_HoG_Juan_Isidro_Jimenes = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Isidro_Jimenes_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Jacobo Majluta Azar
		DOM_HoG_Jacobo_Majluta_Azar = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1978.1.1
				date < 1997.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacobo_Majluta_Azar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Mario Imbert McGregor
		DOM_HoG_Mario_Imbert_McGregor = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mario_Imbert_McGregor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Juan Bosch
		DOM_HoG_Juan_Bosch = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Bosch_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Manuel Fernandez Marmol
		DOM_HoG_Manuel_Fernandez_Marmol = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1982.1.1
				date < 2010.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Manuel_Fernandez_Marmol_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Salvador Jorge Blanco
		DOM_HoG_Salvador_Jorge_Blanco = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salvador_Jorge_Blanco_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Jesus de Galindez
		DOM_HoG_Jesus_de_Galindez = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Jesus_de_Galindez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Flamboyant_Tough_Guy }
		}
	# Francisco Prats Ramirez
		DOM_HoG_Francisco_Prats_Ramirez = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_Prats_Ramirez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Happy_Amateur }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Elias Brache
		DOM_FM_Elias_Brache = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Elias_Brache_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Ernesto Bonetti-Burgos
		DOM_FM_Ernesto_BonettiBurgos = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ernesto_BonettiBurgos_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Apologetic_Clerk }
		}
	# Julio Ortega Frier
		DOM_FM_Julio_Ortega_Frier = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Julio_Ortega_Frier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_The_Cloak_N_Dagger_Schemer }
		}
	# Arturo Despradel
		DOM_FM_Arturo_Despradel = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Arturo_Despradel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Manuel Peña Battle
		DOM_FM_Manuel_Pena_Battle = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Manuel_Pena_Battle_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_General_Staffer }
		}
	# Jesus de Galindez
		DOM_FM_Jesus_de_Galindez = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jesus_de_Galindez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Emilio de los Santos
		DOM_FM_Emilio_de_los_Santos = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Emilio_de_los_Santos_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# Juan Bosch
		DOM_FM_Juan_Bosch = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Bosch_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# Antonio Leyba Pou
		DOM_FM_Antonio_Leyba_Pou = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Antonio_Leyba_Pou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Ideological_Crusader }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Rafél Trujillo
		DOM_MoS_Rafel_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Rafel_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Prince_Of_Terror }
		}
	# Jacinto Bienvenido Peynado
		DOM_MoS_Jacinto_Bienvenido_Peynado = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jacinto_Bienvenido_Peynado_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crooked_Kleptocrat }
		}
	# Donald Reid Cabral
		DOM_MoS_Donald_Reid_Cabral = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Donald_Reid_Cabral_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Compassionate_Gentleman }
		}
	# Hector Trujillo
		DOM_MoS_Hector_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hector_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crooked_Kleptocrat }
		}
	# Rafael Espaillat
		DOM_MoS_Rafael_Espaillat = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_Espaillat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# Alberto Garcia Vienda
		DOM_MoS_Alberto_Garcia_Vienda = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alberto_Garcia_Vienda_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Antonio Leyba Pou
		DOM_MoS_Antonio_Leyba_Pou = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Antonio_Leyba_Pou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Man_Of_The_People }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Manuel de Jesús Troncoso
		DOM_AM_Manuel_de_Jesus_Troncoso = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Manuel_de_Jesus_Troncoso_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Infantry_Proponent }
		}
	# Ignacio Bajordo
		DOM_AM_Ignacio_Bajordo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ignacio_Bajordo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Administrative_Genius }
		}
	# Rafél Trujillo
		DOM_AM_Rafel_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Rafel_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Corrupt_Kleptocrat }
		}
	# Luis Amiamo Tio
		DOM_AM_Luis_Amiamo_Tio = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Luis_Amiamo_Tio_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Corrupt_Kleptocrat }
		}
	# Jesus de Galindez
		DOM_AM_Jesus_de_Galindez = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jesus_de_Galindez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Rafael Bonelly Fondeur
		DOM_AM_Rafael_Bonelly_Fondeur = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_Bonelly_Fondeur_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Infantry_Proponent }
		}
	# Jorge Balaguer
		DOM_AM_Jorge_Balaguer = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jorge_Balaguer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Administrative_Genius }
		}
	# Francisco Cutricos
		DOM_AM_Francisco_Cutricos = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_Cutricos_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Corrupt_Kleptocrat }
		}
	# Mario Imbert McGregor
		DOM_AM_Mario_Imbert_McGregor = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mario_Imbert_McGregor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Jesus da Costa Ruiz
		DOM_HoI_Jesus_da_Costa_Ruiz = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jesus_da_Costa_Ruiz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Technical_Specialist }
		}
	# Manuel de Jesús Troncoso
		DOM_HoI_Manuel_de_Jesus_Troncoso = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Manuel_de_Jesus_Troncoso_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Naval_Intelligence_Specialist }
		}
	# Jorge Balaguer
		DOM_HoI_Jorge_Balaguer = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jorge_Balaguer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Hector Trujillo
		DOM_HoI_Hector_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hector_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Alberto Garcia Vienda
		DOM_HoI_Alberto_Garcia_Vienda = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alberto_Garcia_Vienda_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Mario Imbert McGregor
		DOM_HoI_Mario_Imbert_McGregor = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mario_Imbert_McGregor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Francisco Prats Ramirez
		DOM_HoI_Francisco_Prats_Ramirez = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_Prats_Ramirez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Rafél Trujillo
		DOM_CoStaff_Rafel_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Rafel_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Manuel Peña Battle
		DOM_CoStaff_Manuel_Pena_Battle = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Manuel_Pena_Battle_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Jesus da Costa-Ruiz
		DOM_CoStaff_Jesus_da_CostaRuiz = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jesus_da_CostaRuiz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Victor Viñas Roman
		DOM_CoStaff_Victor_Vinas_Roman = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Vinas_Roman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Rafél Trujillo
		DOM_CoArmy_Rafel_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Rafel_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Domingo de los Santos Ortiz
		DOM_CoArmy_Domingo_de_los_Santos_Ortiz = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Domingo_de_los_Santos_Ortiz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Victor Viñas Roman
		DOM_CoArmy_Victor_Vinas_Roman = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Vinas_Roman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Rafél Trujillo
		DOM_CoNavy_Rafel_Trujillo = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1930.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Rafel_Trujillo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Base_Control_Doctrine }
		}
	# Humberto Rojas
		DOM_CoNavy_Humberto_Rojas = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Humberto_Rojas_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Indirect_Approach_Doctrine }
		}
	# Honoracio Yuna Robles
		DOM_CoNavy_Honoracio_Yuna_Robles = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Honoracio_Yuna_Robles_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Open_Seas_Doctrine }
		}
	# Edgar Martinez
		DOM_CoNavy_Edgar_Martinez = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Edgar_Martinez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Indirect_Approach_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Anibal Vallejo Sosa
		DOM_CoAir_Anibal_Vallejo_Sosa = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Anibal_Vallejo_Sosa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Air_Superiority_Doctrine }
		}
	# Julio Ortega Frier
		DOM_CoAir_Julio_Ortega_Frier = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Julio_Ortega_Frier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# Diego Trabajo Rúiz
		DOM_CoAir_Diego_Trabajo_Ruiz = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Diego_Trabajo_Ruiz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# Frank Feliz Miranda
		DOM_CoAir_Frank_Feliz_Miranda = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Frank_Feliz_Miranda_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Naval_Aviation_Doctrine }
		}
	# Edgar Martinez
		DOM_CoAir_Edgar_Martinez = {
			picture = Generic_Portrait
			allowed = { tag = DOM }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Edgar_Martinez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Air_Superiority_Doctrine }
		}
	}
}
