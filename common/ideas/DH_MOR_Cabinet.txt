ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Sidi Mohamed bin Mohamed
		MOR_HoG_Sidi_Mohamed_bin_Mohamed = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sidi_Mohamed_bin_Mohamed_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Political_Protege }
		}
	# Muhammed V
		MOR_HoG_Muhammed_V = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1960.1.1
				date < 1961.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Muhammed_V_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Ambitious_Union_Boss }
		}
	# Hassan II
		MOR_HoG_Hassan_II = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1961.1.1
				date < 2000.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hassan_II_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_Airmarshal }
		}
	# Mohamed Benhima
		MOR_HoG_Mohamed_Benhima = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1967.1.1
				date < 1993.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mohamed_Benhima_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_Airmarshal }
		}
	# Ahmed Laraki
		MOR_HoG_Ahmed_Laraki = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1969.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmed_Laraki_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Silent_Workhorse }
		}
	# Mohammed Karim Lamrani
		MOR_HoG_Mohammed_Karim_Lamrani = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1971.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_Karim_Lamrani_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Political_Protege }
		}
	# Ahmed Osman
		MOR_HoG_Ahmed_Osman = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1972.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmed_Osman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Corporate_Suit }
		}
	# Maati Bouabid
		MOR_HoG_Maati_Bouabid = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1979.1.1
				date < 1997.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Maati_Bouabid_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Corporate_Suit }
		}
	# Azzeddine Laraki
		MOR_HoG_Azzeddine_Laraki = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1986.1.1
				date < 2011.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Azzeddine_Laraki_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Flamboyant_Tough_Guy }
		}
	# Marcel Peyrouton
		MOR_HoG_Marcel_Peyrouton = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Marcel_Peyrouton_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Charles Nogues
		MOR_HoG_Charles_Nogues = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Nogues_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Charles Hippolyte Nogues
		MOR_HoG_Charles_Hippolyte_Nogues = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Hippolyte_Nogues_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Gabriel Puaux
		MOR_HoG_Gabriel_Puaux = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gabriel_Puaux_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Gabriel Puaux
		MOR_HoG_Gabriel_Puaux_2 = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gabriel_Puaux_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Erik Labonne
		MOR_HoG_Erik_Labonne = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Erik_Labonne_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Eirik Labonne
		MOR_HoG_Eirik_Labonne = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Eirik_Labonne_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Alphonse Pierre Juin
		MOR_HoG_Alphonse_Pierre_Juin = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alphonse_Pierre_Juin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Admiral }
		}
	# Augustin Léon Guillaume
		MOR_HoG_Augustin_Leon_Guillaume = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Augustin_Leon_Guillaume_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Francis Lacoste
		MOR_HoG_Francis_Lacoste = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francis_Lacoste_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# André Louis Dubois
		MOR_HoG_Andre_Louis_Dubois = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Andre_Louis_Dubois_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Mbarek Bekkai
		MOR_HoG_Mbarek_Bekkai = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mbarek_Bekkai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Ahmed Balafrej
		MOR_HoG_Ahmed_Balafrej = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1958.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmed_Balafrej_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Abdallah Ibrahim
		MOR_HoG_Abdallah_Ibrahim = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1959.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdallah_Ibrahim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Abderrahmane Youssoufi
		MOR_HoG_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Allal el-Fassi
		MOR_HoG_Allal_elFassi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Allal_elFassi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Flamboyant_Tough_Guy }
		}
	# Mehdi ben-Barka
		MOR_HoG_Mehdi_benBarka = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mehdi_benBarka_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Smiling_Oilman }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Ahmed Belafrej
		MOR_FM_Ahmed_Belafrej = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmed_Belafrej_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Biased_Intellectual }
		}
	# Abdallah Ibrahim
		MOR_FM_Abdallah_Ibrahim = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdallah_Ibrahim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
	# Abdellatif Filali
		MOR_FM_Abdellatif_Filali = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdellatif_Filali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Abderrahmane Youssoufi
		MOR_FM_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Sana Madiha
		MOR_FM_Sana_Madiha = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Sana_Madiha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_The_Cloak_N_Dagger_Schemer }
		}
	# Mehdi ben-Barka
		MOR_FM_Mehdi_benBarka = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mehdi_benBarka_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Apologetic_Clerk }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# HM Hassan II
		MOR_MoS_HM_Hassan_II = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 2000.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hassan_II_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Back_Stabber }
		}
	# Francis-Louis Closon
		MOR_MoS_FrancisLouis_Closon = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = FrancisLouis_Closon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Edouard Corniglion-Molinier
		MOR_MoS_Edouard_CorniglionMolinier = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edouard_CorniglionMolinier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Mohammed Oufkir
		MOR_MoS_Mohammed_Oufkir = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_Oufkir_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# Abderrahmane Youssoufi
		MOR_MoS_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# M'Barak Bekkar
		MOR_MoS_MBarak_Bekkar = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = MBarak_Bekkar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# HM Hassan II
		MOR_AM_HM_Hassan_II = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 2000.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hassan_II_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Military_Entrepreneur }
		}
	# Emile Hugues
		MOR_AM_Emile_Hugues = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Emile_Hugues_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Erik Labonne
		MOR_AM_Erik_Labonne = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Erik_Labonne_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Mehdi ben-Barka
		MOR_AM_Mehdi_benBarka = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mehdi_benBarka_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_Superiority_Proponent }
		}
	# Abderrahmane Youssoufi
		MOR_AM_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Administrative_Genius }
		}
	# René Cassin
		MOR_AM_Rene_Cassin = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Rene_Cassin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Air_To_Ground_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# HM Hassan II
		MOR_HoI_HM_Hassan_II = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 2000.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hassan_II_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Dismal_Enigma }
		}
	# René Cassin
		MOR_HoI_Rene_Cassin = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rene_Cassin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Allal el-Fassi
		MOR_HoI_Allal_elFassi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Allal_elFassi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# M'Barak Bekkar
		MOR_HoI_MBarak_Bekkar = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = MBarak_Bekkar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Abderrahmane Youssoufi
		MOR_HoI_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Ahmed Dlimi
		MOR_HoI_Ahmed_Dlimi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmed_Dlimi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Logistics_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# HM Hassan II
		MOR_CoStaff_HM_Hassan_II = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 2000.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hassan_II_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Manoeuvre }
		}
	# Theophile la Guillin
		MOR_CoStaff_Theophile_la_Guillin = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Theophile_la_Guillin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Jacques de la Bollardiere
		MOR_CoStaff_Jacques_de_la_Bollardiere = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacques_de_la_Bollardiere_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Mahfoud Gharbaoui
		MOR_CoStaff_Mahfoud_Gharbaoui = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mahfoud_Gharbaoui_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Abderrahmane Youssoufi
		MOR_CoStaff_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Islem Hadjira
		MOR_CoStaff_Islem_Hadjira = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Islem_Hadjira_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Mass_Combat }
		}
	# Mahfoud Gharbaoui
		MOR_CoStaff_Mahfoud_Gharbaoui_2 = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mahfoud_Gharbaoui_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Psychology }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# HM Hassan II
		MOR_CoArmy_HM_Hassan_II = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 2000.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hassan_II_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Decisive_Battle_Doctrine }
		}
	# Albert Guillaume
		MOR_CoArmy_Albert_Guillaume = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Albert_Guillaume_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Georges Jeanperrin
		MOR_CoArmy_Georges_Jeanperrin = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Georges_Jeanperrin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Abdallah Nmichi
		MOR_CoArmy_Abdallah_Nmichi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdallah_Nmichi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# Abderrahmane Youssoufi
		MOR_CoArmy_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Islem Hadjira
		MOR_CoArmy_Islem_Hadjira = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Islem_Hadjira_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Elastic_Defence_Doctrine }
		}
	# Abdallah Nmichi
		MOR_CoArmy_Abdallah_Nmichi_2 = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Abdallah_Nmichi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Static_Defence_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# HM Hassan II
		MOR_CoNavy_HM_Hassan_II = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 2000.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hassan_II_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Georges Cabanier
		MOR_CoNavy_Georges_Cabanier = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Georges_Cabanier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Djamel Abdelmounem
		MOR_CoNavy_Djamel_Abdelmounem = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Djamel_Abdelmounem_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# Abderrahmane Youssoufi
		MOR_CoNavy_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# René Cassin
		MOR_CoNavy_Rene_Cassin = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Rene_Cassin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Base_Control_Doctrine }
		}
	# Djamel Abdelmounem
		MOR_CoNavy_Djamel_Abdelmounem_2 = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Djamel_Abdelmounem_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Power_Projection_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# HM Hassan II
		MOR_CoAir_HM_Hassan_II = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 2000.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hassan_II_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoAir_Army_Aviation_Doctrine }
		}
	# Edouard Corniglion-Molinier
		MOR_CoAir_Edouard_CorniglionMolinier = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edouard_CorniglionMolinier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Christian Martell
		MOR_CoAir_Christian_Martell = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Christian_Martell_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# Fawzi Joumana
		MOR_CoAir_Fawzi_Joumana = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_Joumana_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# Abderrahmane Youssoufi
		MOR_CoAir_Abderrahmane_Youssoufi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abderrahmane_Youssoufi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Ahmed Dlimi
		MOR_CoAir_Ahmed_Dlimi = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmed_Dlimi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Carpet_Bombing_Doctrine }
		}
	# Fawzi Joumana
		MOR_CoAir_Fawzi_Joumana_2 = {
			picture = Generic_Portrait
			allowed = { tag = MOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_Joumana_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
