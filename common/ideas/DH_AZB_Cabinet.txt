ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Fath Ali Khan Khoiski
		AZB_HoG_Fath_Ali_Khan_Khoiski = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Fath_Ali_Khan_Khoiski_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Nasibbey Yusifbeyli
		AZB_HoG_Nasibbey_Yusifbeyli = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1919.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nasibbey_Yusifbeyli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Arash Bulbul
		AZB_HoG_Arash_Bulbul = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arash_Bulbul_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Suleyman Rustam
		AZB_HoG_Suleyman_Rustam = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Suleyman_Rustam_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Hasan Hasanov
		AZB_HoG_Hasan_Hasanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hasan_Hasanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Faraz Guzeinov
		AZB_HoG_Faraz_Guzeinov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Faraz_Guzeinov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Backroom_Backstabber }
		}
	# Sadykh Ragimov
		AZB_HoG_Sadykh_Ragimov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Sadykh_Ragimov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Corporate_Suit }
		}
	# Veli Akhdunov
		AZB_HoG_Veli_Akhdunov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1958.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Veli_Akhdunov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Naive_Optimist }
		}
	# Mamed Iskenderov
		AZB_HoG_Mamed_Iskenderov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1959.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mamed_Iskenderov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Corporate_Suit }
		}
	# Enver Alikhanov
		AZB_HoG_Enver_Alikhanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1961.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Enver_Alikhanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_Airmarshal }
		}
	# Ali Ibrahimov
		AZB_HoG_Ali_Ibrahimov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1970.1.1
				date < 1981.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Ibrahimov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_General }
		}
	# Hasan Seyidov
		AZB_HoG_Hasan_Seyidov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1981.1.1
				date < 1991.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Hasan_Seyidov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_General }
		}
	# Ayaz Mutallibov
		AZB_HoG_Ayaz_Mutallibov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1989.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Ayaz_Mutallibov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_Admiral }
		}
	# Mirza Husseynov
		AZB_HoG_Mirza_Husseynov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1920.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mirza_Husseynov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Backroom_Backstabber }
		}
	# Narim Narimanov
		AZB_HoG_Narim_Narimanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1921.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Narim_Narimanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Political_Protege }
		}
	# Mukhtar Hajiyev
		AZB_HoG_Mukhtar_Hajiyev = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1921.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mukhtar_Hajiyev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Samad Aliyev
		AZB_HoG_Samad_Aliyev = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1922.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Samad_Aliyev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Backroom_Backstabber }
		}
	# Huseyn Rakhmanov
		AZB_HoG_Huseyn_Rakhmanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1936.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Huseyn_Rakhmanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Political_Protege }
		}
	# Teymur Kuliyev
		AZB_HoG_Teymur_Kuliyev = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Teymur_Kuliyev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_Admiral }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Mahammad Rahman
		AZB_FM_Mahammad_Rahman = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mahammad_Rahman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# Alimardanbey Topchubashov
		AZB_FM_Alimardanbey_Topchubashov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alimardanbey_Topchubashov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Biased_Intellectual }
		}
	# Arash Bulbul
		AZB_FM_Arash_Bulbul = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arash_Bulbul_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Hasan Hasanov
		AZB_FM_Hasan_Hasanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hasan_Hasanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Suleyman Rustam
		AZB_FM_Suleyman_Rustam = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Suleyman_Rustam_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_The_Cloak_N_Dagger_Schemer }
		}
	# Khaslatam Narimanov
		AZB_FM_Khaslatam_Narimanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Khaslatam_Narimanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_General_Staffer }
		}
	# Mammad Amin Rasulzade
		AZB_FM_Mammad_Amin_Rasulzade = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mammad_Amin_Rasulzade_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Biased_Intellectual }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Mirza Ibrahimov
		AZB_MoS_Mirza_Ibrahimov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mirza_Ibrahimov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Mahammad Asadov
		AZB_MoS_Mahammad_Asadov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mahammad_Asadov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Amir Aslan Khan Khoiski
		AZB_MoS_Amir_Aslan_Khan_Khoiski = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Amir_Aslan_Khan_Khoiski_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# Rasul Reza
		AZB_MoS_Rasul_Reza = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rasul_Reza_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Arash Bulbul
		AZB_MoS_Arash_Bulbul = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Arash_Bulbul_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Compassionate_Gentleman }
		}
	# Nigar Rafibelyi
		AZB_MoS_Nigar_Rafibelyi = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Nigar_Rafibelyi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Crime_Fighter }
		}
	# Sabat Rahman
		AZB_MoS_Sabat_Rahman = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Sabat_Rahman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Silent_Lawyer }
		}
	# Narim Narimanov
		AZB_MoS_Narim_Narimanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1921.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Narim_Narimanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Crime_Fighter }
		}
	# Gazanfar Musabekov
		AZB_MoS_Gazanfar_Musabekov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1922.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Gazanfar_Musabekov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Secret_Police_Chief }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Ismayl Shikhali
		AZB_AM_Ismayl_Shikhali = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ismayl_Shikhali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Valeh Barshadly
		AZB_AM_Valeh_Barshadly = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Valeh_Barshadly_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Rajib Kulbenzhian
		AZB_AM_Rajib_Kulbenzhian = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rajib_Kulbenzhian_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Corrupt_Kleptocrat }
		}
	# Ilyas Afandiev
		AZB_AM_Ilyas_Afandiev = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Ilyas_Afandiev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Theoretical_Scientist }
		}
	# Mehdi Husein
		AZB_AM_Mehdi_Husein = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mehdi_Husein_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Tank_Proponent }
		}
	# dr. Mahmood Ghasbalatam
		AZB_AM_dr_Mahmood_Ghasbalatam = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = dr_Mahmood_Ghasbalatam_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Laissez-faire_Capitalist }
		}
	# Amin Aziz
		AZB_AM_Amin_Aziz = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Amin_Aziz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Infantry_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Rahmud Azizbayob
		AZB_HoI_Rahmud_Azizbayob = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rahmud_Azizbayob_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Mahammad Asadov
		AZB_HoI_Mahammad_Asadov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mahammad_Asadov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Soultanov Khosrov Ebk
		AZB_HoI_Soultanov_Khosrov_Ebk = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Soultanov_Khosrov_Ebk_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Rasul Reza
		AZB_HoI_Rasul_Reza = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Rasul_Reza_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Industrial_Specialist }
		}
	# Nariman Narimanov
		AZB_HoI_Nariman_Narimanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Nariman_Narimanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Naval_Intelligence_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Khalil Mamedov
		AZB_CoStaff_Khalil_Mamedov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Khalil_Mamedov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Safar Abiyev
		AZB_CoStaff_Safar_Abiyev = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Safar_Abiyev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Samed Bey Mehmandarov
		AZB_CoStaff_Samed_Bey_Mehmandarov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Samed_Bey_Mehmandarov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Mahmud Nuri Berköz
		AZB_CoStaff_Mahmud_Nuri_Berkoz = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mahmud_Nuri_Berkoz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Mass_Combat }
		}
	# Mirza Husseynov
		AZB_CoStaff_Mirza_Husseynov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1920.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mirza_Husseynov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Defence }
		}
	# Narim Narimanov
		AZB_CoStaff_Narim_Narimanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1920.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Narim_Narimanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Psychology }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Soureb Hakop Gasparyan
		AZB_CoArmy_Soureb_Hakop_Gasparyan = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Soureb_Hakop_Gasparyan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Safar Abiyev
		AZB_CoArmy_Safar_Abiyev = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Safar_Abiyev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Mahmud Nuri Berköz
		AZB_CoArmy_Mahmud_Nuri_Berkoz = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mahmud_Nuri_Berkoz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Khalil Mamedov
		AZB_CoArmy_Khalil_Mamedov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Khalil_Mamedov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Elastic_Defence_Doctrine }
		}
	# Mirza Husseynov
		AZB_CoArmy_Mirza_Husseynov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1920.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mirza_Husseynov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Static_Defence_Doctrine }
		}
	# Narim Narimanov
		AZB_CoArmy_Narim_Narimanov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1920.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Narim_Narimanov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Decisive_Battle_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Ysmail Makhov
		AZB_CoNavy_Ysmail_Makhov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ysmail_Makhov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Rafig Asgarov
		AZB_CoNavy_Rafig_Asgarov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rafig_Asgarov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Samed Bey Mehmandarov
		AZB_CoNavy_Samed_Bey_Mehmandarov = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Samed_Bey_Mehmandarov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Kambiz Krasnoshapka
		AZB_CoNavy_Kambiz_Krasnoshapka = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Kambiz_Krasnoshapka_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Open_Seas_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Andronik Abram Kazaryan
		AZB_CoAir_Andronik_Abram_Kazaryan = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Andronik_Abram_Kazaryan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Rail Rzayev
		AZB_CoAir_Rail_Rzayev = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1980.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rail_Rzayev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Kambiz Krasnoshapka
		AZB_CoAir_Kambiz_Krasnoshapka = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Kambiz_Krasnoshapka_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Naval_Aviation_Doctrine }
		}
	# Gurgen Voskan Kamalyan
		AZB_CoAir_Gurgen_Voskan_Kamalyan = {
			picture = Generic_Portrait
			allowed = { tag = AZB }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Gurgen_Voskan_Kamalyan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Air_Superiority_Doctrine }
		}
	}
}
