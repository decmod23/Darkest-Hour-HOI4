ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Hjalmar Mäe
		EST_HoG_Hjalmar_Mae = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Hjalmar_Mae_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Flamboyant_Tough_Guy }
		}
	# Konstantin Päts
		EST_HoG_Konstantin_Pats = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1921.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Konstantin_Pats_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# Kaarel Eenpalu
		EST_HoG_Kaarel_Eenpalu = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kaarel_Eenpalu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Ambitious_Union_Boss }
		}
	# Jüri Uluots
		EST_HoG_Juri_Uluots_2 = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Juri_Uluots_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Smiling_Oilman }
		}
	# Jaan Tõnisson
		EST_HoG_Jaan_Tonisson = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1927.1.1
				date < 1940.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jaan_Tonisson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Johannes Sikkar
		EST_HoG_Johannes_Sikkar = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_Sikkar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Tonis Kint
		EST_HoG_Tonis_Kint = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1960.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tonis_Kint_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Aleksander Warma
		EST_HoG_Aleksander_Warma = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1962.1.1
				date < 1971.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksander_Warma_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Tonis Kint
		EST_HoG_Tonis_Kint_2 = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1963.1.1
				date < 1992.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tonis_Kint_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Heinrich Mark
		EST_HoG_Heinrich_Mark = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1971.1.1
				date < 2005.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Heinrich_Mark_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Jüri Uluots
		EST_HoG_Juri_Uluots = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juri_Uluots_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Otto Tief
		EST_HoG_Otto_Tief = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Otto_Tief_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Kaarel Liidak
		EST_HoG_Kaarel_Liidak = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Kaarel_Liidak_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Edgar Savisaar
		EST_HoG_Edgar_Savisaar = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edgar_Savisaar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Harald Ilves
		EST_HoG_Harald_Ilves = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1959.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Harald_Ilves_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Vaino Väljas
		EST_HoG_Vaino_Valjas = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1963.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vaino_Valjas_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Naive_Optimist }
		}
	# Arnold Koop
		EST_HoG_Arnold_Koop = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1967.1.1
				date < 1989.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Arnold_Koop_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Happy_Amateur }
		}
	# Ilmar Vahe
		EST_HoG_Ilmar_Vahe = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1968.1.1
				date < 1977.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Ilmar_Vahe_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Political_Protege }
		}
	# Johannes Lott
		EST_HoG_Johannes_Lott = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1975.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_Lott_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_Admiral }
		}
	# Jüri Suurhans
		EST_HoG_Juri_Suurhans = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1978.1.1
				date < 1985.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Juri_Suurhans_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Political_Protege }
		}
	# Matti Pedak
		EST_HoG_Matti_Pedak = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1982.1.1
				date < 1991.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Matti_Pedak_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_General }
		}
	# Valde Roosmaa
		EST_HoG_Valde_Roosmaa = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Valde_Roosmaa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Enn-Arno Sillari
		EST_HoG_EnnArno_Sillari = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1989.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = EnnArno_Sillari_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_Admiral }
		}
	# Johannes Vares-Barbaruse
		EST_HoG_Johannes_VaresBarbaruse = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_VaresBarbaruse_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Silent_Workhorse }
		}
	# Johannes Lauristin
		EST_HoG_Johannes_Lauristin = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_Lauristin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Happy_Amateur }
		}
	# Oskar Sepre
		EST_HoG_Oskar_Sepre = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Oskar_Sepre_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Silent_Workhorse }
		}
	# Arnold Veimer
		EST_HoG_Arnold_Veimer = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Arnold_Veimer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Naive_Optimist }
		}
	# August Kründel
		EST_HoG_August_Krundel = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = August_Krundel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Political_Protege }
		}
	# Otto Merimaa
		EST_HoG_Otto_Merimaa = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Otto_Merimaa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Political_Protege }
		}
	# Joosep Saat
		EST_HoG_Joosep_Saat = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Joosep_Saat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Ambitious_Union_Boss }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Hjalmar Mäe
		EST_FM_Hjalmar_Mae = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Hjalmar_Mae_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_Iron_Fisted_Brute }
		}
	# Julius Seljamaa
		EST_FM_Julius_Seljamaa = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Julius_Seljamaa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_The_Cloak_N_Dagger_Schemer }
		}
	# Ants Piip
		EST_FM_Ants_Piip = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ants_Piip_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Friedrich Karl Akel
		EST_FM_Friedrich_Karl_Akel = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Friedrich_Karl_Akel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_General_Staffer }
		}
	# Karl Selter
		EST_FM_Karl_Selter = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Karl_Selter_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Biased_Intellectual }
		}
	# Trivimi Velliste
		EST_FM_Trivimi_Velliste = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Trivimi_Velliste_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# August Rei
		EST_FM_August_Rei = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = August_Rei_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Kaarel Liidak
		EST_FM_Kaarel_Liidak = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Kaarel_Liidak_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# Johannes Lauristin
		EST_FM_Johannes_Lauristin = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_Lauristin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Apologetic_Clerk }
		}
	# Nigol Andresen
		EST_FM_Nigol_Andresen = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nigol_Andresen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Iron_Fisted_Brute }
		}
	# Hans Kruus
		EST_FM_Hans_Kruus = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Hans_Kruus_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Ideological_Crusader }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Oskar Angelus
		EST_MoS_Oskar_Angelus = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Oskar_Angelus_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Prince_Of_Terror }
		}
	# Karl Sigismund Litzmann
		EST_MoS_Karl_Sigismund_Litzmann = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Karl_Sigismund_Litzmann_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Back_Stabber }
		}
	# Kaarel Eenpalu
		EST_MoS_Kaarel_Eenpalu = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kaarel_Eenpalu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Julius Seljamaa
		EST_MoS_Julius_Seljamaa = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Julius_Seljamaa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Prince_Of_Terror }
		}
	# Richard Veermaa
		EST_MoS_Richard_Veermaa = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Richard_Veermaa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# August Jürima
		EST_MoS_August_Jurima = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = August_Jurima_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crooked_Kleptocrat }
		}
	# Johannes Klesment
		EST_MoS_Johannes_Klesment = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_Klesment_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Lagle Parek
		EST_MoS_Lagle_Parek = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lagle_Parek_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Andrei Murro
		EST_MoS_Andrei_Murro = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Andrei_Murro_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Man_Of_The_People }
		}
	# Maksim Unt
		EST_MoS_Maksim_Unt = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Maksim_Unt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Prince_Of_Terror }
		}
	# Boris Kumm
		EST_MoS_Boris_Kumm = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Boris_Kumm_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Efficient_Sociopath }
		}
	# Aleksander Rusev
		EST_MoS_Aleksander_Rusev = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksander_Rusev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Back_Stabber }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Alfred Vendt
		EST_AM_Alfred_Vendt = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Alfred_Vendt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Military_Entrepreneur }
		}
	# Karl Selter
		EST_AM_Karl_Selter = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Karl_Selter_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Administrative_Genius }
		}
	# Otto Tief
		EST_AM_Otto_Tief = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Otto_Tief_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Corrupt_Kleptocrat }
		}
	# Konstantin Päts
		EST_AM_Konstantin_Pats = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Konstantin_Pats_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Administrative_Genius }
		}
	# Leo Sepp
		EST_AM_Leo_Sepp = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leo_Sepp_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Hugo Pärtelpoeg
		EST_AM_Hugo_Partelpoeg = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hugo_Partelpoeg_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Siim Kallas
		EST_AM_Siim_Kallas = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Siim_Kallas_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Juhan Narma-Nihtig
		EST_AM_Juhan_NarmaNihtig = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Juhan_NarmaNihtig_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Resource_Industrialist }
		}
	# Paul Keerdo
		EST_AM_Paul_Keerdo = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Keerdo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Corrupt_Kleptocrat }
		}
	# Nikolaus Pilankor
		EST_AM_Nikolaus_Pilankor = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolaus_Pilankor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Military_Entrepreneur }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Oskar Angelus
		EST_HoI_Oskar_Angelus = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Oskar_Angelus_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Logistics_Specialist }
		}
	# Kaarel Eenpalu
		EST_HoI_Kaarel_Eenpalu = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kaarel_Eenpalu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Technical_Specialist }
		}
	# Jüri Uluots
		EST_HoI_Juri_Uluots = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juri_Uluots_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# August Rei
		EST_HoI_August_Rei = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = August_Rei_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Lagle Parek
		EST_HoI_Lagle_Parek = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lagle_Parek_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Oskar Sepre
		EST_HoI_Oskar_Sepre = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Oskar_Sepre_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Dismal_Enigma }
		}
	# Johannes Vares-Barbaruse
		EST_HoI_Johannes_VaresBarbaruse = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_VaresBarbaruse_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Political_Specialist }
		}
	# Arnold Veimer
		EST_HoI_Arnold_Veimer = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Arnold_Veimer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Technical_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Johannes Soodla
		EST_CoStaff_Johannes_Soodla = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_Soodla_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Psychology }
		}
	# Nikolai Reek
		EST_CoStaff_Nikolai_Reek = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolai_Reek_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Alexander Jaakson
		EST_CoStaff_Alexander_Jaakson = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alexander_Jaakson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Johan Holberg
		EST_CoStaff_Johan_Holberg = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Johan_Holberg_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Ants Laaneots
		EST_CoStaff_Ants_Laaneots = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ants_Laaneots_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Nikolay G. Karotamm
		EST_CoStaff_Nikolay_G_Karotamm = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_G_Karotamm_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Mass_Combat }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Juhan Tuuling
		EST_CoArmy_Juhan_Tuuling = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Juhan_Tuuling_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Decisive_Battle_Doctrine }
		}
	# Alfons Rebane
		EST_CoArmy_Alfons_Rebane = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Alfons_Rebane_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Jaan Soots
		EST_CoArmy_Jaan_Soots = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1918.1.1
				date < 1942.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jaan_Soots_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Johan Laidoner
		EST_CoArmy_Johan_Laidoner = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Johan_Laidoner_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Richard Veermaa
		EST_CoArmy_Richard_Veermaa = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Richard_Veermaa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Ants Laaneots
		EST_CoArmy_Ants_Laaneots = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ants_Laaneots_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Nikolay G. Karotamm
		EST_CoArmy_Nikolay_G_Karotamm = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_G_Karotamm_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Decisive_Battle_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Alfred Vendt
		EST_CoNavy_Alfred_Vendt = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Alfred_Vendt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoNavy_Indirect_Approach_Doctrine }
		}
	# Voldemar Mere
		EST_CoNavy_Voldemar_Mere = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Voldemar_Mere_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Open_Seas_Doctrine }
		}
	# Alex Nirk
		EST_CoNavy_Alex_Nirk = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alex_Nirk_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# Ants Laaneots
		EST_CoNavy_Ants_Laaneots = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ants_Laaneots_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Johannes Vares-Barbaruse
		EST_CoNavy_Johannes_VaresBarbaruse = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_VaresBarbaruse_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Power_Projection_Doctrine }
		}
	# Oskar Eiriksson
		EST_CoNavy_Oskar_Eiriksson = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Oskar_Eiriksson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Open_Seas_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Juhan Tuuling
		EST_CoAir_Juhan_Tuuling = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Juhan_Tuuling_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoAir_Army_Aviation_Doctrine }
		}
	# Richard Tomberg
		EST_CoAir_Richard_Tomberg = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Richard_Tomberg_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# Leo Sepp
		EST_CoAir_Leo_Sepp = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leo_Sepp_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# Ants Laaneots
		EST_CoAir_Ants_Laaneots = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ants_Laaneots_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Oskar Eiriksson
		EST_CoAir_Oskar_Eiriksson = {
			picture = Generic_Portrait
			allowed = { tag = EST }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Oskar_Eiriksson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Carpet_Bombing_Doctrine }
		}
	}
}
