ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Ferdinand J.L. Rougier
		MAL_HoG_Ferdinand_JL_Rougier = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ferdinand_JL_Rougier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Backroom_Backstabber }
		}
	# Yoro Diakité
		MAL_HoG_Yoro_Diakite = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1968.1.1
				date < 1974.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Yoro_Diakite_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Corporate_Suit }
		}
	# Moussa Traoré
		MAL_HoG_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1969.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_General }
		}
	# Mamadou Dembelé
		MAL_HoG_Mamadou_Dembele = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1986.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Mamadou_Dembele_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Backroom_Backstabber }
		}
	# Soumana Sacko
		MAL_HoG_Soumana_Sacko = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1991.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Soumana_Sacko_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Oswald Durand
		MAL_HoG_Oswald_Durand = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Oswald_Durand_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Matteo Alfassa
		MAL_HoG_Matteo_Alfassa = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1936.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Matteo_Alfassa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Ferdinand J.L. Rougier
		MAL_HoG_Ferdinand_JL_Rougier_2 = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ferdinand_JL_Rougier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Jean-Louis Beyries
		MAL_HoG_JeanLouis_Beyries = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JeanLouis_Beyries_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Jean Desanti
		MAL_HoG_Jean_Desanti = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Desanti_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Auguste Calvel
		MAL_HoG_Auguste_Calvel = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Auguste_Calvel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Christian Laigret
		MAL_HoG_Christian_Laigret = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Christian_Laigret_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Henry de Mauduit
		MAL_HoG_Henry_de_Mauduit = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_de_Mauduit_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Edmond Louveau
		MAL_HoG_Edmond_Louveau = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edmond_Louveau_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Salvador Jean Etchéber
		MAL_HoG_Salvador_Jean_Etcheber = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salvador_Jean_Etcheber_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Albert Jean Mouragues
		MAL_HoG_Albert_Jean_Mouragues = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Albert_Jean_Mouragues_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Lucien Eugene Geay
		MAL_HoG_Lucien_Eugene_Geay = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lucien_Eugene_Geay_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Henri Victor Gipoulon
		MAL_HoG_Henri_Victor_Gipoulon = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Victor_Gipoulon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Jean-Marie Koné
		MAL_HoG_JeanMarie_Kone = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1958.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JeanMarie_Kone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Modibo Keita
		MAL_HoG_Modibo_Keita = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1959.1.1
				date < 1978.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Modibo_Keita_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Moktar Ould Daddah
		MAL_HoG_Moktar_Ould_Daddah = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Moktar_Ould_Daddah_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Silent_Workhorse }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Jean-Louis Beyries
		MAL_FM_JeanLouis_Beyries = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = JeanLouis_Beyries_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_General_Staffer }
		}
	# Moussa Traoré
		MAL_FM_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Iron_Fisted_Brute }
		}
	# Ferdinand J.L. Rougier
		MAL_FM_Ferdinand_JL_Rougier = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ferdinand_JL_Rougier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# Jean Desanti
		MAL_FM_Jean_Desanti = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Desanti_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Biased_Intellectual }
		}
	# Auguste Calvel
		MAL_FM_Auguste_Calvel = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Auguste_Calvel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Modibo Keita
		MAL_FM_Modibo_Keita = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Modibo_Keita_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Apologetic_Clerk }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Oswald Durand
		MAL_MoS_Oswald_Durand = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Oswald_Durand_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Back_Stabber }
		}
	# Fily Dabo Sissoko
		MAL_MoS_Fily_Dabo_Sissoko = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Fily_Dabo_Sissoko_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Moussa Traoré
		MAL_MoS_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Compassionate_Gentleman }
		}
	# Martin Cayla
		MAL_MoS_Martin_Cayla = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Martin_Cayla_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Jean Clouet des Pesruches
		MAL_MoS_Jean_Clouet_des_Pesruches = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Clouet_des_Pesruches_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Paul Ribeyre
		MAL_MoS_Paul_Ribeyre = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Ribeyre_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# Mamadou Konaté
		MAL_MoS_Mamadou_Konate = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mamadou_Konate_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Christian Laigret
		MAL_AM_Christian_Laigret = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Christian_Laigret_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Air_Superiority_Proponent }
		}
	# Moussa Traoré
		MAL_AM_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Theoretical_Scientist }
		}
	# Pierre Boisson
		MAL_AM_Pierre_Boisson = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Boisson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Theoretical_Scientist }
		}
	# Maurice Guillaudot
		MAL_AM_Maurice_Guillaudot = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Maurice_Guillaudot_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Leopold Sedar Senghor
		MAL_AM_Leopold_Sedar_Senghor = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leopold_Sedar_Senghor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Administrative_Genius }
		}
	# Mamadou Dia
		MAL_AM_Mamadou_Dia = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mamadou_Dia_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Battle_Fleet_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Martin Cayla
		MAL_HoI_Martin_Cayla = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Martin_Cayla_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Dismal_Enigma }
		}
	# Moussa Traoré
		MAL_HoI_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Christian Laigret
		MAL_HoI_Christian_Laigret = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Christian_Laigret_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# René V.M. Barthes
		MAL_HoI_Rene_VM_Barthes = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rene_VM_Barthes_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Aoua Keita
		MAL_HoI_Aoua_Keita = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Aoua_Keita_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Leon Cayla
		MAL_CoStaff_Leon_Cayla = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Leon_Cayla_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Moussa Traoré
		MAL_CoStaff_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Fire_Support }
		}
	# Pierre Charles Cournarie
		MAL_CoStaff_Pierre_Charles_Cournarie = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Charles_Cournarie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Paul Moguez
		MAL_CoStaff_Paul_Moguez = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Moguez_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Abdallah Soumare
		MAL_CoStaff_Abdallah_Soumare = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Abdallah_Soumare_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Fire_Support }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Henri Verdier
		MAL_CoArmy_Henri_Verdier = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Verdier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Moussa Traoré
		MAL_CoArmy_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Guns_And_Butter_Doctrine }
		}
	# Jean-Louis Beyries
		MAL_CoArmy_JeanLouis_Beyries = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JeanLouis_Beyries_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Aimé Teisseire
		MAL_CoArmy_Aime_Teisseire = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aime_Teisseire_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Henri Rol-Tanguy
		MAL_CoArmy_Henri_RolTanguy = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_RolTanguy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Decisive_Battle_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Jean Desanti
		MAL_CoNavy_Jean_Desanti = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Desanti_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Indirect_Approach_Doctrine }
		}
	# Moussa Traoré
		MAL_CoNavy_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Open_Seas_Doctrine }
		}
	# Pierre Ponchardier
		MAL_CoNavy_Pierre_Ponchardier = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Ponchardier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# Ousmane Ibrahim Sall
		MAL_CoNavy_Ousmane_Ibrahim_Sall = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Ousmane_Ibrahim_Sall_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Decisive_Naval_Battle_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Auguste Calvel
		MAL_CoAir_Auguste_Calvel = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Auguste_Calvel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Naval_Aviation_Doctrine }
		}
	# Moussa Traoré
		MAL_CoAir_Moussa_Traore = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Moussa_Traore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Vertical_Envelopment_Doctrine }
		}
	# Henri Tourtet
		MAL_CoAir_Henri_Tourtet = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Tourtet_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# Yves Mourier
		MAL_CoAir_Yves_Mourier = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Yves_Mourier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Oumar Ousseynou
		MAL_CoAir_Oumar_Ousseynou = {
			picture = Generic_Portrait
			allowed = { tag = MAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Oumar_Ousseynou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Air_Superiority_Doctrine }
		}
	}
}
