ideas = {
	
	country = {
		SPR_revolutionary_strikes = {
			picture = GENERIC_Demonstrations
			modifier = {
				industrial_capacity_factory = -0.2
				local_resources_factor = -0.30
			}
		}
		SPR_upstart_army = {
			picture = chi_incompetent_officers
			modifier = {
				industrial_capacity_factory = -0.1
				stability_factor = -0.1
				experience_gain_army_factor = -1.0
			}
		}
		SPR_collapsing_society = {
			picture = prc_low_popular_support3
			modifier = {
				political_power_gain = -0.5
				stability_weekly = -0.001
				consumer_goods_factor = 0.1
				democratic_drift = -0.001
			}
		}
		SPR_economic_compromise = {
			picture = GENERIC_Economic_Compromise
			modifier = {
				production_speed_buildings_factor = -0.1
				production_factory_max_efficiency_factor = -0.1
				stability_weekly = 0.001
			}
		}
		SPR_catalonia_turmoil_1 = {
			picture = SPR_Catalonia_Explodes
			modifier = {
				production_speed_buildings_factor = -0.1
				stability_factor = -0.05
			}
		}
		SPR_catalonia_turmoil_2 = {
			picture = SPR_Catalonia_Explodes
			modifier = {
				production_speed_buildings_factor = -0.1
				stability_factor = -0.1
				war_support_factor = 0.05
			}
		}
		SPR_catalonia_turmoil_3 = {
			picture = SPR_Catalonia_Explodes
			modifier = {
				production_speed_buildings_factor = -0.1
				stability_factor = -0.15
				war_support_factor = 0.05
			}
		}
		SPR_catalonia_turmoil_4 = {
			picture = SPR_Catalonia_Explodes
			modifier = {
				production_speed_buildings_factor = -0.1
				stability_factor = -0.20
				war_support_factor = 0.05
				industrial_capacity_factory = 0.05
			}
		}
		SPR_1936_popular_front = {
			picture = SPR_Popular_Front
			modifier = {
				stability_factor = -0.05
				war_support_factor = 0.10
			}
		}
		SPR_may_1st_demonstrations = {
			picture = GENERIC_Demonstrations_2
			modifier = {
				industrial_capacity_factory = -0.2
				political_power_gain = -1.0
			}
		}
	}
}