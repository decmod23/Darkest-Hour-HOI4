ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Phetsarath Mahaouphatat
		LAO_HoG_Phetsarath_Mahaouphatat = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Phetsarath_Mahaouphatat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_General }
		}
	# H.R.H. Kindavong
		LAO_HoG_HRH_Kindavong = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Kindavong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Corporate_Suit }
		}
	# Phoui Sananikone
		LAO_HoG_Phoui_Sananikone = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1950.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Phoui_Sananikone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_General }
		}
	# Souvanna Phouma
		LAO_HoG_Souvanna_Phouma = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1952.1.1
				date < 1989.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Souvanna_Phouma_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Corporate_Suit }
		}
	# Katay Don Sasorith
		LAO_HoG_Katay_Don_Sasorith = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Katay_Don_Sasorith_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# H.R.M. Sauryavong Savang
		LAO_HoG_HRM_Sauryavong_Savang = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = HRM_Sauryavong_Savang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Silent_Workhorse }
		}
	# Eugene H.R. Eutrope
		LAO_HoG_Eugene_HR_Eutrope = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Eugene_HR_Eutrope_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Adrien A.M. Roques
		LAO_HoG_Adrien_AM_Roques = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Adrien_AM_Roques_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Louis A.M. Brasey
		LAO_HoG_Louis_AM_Brasey = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Louis_AM_Brasey_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# H.R.H. Phaya Khammao
		LAO_HoG_HRH_Phaya_Khammao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Phaya_Khammao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# H.R.H. Boun Oum
		LAO_HoG_HRH_Boun_Oum = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Boun_Oum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# H.R.H. Souvanna Rath
		LAO_HoG_HRH_Souvanna_Rath = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souvanna_Rath_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Kou Abhay
		LAO_HoG_Kou_Abhay = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1960.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Kou_Abhay_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Flamboyant_Tough_Guy }
		}
	# Kaysone Phomvihane
		LAO_HoG_Kaysone_Phomvihane = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1975.1.1
				date < 1993.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Kaysone_Phomvihane_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Vang Pao
		LAO_HoG_Vang_Pao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vang_Pao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Khamtai Siphandon
		LAO_HoG_Khamtai_Siphandon = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1991.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khamtai_Siphandon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Khambulatom Oomau
		LAO_HoG_Khambulatom_Oomau = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khambulatom_Oomau_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Happy_Amateur }
		}
	# Phomvihanh Kaysone
		LAO_HoG_Phomvihanh_Kaysone = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Phomvihanh_Kaysone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Backroom_Backstabber }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# H.R.H. Boun Oum
		LAO_FM_HRH_Boun_Oum = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Boun_Oum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_General_Staffer }
		}
	# Vang Pozeb
		LAO_FM_Vang_Pozeb = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2005.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Vang_Pozeb_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Ideological_Crusader }
		}
	# H.R.H. Phaya Khammao
		LAO_FM_HRH_Phaya_Khammao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Phaya_Khammao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Sasorith Kataydon
		LAO_FM_Sasorith_Kataydon = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sasorith_Kataydon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
	# Pierre Brossolette
		LAO_FM_Pierre_Brossolette = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Brossolette_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Iron_Fisted_Brute }
		}
	# H.R.H. Souphanouvong
		LAO_FM_HRH_Souphanouvong = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1996.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souphanouvong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Biased_Intellectual }
		}
	# Vang Pao
		LAO_FM_Vang_Pao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vang_Pao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_General_Staffer }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Phetsarath Mahaouphatat
		LAO_MoS_Phetsarath_Mahaouphatat = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Phetsarath_Mahaouphatat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Silent_Lawyer }
		}
	# Kong Le
		LAO_MoS_Kong_Le = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2014.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kong_Le_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Jacques Renouvin
		LAO_MoS_Jacques_Renouvin = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacques_Renouvin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Jacques Lecompte-Boinet
		LAO_MoS_Jacques_LecompteBoinet = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacques_LecompteBoinet_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Khamtai Siphandon
		LAO_MoS_Khamtai_Siphandon = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khamtai_Siphandon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Prince_Of_Terror }
		}
	# Khamphoui Sisavatdy
		LAO_MoS_Khamphoui_Sisavatdy = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khamphoui_Sisavatdy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Man_Of_The_People }
		}
	# Phomvihanh Kaysone
		LAO_MoS_Phomvihanh_Kaysone = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Phomvihanh_Kaysone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Efficient_Sociopath }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# H.R.H. Souvanna Rath
		LAO_AM_HRH_Souvanna_Rath = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souvanna_Rath_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Laissez-faire_Capitalist }
		}
	# Vang Pao
		LAO_AM_Vang_Pao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2011.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Vang_Pao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Administrative_Genius }
		}
	# Jacques Mouchel-Blaisot
		LAO_AM_Jacques_MouchelBlaisot = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacques_MouchelBlaisot_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Leon Martinaud-Déplat
		LAO_AM_Leon_MartinaudDeplat = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leon_MartinaudDeplat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Nouhak Phoumsavanh
		LAO_AM_Nouhak_Phoumsavanh = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nouhak_Phoumsavanh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Resource_Industrialist }
		}
	# Kong Le
		LAO_AM_Kong_Le = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Kong_Le_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Military_Entrepreneur }
		}
	# Jules Muracciole
		LAO_AM_Jules_Muracciole = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Jules_Muracciole_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Infantry_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Phousannani Kone
		LAO_HoI_Phousannani_Kone = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Phousannani_Kone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Kong Le
		LAO_HoI_Kong_Le_2 = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2014.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kong_Le_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Dismal_Enigma }
		}
	# Louis A.M. Brasey
		LAO_HoI_Louis_AM_Brasey = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Louis_AM_Brasey_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Louis Mairet
		LAO_HoI_Louis_Mairet = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Louis_Mairet_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Nouhak Phoumsavanh
		LAO_HoI_Nouhak_Phoumsavanh = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nouhak_Phoumsavanh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Industrial_Specialist }
		}
	# Kong Le
		LAO_HoI_Kong_Le = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Kong_Le_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Logistics_Specialist }
		}
	# Khambulatom Oomau
		LAO_HoI_Khambulatom_Oomau = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khambulatom_Oomau_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Technical_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Aphay Khou
		LAO_CoStaff_Aphay_Khou = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Aphay_Khou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Manoeuvre }
		}
	# Hang Sao
		LAO_CoStaff_Hang_Sao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2004.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hang_Sao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Adrien A.M. Roques
		LAO_CoStaff_Adrien_AM_Roques = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Adrien_AM_Roques_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Roger Sinaud
		LAO_CoStaff_Roger_Sinaud = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Roger_Sinaud_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Khamtai Siphandon
		LAO_CoStaff_Khamtai_Siphandon = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khamtai_Siphandon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Mass_Combat }
		}
	# Bounleuth Saycocie
		LAO_CoStaff_Bounleuth_Saycocie = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Bounleuth_Saycocie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Psychology }
		}
	# Phichith Douangchai
		LAO_CoStaff_Phichith_Douangchai = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Phichith_Douangchai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Psychology }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Savang Vong
		LAO_CoArmy_Savang_Vong = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Savang_Vong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Hang Sao
		LAO_CoArmy_Hang_Sao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2004.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hang_Sao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Phetsarath Mahaouphatat
		LAO_CoArmy_Phetsarath_Mahaouphatat = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Phetsarath_Mahaouphatat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# Henri Adeline
		LAO_CoArmy_Henri_Adeline = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Adeline_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Khamtai Siphandon
		LAO_CoArmy_Khamtai_Siphandon = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khamtai_Siphandon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Guns_And_Butter_Doctrine }
		}
	# Bounleuth Saycocie
		LAO_CoArmy_Bounleuth_Saycocie = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Bounleuth_Saycocie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Guns_And_Butter_Doctrine }
		}
	# Vang Pao
		LAO_CoArmy_Vang_Pao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vang_Pao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Aphay Khou
		LAO_CoNavy_Aphay_Khou = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Aphay_Khou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Power_Projection_Doctrine }
		}
	# H.R.H. Souvanna Rath
		LAO_CoNavy_HRH_Souvanna_Rath = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souvanna_Rath_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Maurice Bayrou
		LAO_CoNavy_Maurice_Bayrou = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Maurice_Bayrou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Khamtai Siphandon
		LAO_CoNavy_Khamtai_Siphandon = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khamtai_Siphandon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Indirect_Approach_Doctrine }
		}
	# Bounleuth Saycocie
		LAO_CoNavy_Bounleuth_Saycocie = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Bounleuth_Saycocie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Indirect_Approach_Doctrine }
		}
	# H.R.H. Souphanou Vong
		LAO_CoNavy_HRH_Souphanou_Vong = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souphanou_Vong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Open_Seas_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Sourith Sasorith
		LAO_CoAir_Sourith_Sasorith = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sourith_Sasorith_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Air_Superiority_Doctrine }
		}
	# Hang Sao
		LAO_CoAir_Hang_Sao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2004.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hang_Sao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# H.R.H. Boun Oum
		LAO_CoAir_HRH_Boun_Oum = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Boun_Oum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Pierre de Maismont
		LAO_CoAir_Pierre_de_Maismont = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_de_Maismont_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# Khamtai Siphandon
		LAO_CoAir_Khamtai_Siphandon = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khamtai_Siphandon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Army_Aviation_Doctrine }
		}
	# Bounleuth Saycocie
		LAO_CoAir_Bounleuth_Saycocie = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1985.1.1
				date < 1992.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Bounleuth_Saycocie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Army_Aviation_Doctrine }
		}
	# Vang Pao
		LAO_CoAir_Vang_Pao = {
			picture = Generic_Portrait
			allowed = { tag = LAO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vang_Pao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
