ideas = {
	high_command = {

		MZB_ar = {
			
			picture = generic_army_african_1
			
			allowed = {
				original_tag =	MZB
			}
			
			traits = { army_regrouping_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		MZB_ai2 = {

			picture = generic_army_african_3
			
			allowed = {
				original_tag =	MZB
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		MZB_aa2 = {

			picture = generic_army_african_2
			
			allowed = {
				original_tag =	MZB
			}
			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		MZB_acas = {

			picture = generic_air_african_2
			
			allowed = {
				original_tag =	MZB
			}
			
			traits = { air_close_air_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		MZB_ncs = {

			picture = generic_navy_african_2
			
			allowed = {
				original_tag =	MZB
			}
			
			traits = { navy_capital_ship_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}	

	theorist = {
		MZB_mt = {
			
			picture = generic_army_african_2

			allowed = {
				original_tag =	MZB
			}
			
			research_bonus = {
				land_doctrine = 0.07
			}
			
			traits = { military_theorist }
		}
		
		MZB_nt = {

			picture = generic_navy_african_3
			
			allowed = {
				original_tag =	MZB
			}
			
			research_bonus = {
				naval_doctrine = 0.07
			}
			
			traits = { naval_theorist }
		}	

		MZB_awt = {

			picture = generic_air_african_2
			
			allowed = {
				original_tag =	MZB
			}
			
			research_bonus = {
				air_doctrine = 0.07
			}
			
			traits = { air_warfare_theorist }
		}

	}
}