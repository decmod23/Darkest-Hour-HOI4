ideas = {
	#########################################################################
	#  Foreign Policy
	#########################################################################
	social_policies = {
		law = yes
		full_civil_liberties = {
			cost = 150
			removal_cost = -1
			level = 5
			available = {
				has_government = democratic
			}
			modifier = {
				political_power_gain = -0.10
				war_support_factor = -0.05
				research_speed_factor = 0.05
				drift_defence_factor = -0.20
				trade_opinion_factor = 0.20
			}
			cancel_if_invalid = no
		}
		limited_civil_liberties = {
			cost = 150
			removal_cost = -1
			level = 4
			available = {
				AND = {
					NOT = { has_government = communist }
					NOT = { has_government = socialist }
					NOT = { has_government = fascist }
				}
			}
			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.02
				research_speed_factor = 0.02
				drift_defence_factor = -0.10
			}
			cancel_if_invalid = no
			default = yes
		}
		police_state = {
			cost = 150
			removal_cost = -1
			level = 3
			available = {}
			modifier = {
				stability_factor = -0.05
				partisan_effect = -0.15
				drift_defence_factor = 0.10
			}
			cancel_if_invalid = no
		}
		martial_law = {
			cost = 150
			removal_cost = -1
			level = 2
			available = {}
			modifier = {
				stability_factor = 0.10
				war_support_factor = -0.10
				partisan_effect = -0.2
				conscription_factor = 0.05
			}
			cancel_if_invalid = no
		}
		totalitarian_system = {
			cost = 150
			removal_cost = -1
			level = 1
			available = {
				NOT = { has_government = democratic }
			}
			modifier = {
				political_power_gain = 0.10
				stability_factor = -0.10
				war_support_factor = 0.10
				drift_defence_factor = 0.10
				conscription_factor = 0.10
			}
			cancel_if_invalid = no
		}
	}
}
