ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Antun Saadeh
		SYR_HoG_Antun_Saadeh = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Antun_Saadeh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Naive_Optimist }
		}
	# Henri Dentz
		SYR_HoG_Henri_Dentz = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Dentz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_General }
		}
	# Salah al-Din al-Bitar
		SYR_HoG_Salah_alDin_alBitar = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1963.1.1
				date < 1981.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Salah_alDin_alBitar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_General }
		}
	# Amin al-Hafiz
		SYR_HoG_Amin_alHafiz = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1964.1.1
				date < 2010.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Amin_alHafiz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Political_Protege }
		}
	# Yusuf Zu'ayyin
		SYR_HoG_Yusuf_Zuayyin = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1966.1.1
				date < 1980.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Yusuf_Zuayyin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Flamboyant_Tough_Guy }
		}
	# Nureddin al-Atassi
		SYR_HoG_Nureddin_alAtassi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1968.1.1
				date < 1993.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Nureddin_alAtassi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Corporate_Suit }
		}
	# Sabri al-Assali
		SYR_HoG_Sabri_alAssali = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sabri_alAssali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Said al-Ghazzi
		SYR_HoG_Said_alGhazzi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Said_alGhazzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Jamil Mardam Bay
		SYR_HoG_Jamil_Mardam_Bay = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jamil_Mardam_Bay_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Damien Comte de Martel
		SYR_HoG_Damien_Comte_de_Martel = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Damien_Comte_de_Martel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Gabriel Puaux
		SYR_HoG_Gabriel_Puaux = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gabriel_Puaux_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Georges Catroux
		SYR_HoG_Georges_Catroux = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Georges_Catroux_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Khalid al Azm
		SYR_HoG_Khalid_al_Azm = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Khalid_al_Azm_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Hassan al Hakim
		SYR_HoG_Hassan_al_Hakim = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hassan_al_Hakim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Yves Chataigneau
		SYR_HoG_Yves_Chataigneau = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Yves_Chataigneau_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Shukri al-Kuwaitli
		SYR_HoG_Shukri_alKuwaitli = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shukri_alKuwaitli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Hussein al-Zaim
		SYR_HoG_Hussein_alZaim = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hussein_alZaim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Muhsin al Barazi
		SYR_HoG_Muhsin_al_Barazi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Muhsin_al_Barazi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Hassim al-Atassi
		SYR_HoG_Hassim_alAtassi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hassim_alAtassi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Nazim al Kudsi
		SYR_HoG_Nazim_al_Kudsi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1950.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nazim_al_Kudsi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Fawzi as Silu
		SYR_HoG_Fawzi_as_Silu = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_as_Silu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Adib al-Shishakli
		SYR_HoG_Adib_alShishakli = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Adib_alShishakli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Ata al-Ayyubi
		SYR_HoG_Ata_alAyyubi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1936.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ata_alAyyubi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Bahij ad din al-Khatib
		SYR_HoG_Bahij_ad_din_alKhatib = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bahij_ad_din_alKhatib_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Khalid al-Azm
		SYR_HoG_Khalid_alAzm = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Khalid_alAzm_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Husni al-Barazi
		SYR_HoG_Husni_alBarazi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Husni_alBarazi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Saadallah al-Jabiri
		SYR_HoG_Saadallah_alJabiri = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Saadallah_alJabiri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Faris al-Khouri
		SYR_HoG_Faris_alKhouri = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Faris_alKhouri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Maarouf al-Dawalibi
		SYR_HoG_Maarouf_alDawalibi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1961.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Maarouf_alDawalibi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Bashir al-Azma
		SYR_HoG_Bashir_alAzma = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1962.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bashir_alAzma_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Admiral }
		}
	# Hafez al-Assad
		SYR_HoG_Hafez_alAssad = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1970.1.1
				date < 2001.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Hafez_alAssad_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Ambitious_Union_Boss }
		}
	# Abdul Rahman Khleifawi
		SYR_HoG_Abdul_Rahman_Khleifawi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1971.1.1
				date < 2010.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Abdul_Rahman_Khleifawi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Flamboyant_Tough_Guy }
		}
	# Mahmoud al-Ayyubi
		SYR_HoG_Mahmoud_alAyyubi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1972.1.1
				date < 2020.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mahmoud_alAyyubi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Happy_Amateur }
		}
	# Muhammad Ali al-Halabi
		SYR_HoG_Muhammad_Ali_alHalabi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1978.1.1
				date < 2020.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Muhammad_Ali_alHalabi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Flamboyant_Tough_Guy }
		}
	# Abdul Rauf al-Kasm
		SYR_HoG_Abdul_Rauf_alKasm = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1980.1.1
				date < 2020.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Abdul_Rauf_alKasm_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Silent_Workhorse }
		}
	# Mahmoud Zuabi
		SYR_HoG_Mahmoud_Zuabi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1987.1.1
				date < 2001.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mahmoud_Zuabi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Flamboyant_Tough_Guy }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Antun Saadeh
		SYR_FM_Antun_Saadeh = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Antun_Saadeh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_Biased_Intellectual }
		}
	# Faris al-Khouri
		SYR_FM_Faris_alKhouri = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Faris_alKhouri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_The_Cloak_N_Dagger_Schemer }
		}
	# Shaykh Mustafa al-Maraghi
		SYR_FM_Shaykh_Mustafa_alMaraghi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Mustafa_alMaraghi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Jamil Mardam Bay
		SYR_FM_Jamil_Mardam_Bay = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jamil_Mardam_Bay_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Muhsin al Barazi
		SYR_FM_Muhsin_al_Barazi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Muhsin_al_Barazi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Sultan al-Atrash
		SYR_FM_Sultan_alAtrash = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sultan_alAtrash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Shaykh Muhammed al-Gawzi
		SYR_FM_Shaykh_Muhammed_alGawzi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Muhammed_alGawzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_General_Staffer }
		}
	# Lufti al-Haffar
		SYR_FM_Lufti_alHaffar = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lufti_alHaffar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# Saadallah al-Jabiri
		SYR_FM_Saadallah_alJabiri = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Saadallah_alJabiri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
	# Sultan al-Atrash
		SYR_FM_Sultan_alAtrash_2 = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sultan_alAtrash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Salah al-Din al-Bitar
		SYR_FM_Salah_alDin_alBitar = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salah_alDin_alBitar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Farouk al-Sharaa
		SYR_FM_Farouk_alSharaa = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Farouk_alSharaa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Great_Compromiser }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Shaykh Mustafa al-Maraghi
		SYR_MoS_Shaykh_Mustafa_alMaraghi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Mustafa_alMaraghi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Back_Stabber }
		}
	# Faris al-Khouri
		SYR_MoS_Faris_alKhouri = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Faris_alKhouri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Yves Morandat
		SYR_MoS_Yves_Morandat = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Yves_Morandat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crooked_Kleptocrat }
		}
	# Mustafa al-Atassi
		SYR_MoS_Mustafa_alAtassi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mustafa_alAtassi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# André Salvat
		SYR_MoS_Andre_Salvat = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Andre_Salvat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Jamil Chakrura
		SYR_MoS_Jamil_Chakrura = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jamil_Chakrura_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Nazim al Kudsi
		SYR_MoS_Nazim_al_Kudsi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nazim_al_Kudsi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Jean Abdel Foukha
		SYR_MoS_Jean_Abdel_Foukha = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Abdel_Foukha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Fu'ad Absi
		SYR_MoS_Fuad_Absi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Fuad_Absi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Yves Morandat
		SYR_AM_Yves_Morandat = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Yves_Morandat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Air_Superiority_Proponent }
		}
	# Amir Mustafa al-Shihabi
		SYR_AM_Amir_Mustafa_alShihabi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Amir_Mustafa_alShihabi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Laissez-faire_Capitalist }
		}
	# Shaykh Mustafa al-Maraghi
		SYR_AM_Shaykh_Mustafa_alMaraghi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Mustafa_alMaraghi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_To_Ground_Proponent }
		}
	# Hafez al-Assad
		SYR_AM_Hafez_alAssad = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1958.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hafez_alAssad_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_Superiority_Proponent }
		}
	# Faris al-Khouri
		SYR_AM_Faris_alKhouri = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Faris_alKhouri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_To_Ground_Proponent }
		}
	# Shaykh Muhammed al-Shinnawi
		SYR_AM_Shaykh_Muhammed_alShinnawi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Muhammed_alShinnawi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Infantry_Proponent }
		}
	# Shaykh Abd al-Alim Selim
		SYR_AM_Shaykh_Abd_alAlim_Selim = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Abd_alAlim_Selim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Michel Maurice-Bokanowski
		SYR_AM_Michel_MauriceBokanowski = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michel_MauriceBokanowski_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Paul Leistenschneider
		SYR_AM_Paul_Leistenschneider = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Leistenschneider_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Khalid al Azm
		SYR_AM_Khalid_al_Azm = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Khalid_al_Azm_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Hassan al Hakim
		SYR_AM_Hassan_al_Hakim = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hassan_al_Hakim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_To_Ground_Proponent }
		}
	# Michel Aflaq
		SYR_AM_Michel_Aflaq = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michel_Aflaq_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Mustafa Tlass
		SYR_AM_Mustafa_Tlass = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Mustafa_Tlass_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Military_Entrepreneur }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Henri Dentz
		SYR_HoI_Henri_Dentz = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Dentz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Shaykh Muhammed al-Gawzi
		SYR_HoI_Shaykh_Muhammed_alGawzi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Muhammed_alGawzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Damien Comte de Martel
		SYR_HoI_Damien_Comte_de_Martel = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Damien_Comte_de_Martel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Gabriel Puaux
		SYR_HoI_Gabriel_Puaux = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gabriel_Puaux_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Georges Catroux
		SYR_HoI_Georges_Catroux = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Georges_Catroux_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Yves Chataigneau
		SYR_HoI_Yves_Chataigneau = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Yves_Chataigneau_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Sami Hinnawi
		SYR_HoI_Sami_Hinnawi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sami_Hinnawi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Muhammad al-Khuli
		SYR_HoI_Muhammad_alKhuli = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Muhammad_alKhuli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Logistics_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Henri Dentz
		SYR_CoStaff_Henri_Dentz = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Dentz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Fawzi al-Qawuqji
		SYR_CoStaff_Fawzi_alQawuqji = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_alQawuqji_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Shaykh Muhammed al-Gawzi
		SYR_CoStaff_Shaykh_Muhammed_alGawzi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Muhammed_alGawzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Mustafa Tlass
		SYR_CoStaff_Mustafa_Tlass = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1961.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mustafa_Tlass_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Henri Amiel
		SYR_CoStaff_Henri_Amiel = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Amiel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Sultan al-Atrash
		SYR_CoStaff_Sultan_alAtrash_2 = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sultan_alAtrash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Lucien Cambas
		SYR_CoStaff_Lucien_Cambas = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lucien_Cambas_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Hussein al-Zaim
		SYR_CoStaff_Hussein_alZaim = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hussein_alZaim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Fawzi as Silu
		SYR_CoStaff_Fawzi_as_Silu = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_as_Silu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Sultan al-Atrash
		SYR_CoStaff_Sultan_alAtrash = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sultan_alAtrash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Hikmat al-Shihabi
		SYR_CoStaff_Hikmat_alShihabi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Hikmat_alShihabi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Fire_Support }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Fawzi al-Quawugji
		SYR_CoArmy_Fawzi_alQuawugji = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_alQuawugji_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Bernard Duperier
		SYR_CoArmy_Bernard_Duperier = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bernard_Duperier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Gabriel Puaux
		SYR_CoArmy_Gabriel_Puaux = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gabriel_Puaux_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Georges Catroux
		SYR_CoArmy_Georges_Catroux = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Georges_Catroux_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Adib al-Shishakli
		SYR_CoArmy_Adib_alShishakli = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Adib_alShishakli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Hikmat al-Shihabi
		SYR_CoArmy_Hikmat_alShihabi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Hikmat_alShihabi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Armoured_Spearhead_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Shaykh Muhammed al-Gawzi
		SYR_CoNavy_Shaykh_Muhammed_alGawzi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Muhammed_alGawzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Base_Control_Doctrine }
		}
	# Yves Morandat
		SYR_CoNavy_Yves_Morandat = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Yves_Morandat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Yves Carré de Lusancay
		SYR_CoNavy_Yves_Carre_de_Lusancay = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Yves_Carre_de_Lusancay_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Omar Abrash
		SYR_CoNavy_Omar_Abrash = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Omar_Abrash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# Hikmat al-Shihabi
		SYR_CoNavy_Hikmat_alShihabi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Hikmat_alShihabi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Indirect_Approach_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Hussein al-Zaim
		SYR_CoAir_Hussein_alZaim = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hussein_alZaim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Air_Superiority_Doctrine }
		}
	# Henri Amiel
		SYR_CoAir_Henri_Amiel_2 = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Amiel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# Christian Coudray
		SYR_CoAir_Christian_Coudray = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Christian_Coudray_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Naval_Aviation_Doctrine }
		}
	# Henri Amiel
		SYR_CoAir_Henri_Amiel = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Amiel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Khaleed al Razi
		SYR_CoAir_Khaleed_al_Razi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Khaleed_al_Razi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# Hikmat al-Shihabi
		SYR_CoAir_Hikmat_alShihabi = {
			picture = Generic_Portrait
			allowed = { tag = SYR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Hikmat_alShihabi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Army_Aviation_Doctrine }
		}
	}
}
