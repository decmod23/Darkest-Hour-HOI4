ideas = {

	high_command = {

		BOL_tomas_monje_gutierrez = {
			
			picture = generic_army_south_america_2
			
			allowed = {
				original_tag = BOL
			}
			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		BOL_carlos_altamirano = {

			picture = generic_army_south_america_5
			
			allowed = {
				original_tag = BOL
			}
			
			traits = { army_artillery_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		BOL_fabian_vaca_chavez = {

			picture = generic_air_south_america_1
			
			allowed = {
				original_tag = BOL
			}
			
			traits = { air_naval_strike_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		BOL_julian_montellano_carrasco = {

			picture = generic_navy_south_america_1
			
			allowed = {
				original_tag = BOL
			}
			
			traits = { navy_naval_air_defense_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}	

	theorist = {
		BOL_enrique_baldivieso_aparicio = {
			
			picture = generic_army_south_america_1

			allowed = {
				original_tag = BOL
			}
			
			research_bonus = {
				land_doctrine = 0.07
			}
			
			traits = { military_theorist }
		}
		
		BOL_hector_bilbao_roja = {

			picture = generic_air_south_america_1
			
			allowed = {
				original_tag = BOL
			}
			
			research_bonus = {
				air_doctrine = 0.07
			}
			
			traits = { air_warfare_theorist }
		}	

		BOL_alberto_ostria_gutierrez = {

			picture = generic_navy_south_america_1
			
			allowed = {
				original_tag = BOL
			}
			
			research_bonus = {
				naval_doctrine = 0.07
			}
			
			traits = { naval_theorist }
		}

	}
}