ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Kayreddin al-Ahdab
		ARA_HoG_Kayreddin_alAhdab = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kayreddin_alAhdab_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Naive_Optimist }
		}
	# Mustafa an-Nahhas Pasha
		ARA_HoG_Mustafa_anNahhas_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mustafa_anNahhas_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Ismael Bey Gaspirali
		ARA_HoG_Ismael_Bey_Gaspirali = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ismael_Bey_Gaspirali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Ismael Bey Gaspirali
		ARA_HoG_Ismael_Bey_Gaspirali_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ismael_Bey_Gaspirali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Ata al-Ayyubi
		ARA_HoG_Ata_alAyyubi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1936.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ata_alAyyubi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Bahij ad din al-Khatib
		ARA_HoG_Bahij_ad_din_alKhatib = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bahij_ad_din_alKhatib_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Khalid al-Azm
		ARA_HoG_Khalid_alAzm = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Khalid_alAzm_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Husni al-Barazi
		ARA_HoG_Husni_alBarazi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Husni_alBarazi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Saadallah al-Jabiri
		ARA_HoG_Saadallah_alJabiri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Saadallah_alJabiri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Faris al-Khouri
		ARA_HoG_Faris_alKhouri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Faris_alKhouri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Mohammed Naguib
		ARA_HoG_Mohammed_Naguib = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_Naguib_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Gamal Abdel Nasser
		ARA_HoG_Gamal_Abdel_Nasser = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gamal_Abdel_Nasser_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Maarouf al-Dawalibi
		ARA_HoG_Maarouf_alDawalibi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1961.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Maarouf_alDawalibi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Ali Sabri
		ARA_HoG_Ali_Sabri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1962.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Sabri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Bashir al-Azma
		ARA_HoG_Bashir_alAzma = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1962.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bashir_alAzma_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Tawfiq al Suweidi
		ARA_HoG_Tawfiq_al_Suweidi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Tawfiq_al_Suweidi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Ambitious_Union_Boss }
		}
	# Arshad al Umari
		ARA_HoG_Arshad_al_Umari = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Arshad_al_Umari_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Happy_Amateur }
		}
	# Abdul Karim Qassim
		ARA_HoG_Abdul_Karim_Qassim = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1958.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Abdul_Karim_Qassim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Old_Airmarshal }
		}
	# Ahmed Hassan al-Bakr
		ARA_HoG_Ahmed_Hassan_alBakr = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1963.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmed_Hassan_alBakr_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Political_Protege }
		}
	# Tahir Yahya
		ARA_HoG_Tahir_Yahya = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1963.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Tahir_Yahya_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Flamboyant_Tough_Guy }
		}
	# Shukri al-Kuwaitli
		ARA_HoG_Shukri_alKuwaitli = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Shukri_alKuwaitli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Backroom_Backstabber }
		}
	# Abdel Hakim Amer
		ARA_HoG_Abdel_Hakim_Amer = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Abdel_Hakim_Amer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_General }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Aziz Ali al-Masri
		ARA_FM_Aziz_Ali_alMasri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1950.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Aziz_Ali_alMasri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_General_Staffer }
		}
	# Abdallah Aref al-Yafi
		ARA_FM_Abdallah_Aref_alYafi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Abdallah_Aref_alYafi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_General_Staffer }
		}
	# Ali Mahir Pasha
		ARA_FM_Ali_Mahir_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Mahir_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Samir al-Rifai
		ARA_FM_Samir_alRifai = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Samir_alRifai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Said al-Mufti
		ARA_FM_Said_alMufti = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Said_alMufti_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Sultan al-Atrash
		ARA_FM_Sultan_alAtrash = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sultan_alAtrash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Salah al-Din al-Bitar
		ARA_FM_Salah_alDin_alBitar = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salah_alDin_alBitar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Tawfiq al Suweidi
		ARA_FM_Tawfiq_al_Suweidi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Tawfiq_al_Suweidi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Apologetic_Clerk }
		}
	# Ali Mumtaz
		ARA_FM_Ali_Mumtaz = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Mumtaz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_General_Staffer }
		}
	# Muhammed Fadelfomali
		ARA_FM_Muhammed_Fadelfomali = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Muhammed_Fadelfomali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Biased_Intellectual }
		}
	# Yahya Hamudeh
		ARA_FM_Yahya_Hamudeh = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Yahya_Hamudeh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Iron_Fisted_Brute }
		}
	# Anwar El Sadat
		ARA_FM_Anwar_El_Sadat = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Anwar_El_Sadat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_General_Staffer }
		}
	# Salah Salem
		ARA_FM_Salah_Salem = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Salah_Salem_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_The_Cloak_N_Dagger_Schemer }
		}
	# Salah Salem
		ARA_FM_Salah_Salem_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Salah_Salem_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_The_Cloak_N_Dagger_Schemer }
		}
	# Faris al-Khouri
		ARA_FM_Faris_alKhouri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Faris_alKhouri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_The_Cloak_N_Dagger_Schemer }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Nassim Majdalany
		ARA_MoS_Nassim_Majdalany = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Nassim_Majdalany_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Compassionate_Gentleman }
		}
	# Sayyid Qutb
		ARA_MoS_Sayyid_Qutb = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sayyid_Qutb_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Silent_Lawyer }
		}
	# Romarcos Hanna Pasha
		ARA_MoS_Romarcos_Hanna_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Romarcos_Hanna_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Awni Abdul Hadi
		ARA_MoS_Awni_Abdul_Hadi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Awni_Abdul_Hadi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Jacob Dorval-Penney
		ARA_MoS_Jacob_DorvalPenney = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacob_DorvalPenney_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Mohammed al-Shureiki
		ARA_MoS_Mohammed_alShureiki = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_alShureiki_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Ali Hilali Pasha
		ARA_MoS_Ali_Hilali_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Hilali_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Zakaria Mohieddin
		ARA_MoS_Zakaria_Mohieddin = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Zakaria_Mohieddin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Kamaleddin Hussein
		ARA_MoS_Kamaleddin_Hussein = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Kamaleddin_Hussein_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Salah Nasr
		ARA_MoS_Salah_Nasr = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1957.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salah_Nasr_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Salah Nasr
		ARA_MoS_Salah_Nasr_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1957.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salah_Nasr_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Abdel Latif Boghdadi
		ARA_MoS_Abdel_Latif_Boghdadi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Abdel_Latif_Boghdadi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Man_Of_The_People }
		}
	# Gamal Salem
		ARA_MoS_Gamal_Salem = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Gamal_Salem_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Prince_Of_Terror }
		}
	# Faris al-Khouri
		ARA_MoS_Faris_alKhouri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Faris_alKhouri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Man_Of_The_People }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Kayreddin al-Ahdab
		ARA_AM_Kayreddin_alAhdab = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kayreddin_alAhdab_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Laissez-faire_Capitalist }
		}
	# Muhammad Mahmoud Pasha
		ARA_AM_Muhammad_Mahmoud_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Muhammad_Mahmoud_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Ahmad Qadri
		ARA_AM_Ahmad_Qadri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmad_Qadri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Ahmad Qadri
		ARA_AM_Ahmad_Qadri_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmad_Qadri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Michel Aflaq
		ARA_AM_Michel_Aflaq = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michel_Aflaq_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Noureddin Arraf
		ARA_AM_Noureddin_Arraf = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Noureddin_Arraf_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Theoretical_Scientist }
		}
	# Anwar Khatib
		ARA_AM_Anwar_Khatib = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Anwar_Khatib_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Strategic_Air_Proponent }
		}
	# Khaled Mohieddin
		ARA_AM_Khaled_Mohieddin_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Khaled_Mohieddin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Tank_Proponent }
		}
	# Shaykh Abd al-Alim Selim
		ARA_AM_Shaykh_Abd_alAlim_Selim = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Abd_alAlim_Selim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Administrative_Genius }
		}
	# Khaled Mohieddin
		ARA_AM_Khaled_Mohieddin = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khaled_Mohieddin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Tank_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Kayreddin al-Ahdab
		ARA_HoI_Kayreddin_alAhdab = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kayreddin_alAhdab_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Technical_Specialist }
		}
	# Mohammed Hilmi Issa
		ARA_HoI_Mohammed_Hilmi_Issa = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_Hilmi_Issa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Rustum Haidar
		ARA_HoI_Rustum_Haidar = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rustum_Haidar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Rustum Haidar
		ARA_HoI_Rustum_Haidar_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rustum_Haidar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Zakaria Mohieddin
		ARA_HoI_Zakaria_Mohieddin = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Zakaria_Mohieddin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Ali Sabri
		ARA_HoI_Ali_Sabri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Sabri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Rifai al Fadel
		ARA_HoI_Rifai_al_Fadel = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Rifai_al_Fadel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Dismal_Enigma }
		}
	# Ali Sabri
		ARA_HoI_Ali_Sabri_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Sabri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Dismal_Enigma }
		}
	# Shaykh Muhammed al-Gawzi
		ARA_HoI_Shaykh_Muhammed_alGawzi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Muhammed_alGawzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Aziz Ali al-Masri
		ARA_CoStaff_Aziz_Ali_alMasri = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Aziz_Ali_alMasri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Manoeuvre }
		}
	# Antoine Harb
		ARA_CoStaff_Antoine_Harb = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Antoine_Harb_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Mass_Combat }
		}
	# Fawzi al-Qawuqji
		ARA_CoStaff_Fawzi_alQawuqji = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_alQawuqji_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Fawzi al-Qawuqji
		ARA_CoStaff_Fawzi_alQawuqji_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_alQawuqji_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Oskar Munzel
		ARA_CoStaff_Oskar_Munzel = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Oskar_Munzel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Fire_Support }
		}
	# Hassan Sabry Pasha
		ARA_CoStaff_Hassan_Sabry_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hassan_Sabry_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Hasan Salama
		ARA_CoStaff_Hasan_Salama = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hasan_Salama_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Haddad al-Wali
		ARA_CoStaff_Haddad_alWali = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Haddad_alWali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Haddad al-Wali
		ARA_CoStaff_Haddad_alWali_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Haddad_alWali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Sultan al-Atrash
		ARA_CoStaff_Sultan_alAtrash = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sultan_alAtrash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Zakaria Mohieddin
		ARA_CoStaff_Zakaria_Mohieddin = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1948.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Zakaria_Mohieddin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Mohammed Naguib
		ARA_CoStaff_Mohammed_Naguib = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_Naguib_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Mohamed Hussein Tantawi
		ARA_CoStaff_Mohamed_Hussein_Tantawi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1957.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohamed_Hussein_Tantawi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Yasser Arafat
		ARA_CoStaff_Yasser_Arafat = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1961.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Yasser_Arafat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Mass_Combat }
		}
	# Shaykh Muhammed al-Gawzi
		ARA_CoStaff_Shaykh_Muhammed_alGawzi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Muhammed_alGawzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Fire_Support }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Jamil Lahoud
		ARA_CoArmy_Jamil_Lahoud = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jamil_Lahoud_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Mohammed Naguib
		ARA_CoArmy_Mohammed_Naguib = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_Naguib_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Nair al-Jafaar
		ARA_CoArmy_Nair_alJafaar = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nair_alJafaar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Nair al-Jafaar
		ARA_CoArmy_Nair_alJafaar_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nair_alJafaar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Gamal Abdel Nasser
		ARA_CoArmy_Gamal_Abdel_Nasser = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gamal_Abdel_Nasser_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Hussein El-Shafei
		ARA_CoArmy_Hussein_ElShafei = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hussein_ElShafei_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Mohamed Hussein Tantawi
		ARA_CoArmy_Mohamed_Hussein_Tantawi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1957.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohamed_Hussein_Tantawi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Mohamed Hussein Tantawi
		ARA_CoArmy_Mohamed_Hussein_Tantawi_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1957.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohamed_Hussein_Tantawi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Abdel Latif Boghdadi
		ARA_CoArmy_Abdel_Latif_Boghdadi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Abdel_Latif_Boghdadi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Static_Defence_Doctrine }
		}
	# Abdel Hakim Amer
		ARA_CoArmy_Abdel_Hakim_Amer_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Abdel_Hakim_Amer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Adib al-Shishakli
		ARA_CoArmy_Adib_alShishakli = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Adib_alShishakli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Elastic_Defence_Doctrine }
		}
	# Abdel Hakim Amer
		ARA_CoArmy_Abdel_Hakim_Amer = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1955.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Abdel_Hakim_Amer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Armoured_Spearhead_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Nabil Jad
		ARA_CoNavy_Nabil_Jad = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Nabil_Jad_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Open_Seas_Doctrine }
		}
	# Abdallah Ziwar Pasha
		ARA_CoNavy_Abdallah_Ziwar_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdallah_Ziwar_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Hibah abd-el Adir
		ARA_CoNavy_Hibah_abdel_Adir = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hibah_abdel_Adir_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Gadi abd-el Jabbar
		ARA_CoNavy_Gadi_abdel_Jabbar = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gadi_abdel_Jabbar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# Shaykh Muhammed al-Gawzi
		ARA_CoNavy_Shaykh_Muhammed_alGawzi = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Shaykh_Muhammed_alGawzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Base_Control_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Slimane Naoufai
		ARA_CoAir_Slimane_Naoufai = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Slimane_Naoufai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# Abdelhamid Soliman
		ARA_CoAir_Abdelhamid_Soliman = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdelhamid_Soliman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# Sami al-Afiz
		ARA_CoAir_Sami_alAfiz = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sami_alAfiz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# Sami al-Afiz
		ARA_CoAir_Sami_alAfiz_2 = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sami_alAfiz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# Gamal Abdel Nasser
		ARA_CoAir_Gamal_Abdel_Nasser = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gamal_Abdel_Nasser_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# Hussein al-Zaim
		ARA_CoAir_Hussein_alZaim = {
			picture = Generic_Portrait
			allowed = { tag = ARA }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Hussein_alZaim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Carpet_Bombing_Doctrine }
		}
	}
}
