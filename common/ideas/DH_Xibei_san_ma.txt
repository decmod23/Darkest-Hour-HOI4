ideas = {

	theorist = {
		XSM_keng_shoushan = {

			picture = generic_army_asia_4
			
			allowed = {
				original_tag = XSM
			}
			
			research_bonus = {
				land_doctrine = 0.07
			}
			
			traits = { military_theorist }
		}
		
		XSM_ma_hongkui = { 

			picture = generic_air_asia_2
			
			allowed = {
				original_tag = XSM
			}
			
			research_bonus = {
				air_doctrine = 0.07
			}
			
			traits = { air_warfare_theorist }
		}
	}

	high_command = {

		XSM_ma_donghai = {

			picture = generic_air_asia_2
			
			allowed = {
				original_tag = XSM
			}
			
			traits = { air_close_air_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		XSM_wu_qi = {

			picture = generic_army_asia_1
			
			allowed = {
				original_tag = XSM
			}
			
			traits = { army_cavalry_3 }
			
			ai_will_do = {
				factor = 1
			}
		}

		XSM_au_yongrui = {

			picture = generic_army_asia_2
			
			allowed = {
				original_tag = XSM
			}
			
			traits = { army_commando_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		XSM_ho_tengfei = {

			picture = generic_navy_asia_2
			
			allowed = {
				original_tag = XSM
			}
			
			traits = { navy_fleet_logistics_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}
}