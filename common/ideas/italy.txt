ideas = {
	country = {
		victor_emmanuel = {
			allowed = {
				original_tag = "ITA"
			}

			allowed_civil_war = {
				NOT = {
					has_government = communist
				}
				NOT = {
					any_other_country = {
						original_tag = ITA
						has_government = fascist
					}
				}
			}
			
			removal_cost = -1
			
			modifier = {
				stability_factor = 0.05				
			}
		}
		vittoria_mutilata = {			
			
			allowed = {
				original_tag = "ITA"
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = communist
						is_democratic = yes
					}
				}
			}
			
			removal_cost = -1
			
			modifier = {
				communist_acceptance = -10
				democratic_acceptance = -10
				fascist_acceptance = 10
				ai_focus_aggressive_factor = 0.5
				justify_war_goal_time = -0.05
			}
		}

		ITA_german_millitary_cooperation_focus = {

			picture = german_advisors

			allowed = {
				original_tag = ITA
			}

			allowed_civil_war = {
				OR = {
					AND = {
						has_government = fascist
						GER = {
							has_government = fascist
						}
					}
					AND = {
						has_government = communist
						GER = {
							has_government = communist
						}
					}
					AND = {
						is_democratic = yes
						GER = {
							is_democratic = yes
						}
					}
				}
			}

			removal_cost = -1

			research_bonus = {
				land_doctrine = 0.05
			}
			research_bonus = {
				air_doctrine = 0.05
			}				
		}
	}
		
	
	high_command = {
	
		mario_roatta = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { army_regrouping_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		vittorio_ambrosio = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { army_cavalry_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		alfredo_guzzoni = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { army_infantry_1 }
			
			ai_will_do = {
				factor = 2
			}
		}	
		
		ettore_muti = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { air_tactical_bombing_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		marziale_cerutti = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { air_air_combat_training_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		silvio_scaroni = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { air_bomber_interception_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		alberto_da_zara = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { navy_anti_submarine_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		carlo_bergamini = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { navy_capital_ship_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		luigi_mascherpa = {
			
			
			allowed = {
				original_tag = ITA
			}
			

			
			traits = { navy_naval_air_defense_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

}
