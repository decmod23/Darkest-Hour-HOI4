ideas = {
	#########################################################################
	#  National Spirits
	#########################################################################
	country = {
		#### Marginalized Muslim Community
		RAJ_Marginalized_Muslim_Community = {
			name = RAJ_marginalized_muslim_community
			picture = raj_marginalized_muslim_community

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.1
				conscription_factor = -0.2
			}
		}
		
		#### Princely States
		RAJ_Princely_States = {

			name = RAJ_princely_states
			picture = generic_morale_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = fascist
						has_government = communist
					}
				}
			}

			removal_cost = -1

			modifier = {
				autonomy_gain = -0.1
			}
		}
		#### Risk of Famine
		RAJ_Risk_of_Famine = {

			name = RAJ_risk_of_famine
			picture = raj_risk_of_famine

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = fascist
						has_government = communist
					}
				}
			}

			removal_cost = -1

			modifier = {

			}
		}
		RAJ_azad_hind = {
			picture = generic_morale_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				fascist_drift = 0.05
			}
		}



		RAJ_marginalized_muslim_community_angry = {
		
			picture = raj_marginalized_muslim_community_angry

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.1
				conscription_factor = -0.2
				autonomy_gain = -0.4
			}
		}

		RAJ_marginalized_muslim_community_happy = {
		
			picture = raj_marginalized_muslim_community_happy

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.02
				conscription_factor = -0.1
				autonomy_gain = 0.6
			}
		}


		RAJ_famine = {

			picture = ENG_the_war_to_end_all_wars

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				stability_factor = -0.15
				industrial_capacity_factory = -0.25
				industrial_capacity_dockyard = -0.25
				conscription_factor = -0.1
				consumer_goods_factor = 0.05
			}
		}


		RAJ_princely_states_election = {

			picture = generic_morale_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = fascist
						has_government = communist
					}
				}
			}

			removal_cost = -1

			modifier = {
				autonomy_gain = -0.4
			}
		}

		RAJ_princely_states_donations = {

			picture = generic_production_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = fascist
						has_government = communist
					}
				}
			}

			removal_cost = -1

			modifier = {
				autonomy_gain = -0.4
				consumer_goods_factor = -0.03
			}
		}

		RAJ_princely_states_donations_troops = {

			picture = generic_manpower_bonus

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = fascist
						has_government = communist
					}
				}
			}

			removal_cost = -1

			modifier = {
				autonomy_gain = -0.4
				consumer_goods_factor = -0.03
				conscription_factor = 0.05
			}
		}

		RAJ_indian_gentlemen_officers = {

			picture = raj_indian_gentlemen_officers

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = fascist
						has_government = communist
					}
				}
			}

			removal_cost = -1

			modifier = {
				military_leader_cost_factor = -0.50
				army_leader_start_level = 1
			}
		}

		RAJ_all_india_forward_bloc = {

			picture = raj_all_india_forward_bloc

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				OR = {
					has_government = fascist
					has_government = communist
				}
			}

			removal_cost = -1

			modifier = {
				democratic_drift = -0.02
				democratic_drift = -0.02
				democratic_drift = -0.02
				authoritarian_drift = -0.02
			}
		}

		RAJ_all_india_forward_bloc_fascism = {

			picture = raj_all_india_forward_bloc_fascism

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				has_government = fascist
			}

			removal_cost = -1

			modifier = {
				democratic_drift = -0.02
				authoritarian_drift = -0.02
				fascist_drift = 0.03
				autonomy_gain = 0.5
			}
		}

		RAJ_all_india_forward_bloc_communism = {

			picture = raj_all_india_forward_bloc_communism

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				has_government = communist
			}

			removal_cost = -1

			modifier = {
				democratic_drift = -0.02
				authoritarian_drift = -0.02
				communist_drift = 0.03
				autonomy_gain = 0.5
			}
		}

		RAJ_agrarian_society = {

			picture = generic_agrarian_society

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				conscription_factor = -0.69
			}
		}

		RAJ_quit_india_movement = {
		
			picture = raj_quit_india_movement
			
			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				NOT = {
					OR = {
						has_government = fascist
						has_government = communist
					}
				}
			}
			
			removal_cost = -1
			
			modifier = {
				autonomy_gain = 0.4
			}
		}
	}
	hidden_ideas = {
		RAJ_lions_of_the_great_war = {

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				conscription_factor = 0.02
			}
		}

		RAJ_indian_gurkhas = {

			allowed = {
				original_tag = RAJ
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				conscription_factor = 0.02
			}
		}
	}



	theorist = {

		RAJ_rajendra_prasad = {
					
			picture = generic_army_arab_1

			allowed = {
				original_tag = RAJ
			}
			
			research_bonus = {
				land_doctrine = 0.07
			}
			
			traits = { military_theorist }
		}

		RAJ_anyang_bhula = {
					
			picture = generic_air_arab_1
				
			allowed = {
				original_tag = RAJ
			}
			
			research_bonus = {
				air_doctrine = 0.07
			}
			
			traits = { air_warfare_theorist }
		}

		RAJ_bahadur_jayla_of_khoch_bahur = {
					
					
			allowed = {
				original_tag = RAJ
			}
			
			picture = generic_political_advisor_arab_1
			
			research_bonus = {
				naval_doctrine = 0.07
			}
			
			traits = { naval_theorist }
		}

		RAJ_homi_j_bhabha = {

			allowed = {
				original_tag = RAJ
			}
			
			picture = generic_political_advisor_arab_2
			
			research_bonus = {
				nuclear = 0.07
			}
			
			traits = { nuclear_scientist }
		}
	}

	high_command = {

		RAJ_ram_singh_thakur = {

			picture = generic_army_arab_1
			
			allowed = {
				original_tag = RAJ
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_arjan_singh_vibhusan = {

			picture = generic_air_arab_3
			
			allowed = {
				original_tag = RAJ
			}
			
			traits = { air_air_combat_training_3 }
			
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_aspy_merwan_engineer = {

			picture = generic_air_arab_2
			
			allowed = {
				original_tag = RAJ
			}
			
			traits = { air_naval_strike_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_mehar_singh = {

			picture = generic_air_arab_1
			
			allowed = {
				original_tag = RAJ
			}
			
			traits = { air_close_air_support_3 }
			
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_w_h_gould_bradford = {

			picture = generic_navy_europe_1
			
			allowed = {
				original_tag = RAJ
			}
			
			traits = { navy_anti_submarine_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}
}
