ideas = {
	#################################################
	### Head of Government
	#################################################
	Head_of_Government = {
		# Damian Kratzenberg
		LUX_HoG_Damian_Kratzenberg = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Damian_Kratzenberg_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Corporate_Suit }
		}
		# Guillaume Soisson
		LUX_HoG_Guillaume_Soisson = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Guillaume_Soisson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Happy_Amateur }
		}
		# Joseph Bech
		LUX_HoG_Joseph_Bech = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1926.1.1
				date < 1975.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Bech_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
		# Pierre Dupong
		LUX_HoG_Pierre_Dupong = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Dupong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
		# Paul Eyschen
		LUX_HoG_Paul_Eyschen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Eyschen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
		# Mathias Mongenast
		LUX_HoG_Mathias_Mongenast = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mathias_Mongenast_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
		# Hubert Loutsch
		LUX_HoG_Hubert_Loutsch = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hubert_Loutsch_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
		# Victor Thorn
		LUX_HoG_Victor_Thorn = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Thorn_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
		# Léon Kauffman
		LUX_HoG_Leon_Kauffman = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1917.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leon_Kauffman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
		# Émile Reuter
		LUX_HoG_Emile_Reuter = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Emile_Reuter_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
		# Henry Cravatte
		LUX_HoG_Henry_Cravatte = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1958.1.1
				date < 1991.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_Cravatte_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
		# Gaston Thorn
		LUX_HoG_Gaston_Thorn = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1964.1.1
				date < 2008.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gaston_Thorn_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
		# Colette Flesch
		LUX_HoG_Colette_Flesch = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1984.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Colette_Flesch_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
		# Charles Goerens
		LUX_HoG_Charles_Goerens = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1989.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Goerens_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
		# Michel Rasquin
		LUX_HoG_Michel_Rasquin_2 = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1958.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michel_Rasquin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
		# Michel Rasquin
		LUX_HoG_Michel_Rasquin = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1948.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michel_Rasquin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
		# Paul Wilwertz
		LUX_HoG_Paul_Wilwertz = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1951.1.1
				date < 1980.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Wilwertz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
		# Albert Bousser
		LUX_HoG_Albert_Bousser = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1954.1.1
				date < 1996.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Albert_Bousser_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
		# Pierre Frieden
		LUX_HoG_Pierre_Frieden = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1958.1.1
				date < 1959.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Frieden_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
		# Pierre Werner
		LUX_HoG_Pierre_Werner = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1959.1.1
				date < 2003.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Werner_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
		# Jacques Santer
		LUX_HoG_Jacques_Santer = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1974.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacques_Santer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
		# René Blum
		LUX_HoG_Rene_Blum = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rene_Blum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
		# Martin Karp
		LUX_HoG_Martin_Karp = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Martin_Karp_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Corporate_Suit }
		}
		# Paul Medinger
		LUX_HoG_Paul_Medinger = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Medinger_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Ambitious_Union_Boss }
		}
		# Charlotte
		LUX_HoG_Charlotte = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1932.1.1
				date < 1964.11.12
				NOT = { has_country_flag = Charlotte_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A }
		}
	}
	#################################################
	### Foreign Minister
	#################################################
	Foreign_Minister = {
		# Damian Kratzenberg
		LUX_FM_Damian_Kratzenberg = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Damian_Kratzenberg_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_Biased_Intellectual }
		}
		# Henri Ahnen
		LUX_FM_Henri_Ahnen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Ahnen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Iron_Fisted_Brute }
		}
		# Joseph Bech
		LUX_FM_Joseph_Bech = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Bech_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
		# Paul Eyschen
		LUX_FM_Paul_Eyschen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Eyschen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_General_Staffer }
		}
		# Hubert Loutsch
		LUX_FM_Hubert_Loutsch = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hubert_Loutsch_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_General_Staffer }
		}
		# Léon Kauffman
		LUX_FM_Leon_Kauffman = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1917.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leon_Kauffman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
		# Émile Reuter
		LUX_FM_Emile_Reuter = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Emile_Reuter_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
		# Pierre Krier
		LUX_FM_Pierre_Krier = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Krier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
		# René Blum
		LUX_FM_Rene_Blum = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rene_Blum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
		# Francois Schammel
		LUX_FM_Francois_Schammel = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Francois_Schammel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_General_Staffer }
		}
		# Paul Medinger
		LUX_FM_Paul_Medinger = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Medinger_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Biased_Intellectual }
		}
	}
	#################################################
	### Minister of Security
	#################################################
	Minister_of_Security = {
		# Henri Ahnen
		LUX_MoS_Henri_Ahnen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Ahnen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Compassionate_Gentleman }
		}
		# Max Simon
		LUX_MoS_Max_Simon = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Max_Simon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Prince_Of_Terror }
		}
		# Norbert Dumont
		LUX_MoS_Norbert_Dumont = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Norbert_Dumont_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
		# Nicolas Braunshausen
		LUX_MoS_Nicolas_Braunshausen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nicolas_Braunshausen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
		# Etienne Schmit
		LUX_MoS_Etienne_Schmit = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Etienne_Schmit_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
		# Joseph Bech
		LUX_MoS_Joseph_Bech = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Bech_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
		# Robert Als
		LUX_MoS_Robert_Als = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_Als_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
		# Victor Bodson
		LUX_MoS_Victor_Bodson = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Bodson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
		# Pierre Braun
		LUX_MoS_Pierre_Braun = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Braun_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
		# Ernest Leclere
		LUX_MoS_Ernest_Leclere = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ernest_Leclere_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
		# Jean-Baptiste Sax
		LUX_MoS_JeanBaptiste_Sax = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JeanBaptiste_Sax_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
		# Léon Moutrier
		LUX_MoS_Leon_Moutrier = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leon_Moutrier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
		# Maurice Kohn
		LUX_MoS_Maurice_Kohn = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1917.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Maurice_Kohn_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
		# Émile Reuter
		LUX_MoS_Emile_Reuter = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Emile_Reuter_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
		# Joseph Bech
		LUX_MoS_Joseph_Bech_2 = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1921.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Bech_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
		# Eugene Schaus
		LUX_MoS_Eugene_Schaus = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Eugene_Schaus_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
		# René Blum
		LUX_MoS_Rene_Blum = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rene_Blum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
		# Joseph Goedert
		LUX_MoS_Joseph_Goedert = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Goedert_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
		# Michel Hansen
		LUX_MoS_Michel_Hansen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Michel_Hansen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Efficient_Sociopath }
		}
		# Jean Pierre Kauder
		LUX_MoS_Jean_Pierre_Kauder = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Pierre_Kauder_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Man_Of_The_People }
		}
	}
	#################################################
	### Armaments Minister
	#################################################
	Armaments_Minister = {
		# Gustave Farver
		LUX_AM_Gustave_Farver = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Gustave_Farver_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Tank_Proponent }
		}
		# Philip Hoffmann
		LUX_AM_Philip_Hoffmann = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Philip_Hoffmann_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Theoretical_Scientist }
		}
		# Pierre Dupong
		LUX_AM_Pierre_Dupong = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Dupong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
		# Jules Keiffer
		LUX_AM_Jules_Keiffer = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jules_Keiffer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
		# Auguste Oster
		LUX_AM_Auguste_Oster = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Auguste_Oster_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Administrative_Genius }
		}
		# Mathias Mongenast
		LUX_AM_Mathias_Mongenast = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mathias_Mongenast_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Administrative_Genius }
		}
		# Edmond Reiffers
		LUX_AM_Edmond_Reiffers = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edmond_Reiffers_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
		# Léon Kauffman
		LUX_AM_Leon_Kauffman = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leon_Kauffman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Infantry_Proponent }
		}
		# Alphonse Neyens
		LUX_AM_Alphonse_Neyens = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alphonse_Neyens_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Administrative_Genius }
		}
		# Pierre Krier
		LUX_AM_Pierre_Krier = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Krier_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
		# Alex Federspiel
		LUX_AM_Alex_Federspiel = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alex_Federspiel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Infantry_Proponent }
		}
		# Jean Pierre Kauder
		LUX_AM_Jean_Pierre_Kauder = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Pierre_Kauder_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Military_Entrepreneur }
		}
		# Martin Karp
		LUX_AM_Martin_Karp = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Martin_Karp_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Infantry_Proponent }
		}
	}
	#################################################
	### Head of Intelligence
	#################################################
	Head_of_Intelligence = {
		# Guillaume Soisson
		LUX_HoI_Guillaume_Soisson = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Guillaume_Soisson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Logistics_Specialist }
		}
		# Max Simon
		LUX_HoI_Max_Simon = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Max_Simon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Political_Specialist }
		}
		# Jules Prussen
		LUX_HoI_Jules_Prussen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jules_Prussen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Technical_Specialist }
		}
		# Nicholas Marque
		LUX_HoI_Nicholas_Marque = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nicholas_Marque_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
		# Edmond Klein
		LUX_HoI_Edmond_Klein = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edmond_Klein_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
		# Joseph Wohlfart
		LUX_HoI_Joseph_Wohlfart = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Wohlfart_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
		# Joseph Tockert
		LUX_HoI_Joseph_Tockert = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Tockert_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Technical_Specialist }
		}
		# Francois Schammel
		LUX_HoI_Francois_Schammel = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francois_Schammel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
		# Charles Becker
		LUX_HoI_Charles_Becker = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Becker_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
		# Max Weckering
		LUX_HoI_Max_Weckering = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Max_Weckering_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Dismal_Enigma }
		}
		# Jean Pierre Kauder
		LUX_HoI_Jean_Pierre_Kauder = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Pierre_Kauder_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Industrial_Specialist }
		}
		# Jaques Tillmany
		LUX_HoI_Jaques_Tillmany = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Jaques_Tillmany_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Political_Specialist }
		}
	}
	#################################################
	### Chief of Staff
	#################################################
	Chief_of_Staff = {
		# Aloyse Glodt
		LUX_CoStaff_Aloyse_Glodt = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Aloyse_Glodt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Fire_Support }
		}
		# Pierre Dupong
		LUX_CoStaff_Pierre_Dupong = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Dupong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
		# Jean Seurlis
		LUX_CoStaff_Jean_Seurlis = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Seurlis_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
		# Alex Federspiel
		LUX_CoStaff_Alex_Federspiel = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alex_Federspiel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
		# Francois Schammel
		LUX_CoStaff_Francois_Schammel = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francois_Schammel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
		# Martin Karp
		LUX_CoStaff_Martin_Karp = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Martin_Karp_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Mass_Combat }
		}
	}
	#################################################
	### Chief of Army
	#################################################
	Chief_of_Army = {
		# Aloyse Glodt
		LUX_CoArmy_Aloyse_Glodt = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Aloyse_Glodt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Armoured_Spearhead_Doctrine }
		}
		# Alexander von Falkenhausen
		LUX_CoArmy_Alexander_von_Falkenhausen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Alexander_von_Falkenhausen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Decisive_Battle_Doctrine }
		}
		# Emile Speller
		LUX_CoArmy_Emile_Speller = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Emile_Speller_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
		# Felix Bourbon-Parma
		LUX_CoArmy_Felix_BourbonParma = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Felix_BourbonParma_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
		# Martin Karp
		LUX_CoArmy_Martin_Karp = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Martin_Karp_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Elastic_Defence_Doctrine }
		}
	}
	#################################################
	### Chief of Navy
	#################################################
	Chief_of_Navy = {
		# Henri Ahnen
		LUX_CoNavy_Henri_Ahnen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_Ahnen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoNavy_Indirect_Approach_Doctrine }
		}
		# Emile Speller
		LUX_CoNavy_Emile_Speller = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Emile_Speller_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
		# Pierre Fischbach
		LUX_CoNavy_Pierre_Fischbach = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Fischbach_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
		# Paul Medinger
		LUX_CoNavy_Paul_Medinger = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Medinger_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Base_Control_Doctrine }
		}
	}
	#################################################
	### Chief of Airforce
	#################################################
	Chief_of_Airforce = {
		# Guillaume Soisson
		LUX_CoAir_Guillaume_Soisson = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Guillaume_Soisson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoAir_Air_Superiority_Doctrine }
		}
		# Nicholas Diedrich
		LUX_CoAir_Nicholas_Diedrich = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nicholas_Diedrich_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
		# Jean Marc Steffen
		LUX_CoAir_Jean_Marc_Steffen = {
			picture = Generic_Portrait
			allowed = { tag = LUX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Marc_Steffen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}