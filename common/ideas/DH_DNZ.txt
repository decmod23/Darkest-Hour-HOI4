ideas = {
	#########################################################################
	#  National Spirits
	#########################################################################
	country = {
		#### Free City of Danzig
		DNZ_Free_City_of_Danzig = {
			picture = DNZ_Free_City_of_Danzig
			allowed = {
				original_tag = DNZ
			}
			available = {
				has_war = no
			}
			modifier = {
				drift_defence_factor = 0.5
				enemy_justify_war_goal_time = 0.5
				trade_opinion_factor = 0.5
			}
		}
	}
}