ideas = {
#################################################
### Head of Government             Shanxi     ### 
#################################################
	Head_of_Government = {
	# Yan Xishan
		CSX_HoG_Yan_Xishan = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Yan_Xishan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Old_General }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Wang Zuanxu
		CSX_FM_Wang_Zuanxu = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wang_Zuanxu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Iron_Fisted_Brute }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Qu Yangke
		CSX_MoS_Qu_Yangke = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Qu_Yangke_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Fu Zuoyi
		CSX_AM_Fu_Zuoyi = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Fu_Zuoyi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Air_To_Ground_Proponent }
		}
	# Yan Xishan
		CSX_AM_Yan_Xishan = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Yan_Xishan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Keynesian_Economy }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Shang Zhen
		CSX_HoI_Shang_Zhen = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Shang_Zhen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Naval_Intelligence_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Yan Xishan
		CSX_CoStaff_Yan_Xishan = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Yan_Xishan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Defence }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Yan Xishan
		CSX_CoArmy_Yan_Xishan = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Yan_Xishan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Doctrine_Of_Autonomy }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Dmitrij Krolcheff
		CSX_CoNavy_Dmitrij_Krolcheff = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Dmitrij_Krolcheff_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Base_Control_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Yan Xishan
		CSX_CoAir_Yan_Xishan = {
			picture = Generic_Portrait
			allowed = { tag = SHX }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Yan_Xishan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Carpet_Bombing_Doctrine }
		}
	}
}
