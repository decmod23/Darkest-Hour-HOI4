ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# A.E. Davy
		NZL_HoG_AE_Davy = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = AE_Davy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# Adam Hamilton
		NZL_HoG_Adam_Hamilton = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Adam_Hamilton_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Sydney Holland
		NZL_HoG_Sydney_Holland = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sydney_Holland_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Keith J. Holyoake
		NZL_HoG_Keith_J_Holyoake = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1957.1.1
				date < 1984.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Keith_J_Holyoake_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Jack Marshall
		NZL_HoG_Jack_Marshall = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1972.1.1
				date < 1989.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jack_Marshall_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Robert Muldoon
		NZL_HoG_Robert_Muldoon = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1975.1.1
				date < 1993.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_Muldoon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Jim Bolger
		NZL_HoG_Jim_Bolger = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1987.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jim_Bolger_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Thomas Mackenzie
		NZL_HoG_Thomas_Mackenzie = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Thomas_Mackenzie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Arnold H. Nordmeyer
		NZL_HoG_Arnold_H_Nordmeyer = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1963.1.1
				date < 1990.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arnold_H_Nordmeyer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Norman Kirk
		NZL_HoG_Norman_Kirk = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1966.1.1
				date < 1975.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Norman_Kirk_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Bill Rowling
		NZL_HoG_Bill_Rowling = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1974.1.1
				date < 1996.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bill_Rowling_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# David Lange
		NZL_HoG_David_Lange = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1984.1.1
				date < 2006.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = David_Lange_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Geoffrey Palmer
		NZL_HoG_Geoffrey_Palmer = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1989.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Geoffrey_Palmer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Mike Moore
		NZL_HoG_Mike_Moore = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1990.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mike_Moore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Michael Joseph Savage
		NZL_HoG_Michael_Joseph_Savage = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michael_Joseph_Savage_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Peter Fraser
		NZL_HoG_Peter_Fraser = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Peter_Fraser_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Walter Nash
		NZL_HoG_Walter_Nash = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Walter_Nash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Victor Wilcox
		NZL_HoG_Victor_Wilcox = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Wilcox_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Happy_Amateur }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# A.E. Davy
		NZL_FM_AE_Davy = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = AE_Davy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Ideological_Crusader }
		}
	# Adam Hamilton
		NZL_FM_Adam_Hamilton = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Adam_Hamilton_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# William Sinclair-Burgess
		NZL_FM_William_SinclairBurgess = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = William_SinclairBurgess_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_General_Staffer }
		}
	# Gordon Coates
		NZL_FM_Gordon_Coates = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gordon_Coates_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
	# Sydney Holland
		NZL_FM_Sydney_Holland = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sydney_Holland_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Frederick W. Doidge
		NZL_FM_Frederick_W_Doidge = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Frederick_W_Doidge_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Biased_Intellectual }
		}
	# Thomas C. Webb
		NZL_FM_Thomas_C_Webb = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Thomas_C_Webb_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# James Allen
		NZL_FM_James_Allen = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = James_Allen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Thomas Mackenzie
		NZL_FM_Thomas_Mackenzie = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Thomas_Mackenzie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Michael Joseph Savage
		NZL_FM_Michael_Joseph_Savage = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michael_Joseph_Savage_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Frank Langstone
		NZL_FM_Frank_Langstone = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Frank_Langstone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Peter Fraser
		NZL_FM_Peter_Fraser = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Peter_Fraser_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Biased_Intellectual }
		}
	# Walter Nash
		NZL_FM_Walter_Nash = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Walter_Nash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Arnold H. Nordmeyer
		NZL_FM_Arnold_H_Nordmeyer = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1950.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arnold_H_Nordmeyer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
	# Victor Wilcox
		NZL_FM_Victor_Wilcox = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Wilcox_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Biased_Intellectual }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# W.A. Veitch-Milne
		NZL_MoS_WA_VeitchMilne = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = WA_VeitchMilne_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Prince_Of_Terror }
		}
	# Francis Bell
		NZL_MoS_Francis_Bell = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francis_Bell_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Gordon Coates
		NZL_MoS_Gordon_Coates = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gordon_Coates_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Adam Hamilton
		NZL_MoS_Adam_Hamilton = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Adam_Hamilton_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Sydney Holland
		NZL_MoS_Sydney_Holland = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sydney_Holland_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Kenneth Folkes
		NZL_MoS_Kenneth_Folkes = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Kenneth_Folkes_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Thomas L. MacDonald
		NZL_MoS_Thomas_L_MacDonald = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Thomas_L_MacDonald_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# James Allen
		NZL_MoS_James_Allen = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = James_Allen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# E.R. Drummond
		NZL_MoS_ER_Drummond = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = ER_Drummond_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# John Findlay
		NZL_MoS_John_Findlay = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = John_Findlay_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# William Parry
		NZL_MoS_William_Parry = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = William_Parry_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Edward Blundell
		NZL_MoS_Edward_Blundell = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edward_Blundell_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Robert Semple
		NZL_MoS_Robert_Semple = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_Semple_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Gordon Watson
		NZL_MoS_Gordon_Watson = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Gordon_Watson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Efficient_Sociopath }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# A.J. Stallworthy
		NZL_AM_AJ_Stallworthy = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = AJ_Stallworthy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Air_To_Sea_Proponent }
		}
	# R.A. Cochrane
		NZL_AM_RA_Cochrane = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = RA_Cochrane_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_Superiority_Proponent }
		}
	# Sydney Holland
		NZL_AM_Sydney_Holland = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sydney_Holland_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# A. de Terrotte Neville
		NZL_AM_A_de_Terrotte_Neville = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = A_de_Terrotte_Neville_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Strategic_Air_Proponent }
		}
	# W.H. Stratton
		NZL_AM_WH_Stratton = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = WH_Stratton_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_To_Ground_Proponent }
		}
	# Sir Frederick Field
		NZL_AM_Sir_Frederick_Field = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Frederick_Field_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Battle_Fleet_Proponent }
		}
	# Ken MacKenzie
		NZL_AM_Ken_MacKenzie = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ken_MacKenzie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Theoretical_Scientist }
		}
	# Edward Puttick
		NZL_AM_Edward_Puttick = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edward_Puttick_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Infantry_Proponent }
		}
	# J.E. Duigan
		NZL_AM_JE_Duigan = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JE_Duigan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# W.E. Parry
		NZL_AM_WE_Parry = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = WE_Parry_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Submarine_Proponent }
		}
	# Arthur Myers
		NZL_AM_Arthur_Myers = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arthur_Myers_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Walter Nash
		NZL_AM_Walter_Nash = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Walter_Nash_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Dan Sullivan
		NZL_AM_Dan_Sullivan = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Dan_Sullivan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# William J. Jordan
		NZL_AM_William_J_Jordan = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = William_J_Jordan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# L.M. Isitt
		NZL_AM_LM_Isitt = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = LM_Isitt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_To_Sea_Proponent }
		}
	# Keith J. Holyoake
		NZL_AM_Keith_J_Holyoake = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Keith_J_Holyoake_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Sid Scott
		NZL_AM_Sid_Scott = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Sid_Scott_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Theoretical_Scientist }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Arthur Dawes
		NZL_HoI_Arthur_Dawes = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Arthur_Dawes_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Naval_Intelligence_Specialist }
		}
	# Adam Hamilton
		NZL_HoI_Adam_Hamilton = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Adam_Hamilton_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Ken MacKenzie
		NZL_HoI_Ken_MacKenzie = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ken_MacKenzie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# E.R. Drummond
		NZL_HoI_ER_Drummond = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = ER_Drummond_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Naval_Intelligence_Specialist }
		}
	# J.E. Duigan
		NZL_HoI_JE_Duigan = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JE_Duigan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Technical_Specialist }
		}
	# Arthur Myers
		NZL_HoI_Arthur_Myers = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arthur_Myers_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Kenneth Folkes
		NZL_HoI_Kenneth_Folkes = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Kenneth_Folkes_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Hugo Philpott
		NZL_HoI_Hugo_Philpott = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hugo_Philpott_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Tom Stanley
		NZL_HoI_Tom_Stanley = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tom_Stanley_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Frederick Freeman
		NZL_HoI_Frederick_Freeman = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Frederick_Freeman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Technical_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Seymour DeWitt
		NZL_CoStaff_Seymour_DeWitt = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Seymour_DeWitt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Manoeuvre }
		}
	# William Sinclair-Burgess
		NZL_CoStaff_William_SinclairBurgess = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = William_SinclairBurgess_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# J.E. Duigan
		NZL_CoStaff_JE_Duigan = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JE_Duigan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Ronald Park
		NZL_CoStaff_Ronald_Park = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ronald_Park_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Edward Puttick
		NZL_CoStaff_Edward_Puttick = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edward_Puttick_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# William Stevens
		NZL_CoStaff_William_Stevens = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = William_Stevens_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# N. Weir
		NZL_CoStaff_N_Weir = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = N_Weir_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Arthur Myers
		NZL_CoStaff_Arthur_Myers = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arthur_Myers_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Henry Colthorpe
		NZL_CoStaff_Henry_Colthorpe = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_Colthorpe_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Psychology }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Seymour DeWitt
		NZL_CoArmy_Seymour_DeWitt = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Seymour_DeWitt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Elastic_Defence_Doctrine }
		}
	# William Malone
		NZL_CoArmy_William_Malone = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = William_Malone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# William Sinclair-Burgess
		NZL_CoArmy_William_SinclairBurgess = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = William_SinclairBurgess_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Norris Stephen Falla
		NZL_CoArmy_Norris_Stephen_Falla = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Norris_Stephen_Falla_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# J.E. Duigan
		NZL_CoArmy_JE_Duigan = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JE_Duigan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# J.C. Nichols
		NZL_CoArmy_JC_Nichols = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JC_Nichols_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# Edward Puttick
		NZL_CoArmy_Edward_Puttick = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edward_Puttick_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# N. Weir
		NZL_CoArmy_N_Weir = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = N_Weir_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Gordon Watson
		NZL_CoArmy_Gordon_Watson = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Gordon_Watson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Trevor Handley
		NZL_CoNavy_Trevor_Handley = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Trevor_Handley_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Base_Control_Doctrine }
		}
	# Percival Hall-Thompson
		NZL_CoNavy_Percival_HallThompson = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Percival_HallThompson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# E.R. Drummond
		NZL_CoNavy_ER_Drummond = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = ER_Drummond_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# H.E. Horan
		NZL_CoNavy_HE_Horan = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HE_Horan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# W.E. Parry
		NZL_CoNavy_WE_Parry = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = WE_Parry_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Sir Frederick Field
		NZL_CoNavy_Sir_Frederick_Field = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Frederick_Field_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# W.H.Stratton
		NZL_CoNavy_WHStratton = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = WHStratton_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# G.W.G. Simpson
		NZL_CoNavy_GWG_Simpson = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = GWG_Simpson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Eric Prendergast
		NZL_CoNavy_Eric_Prendergast = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Eric_Prendergast_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Decisive_Naval_Battle_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Margaret Whittington
		NZL_CoAir_Margaret_Whittington = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Margaret_Whittington_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Vertical_Envelopment_Doctrine }
		}
	# R.A. Cochrane
		NZL_CoAir_RA_Cochrane = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = RA_Cochrane_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# W.H. Stratton
		NZL_CoAir_WH_Stratton = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = WH_Stratton_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# H.D. McGregor
		NZL_CoAir_HD_McGregor = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HD_McGregor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# L.M. Isitt
		NZL_CoAir_LM_Isitt = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = LM_Isitt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Naval_Aviation_Doctrine }
		}
	# Gordon H. Steege
		NZL_CoAir_Gordon_H_Steege = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gordon_H_Steege_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# A. de Terrotte Neville
		NZL_CoAir_A_de_Terrotte_Neville = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = A_de_Terrotte_Neville_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# L.P. Peterson
		NZL_CoAir_LP_Peterson = {
			picture = Generic_Portrait
			allowed = { tag = NZL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = LP_Peterson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Air_Superiority_Doctrine }
		}
	}
}
