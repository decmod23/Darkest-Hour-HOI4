ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Zaid al-Rifai
		JOR_HoG_Zaid_alRifai = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1973.1.1
				date < 1992.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Zaid_alRifai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Political_Protege }
		}
	# Sherif Husayn á Hijaz
		JOR_HoG_Sherif_Husayn_a_Hijaz = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sherif_Husayn_a_Hijaz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Smiling_Oilman }
		}
	# Tawfiq Abul-Huda
		JOR_HoG_Tawfiq_AbulHuda = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tawfiq_AbulHuda_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Ibrahim Hashim
		JOR_HoG_Ibrahim_Hashim = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ibrahim_Hashim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Samir al-Rifai
		JOR_HoG_Samir_alRifai = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Samir_alRifai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Said al-Mufti
		JOR_HoG_Said_alMufti = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1950.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Said_alMufti_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Fawzi al-Mulqi
		JOR_HoG_Fawzi_alMulqi = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Fawzi_alMulqi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Ahmad al-Lawzi
		JOR_HoG_Ahmad_alLawzi = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1971.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmad_alLawzi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Mudar Badran
		JOR_HoG_Mudar_Badran = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1976.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mudar_Badran_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Ahmad Obeidat
		JOR_HoG_Ahmad_Obeidat = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmad_Obeidat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Zaid ibn Shaker
		JOR_HoG_Zaid_ibn_Shaker = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1989.1.1
				date < 2003.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Zaid_ibn_Shaker_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Hazza' al-Majali 
		JOR_HoG_Hazza_alMajali_ = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1959.1.1
				date < 1961.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hazza_alMajali__unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Bahjat al-Talhouni
		JOR_HoG_Bahjat_alTalhouni = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1960.1.1
				date < 1995.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bahjat_alTalhouni_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Wasfi al-Tal
		JOR_HoG_Wasfi_alTal = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1962.1.1
				date < 1972.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Wasfi_alTal_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Admiral }
		}
	# Hussein ibn Nasser
		JOR_HoG_Hussein_ibn_Nasser = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1963.1.1
				date < 1971.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hussein_ibn_Nasser_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Admiral }
		}
	# Ismael Bey Gaspirali
		JOR_HoG_Ismael_Bey_Gaspirali = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ismael_Bey_Gaspirali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Ibrahim Pasha Hasheem
		JOR_FM_Ibrahim_Pasha_Hasheem = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ibrahim_Pasha_Hasheem_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Biased_Intellectual }
		}
	# Awn Shawkat Al-Khasawneh
		JOR_FM_Awn_Shawkat_AlKhasawneh = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Awn_Shawkat_AlKhasawneh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Samir al-Rifai
		JOR_FM_Samir_alRifai = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Samir_alRifai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Said al-Mufti
		JOR_FM_Said_alMufti = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Said_alMufti_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Bahjat Talhouni
		JOR_MoS_Bahjat_Talhouni = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Bahjat_Talhouni_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Silent_Lawyer }
		}
	# Shaker ben Zaid
		JOR_MoS_Shaker_ben_Zaid = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Shaker_ben_Zaid_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Sir Henry Cox
		JOR_MoS_Sir_Henry_Cox = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Henry_Cox_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Alec Seath Kirkbride
		JOR_MoS_Alec_Seath_Kirkbride = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alec_Seath_Kirkbride_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# Awni Abdul Hadi
		JOR_MoS_Awni_Abdul_Hadi = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Awni_Abdul_Hadi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Jacob Dorval-Penney
		JOR_MoS_Jacob_DorvalPenney = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacob_DorvalPenney_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Mohammed al-Shureiki
		JOR_MoS_Mohammed_alShureiki = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_alShureiki_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# HM Hussein bin Talal
		JOR_AM_HM_Hussein_bin_Talal = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hussein_bin_Talal_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Military_Entrepreneur }
		}
	# Suleiman al-Nabulshi
		JOR_AM_Suleiman_alNabulshi = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Suleiman_alNabulshi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Infantry_Proponent }
		}
	# Omar Pasha Mattar
		JOR_AM_Omar_Pasha_Mattar = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Omar_Pasha_Mattar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Ahmad Qadri
		JOR_AM_Ahmad_Qadri = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmad_Qadri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# HM Hussein bin Talal
		JOR_HoI_HM_Hussein_bin_Talal = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Hussein_bin_Talal_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Political_Specialist }
		}
	# Shadi al-Hakim
		JOR_HoI_Shadi_alHakim = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Shadi_alHakim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Industrial_Specialist }
		}
	# Sir Percy Cox
		JOR_HoI_Sir_Percy_Cox = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Percy_Cox_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# John Bagot Glubb
		JOR_HoI_John_Bagot_Glubb = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = John_Bagot_Glubb_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Rustum Haidar
		JOR_HoI_Rustum_Haidar = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rustum_Haidar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Zaid ibn Shaker
		JOR_CoStaff_Zaid_ibn_Shaker = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 2003.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Zaid_ibn_Shaker_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Psychology }
		}
	# Yoshua Abdel Ahti
		JOR_CoStaff_Yoshua_Abdel_Ahti = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Yoshua_Abdel_Ahti_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Sir Percy Cox
		JOR_CoStaff_Sir_Percy_Cox = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Percy_Cox_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# John E. Osprey
		JOR_CoStaff_John_E_Osprey = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = John_E_Osprey_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Habis Al-Majali
		JOR_CoStaff_Habis_AlMajali = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1948.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Habis_AlMajali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Haddad al-Wali
		JOR_CoStaff_Haddad_alWali = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Haddad_alWali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Zaid ibn Shaker
		JOR_CoArmy_Zaid_ibn_Shaker = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 2003.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Zaid_ibn_Shaker_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Guns_And_Butter_Doctrine }
		}
	# Abdullah Mecit Pasha
		JOR_CoArmy_Abdullah_Mecit_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Abdullah_Mecit_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# H.M. Abdullah I
		JOR_CoArmy_HM_Abdullah_I = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HM_Abdullah_I_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# John Bagot Glubb
		JOR_CoArmy_John_Bagot_Glubb = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = John_Bagot_Glubb_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Habis Al-Majali
		JOR_CoArmy_Habis_AlMajali = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Habis_AlMajali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Nair al-Jafaar
		JOR_CoArmy_Nair_alJafaar = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nair_alJafaar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Zaid ibn Shaker
		JOR_CoNavy_Zaid_ibn_Shaker = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 2003.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Zaid_ibn_Shaker_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoNavy_Indirect_Approach_Doctrine }
		}
	# Hibah abd-el Adir
		JOR_CoNavy_Hibah_abdel_Adir = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hibah_abdel_Adir_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Indirect_Approach_Doctrine }
		}
	# C.G.T. Lloyd-Elford
		JOR_CoNavy_CGT_LloydElford = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = CGT_LloydElford_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Gadi abd-el Jabbar
		JOR_CoNavy_Gadi_abdel_Jabbar = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gadi_abdel_Jabbar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Ihsan Shurdum
		JOR_CoAir_Ihsan_Shurdum = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1984.1.1
				date < 1992.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Ihsan_Shurdum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoAir_Air_Superiority_Doctrine }
		}
	# Esmael al-Khaliq
		JOR_CoAir_Esmael_alKhaliq = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Esmael_alKhaliq_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Carpet_Bombing_Doctrine }
		}
	# Alec Seath Kirkbride
		JOR_CoAir_Alec_Seath_Kirkbride = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alec_Seath_Kirkbride_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Sami al-Afiz
		JOR_CoAir_Sami_alAfiz = {
			picture = Generic_Portrait
			allowed = { tag = JOR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sami_alAfiz_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
