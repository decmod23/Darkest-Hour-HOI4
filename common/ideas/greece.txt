ideas = {

	high_command = {

		GRE_aris_velouchiotis = {
			
			allowed = {
				original_tag = GRE
			}
			
			picture = generic_army_europe_1
			
			traits = { army_artillery_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	
		GRE_napoleon_zervas = {
			
			
			allowed = {
				original_tag = GRE
			}
			
			picture = generic_army_europe_2
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		GRE_nikos_beloyannis = {
			
			
			allowed = {
				original_tag = GRE
			}
			
			picture = generic_air_europe_2
			
			traits = { air_naval_strike_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		GRE_aristeidis_moraitinis = {
			
			
			allowed = {
				original_tag = GRE
			}
			
			picture = generic_air_europe_2
			
			traits = { air_bomber_interception_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}
}