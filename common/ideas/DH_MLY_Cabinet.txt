ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# H.H. Ibrahim IV of Kelantan
		MLY_HoG_HH_Ibrahim_IV_of_Kelantan = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = HH_Ibrahim_IV_of_Kelantan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Backroom_Backstabber }
		}
	# Abdul Razak Hussein
		MLY_HoG_Abdul_Razak_Hussein = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1970.1.1
				date < 1977.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdul_Razak_Hussein_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Hussein Onn
		MLY_HoG_Hussein_Onn = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1976.1.1
				date < 1991.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hussein_Onn_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Mahathir Mohamad
		MLY_HoG_Mahathir_Mohamad = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1981.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mahathir_Mohamad_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Sir Gerald W.R. Templer
		MLY_HoG_Sir_Gerald_WR_Templer = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Gerald_WR_Templer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Sir Edward Gent
		MLY_HoG_Sir_Edward_Gent = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1948.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Edward_Gent_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Sir Henry Gurney
		MLY_HoG_Sir_Henry_Gurney = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Henry_Gurney_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Sir Donald MacGillivray
		MLY_HoG_Sir_Donald_MacGillivray = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1954.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Donald_MacGillivray_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Tunku Abdul Rahman
		MLY_HoG_Tunku_Abdul_Rahman = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1957.1.1
				date < 1991.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tunku_Abdul_Rahman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# David S. Marshall
		MLY_HoG_David_S_Marshall = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = David_S_Marshall_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Hamza ul-Lail
		MLY_FM_Hamza_ulLail = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hamza_ulLail_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_The_Cloak_N_Dagger_Schemer }
		}
	# Sir Henry Goldsworthy Gurney
		MLY_FM_Sir_Henry_Goldsworthy_Gurney = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Henry_Goldsworthy_Gurney_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_General_Staffer }
		}
	# Tan Cheng Lock
		MLY_FM_Tan_Cheng_Lock = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tan_Cheng_Lock_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Tuanku Muhammed of Negri Sembilan
		MLY_MoS_Tuanku_Muhammed_of_Negri_Sembilan = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Tuanku_Muhammed_of_Negri_Sembilan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Efficient_Sociopath }
		}
	# Abdul Hamid Ahmad
		MLY_MoS_Abdul_Hamid_Ahmad = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Abdul_Hamid_Ahmad_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Silent_Lawyer }
		}
	# Charles Hicks
		MLY_MoS_Charles_Hicks = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Hicks_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Arthur E. Young
		MLY_MoS_Arthur_E_Young = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arthur_E_Young_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Lim Yew Hock
		MLY_MoS_Lim_Yew_Hock = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lim_Yew_Hock_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# H.R.H. Yahya ul-Naim
		MLY_AM_HRH_Yahya_ulNaim = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Yahya_ulNaim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Laissez-faire_Capitalist }
		}
	# Sir Hugh Stein Fraser
		MLY_AM_Sir_Hugh_Stein_Fraser = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Hugh_Stein_Fraser_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Thomas Ashton of Hyde
		MLY_AM_Thomas_Ashton_of_Hyde = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Thomas_Ashton_of_Hyde_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Abdul Rahman Shah
		MLY_AM_Abdul_Rahman_Shah = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdul_Rahman_Shah_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Abdul Manap bin Tolok
		MLY_HoI_Abdul_Manap_bin_Tolok = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Abdul_Manap_bin_Tolok_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Idrus bin Tunka of Tampin
		MLY_HoI_Idrus_bin_Tunka_of_Tampin = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Idrus_bin_Tunka_of_Tampin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Naval_Intelligence_Specialist }
		}
	# Sir Gerald E.J. Gent
		MLY_HoI_Sir_Gerald_EJ_Gent = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Gerald_EJ_Gent_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Technical_Specialist }
		}
	# Sir Gerald W.R. Templer
		MLY_HoI_Sir_Gerald_WR_Templer = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Gerald_WR_Templer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Muhammed Yang Besar
		MLY_HoI_Muhammed_Yang_Besar = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Muhammed_Yang_Besar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Lee Kuan Yew
		MLY_HoI_Lee_Kuan_Yew = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lee_Kuan_Yew_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Ibrahim al-Mansur
		MLY_CoStaff_Ibrahim_alMansur = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Ibrahim_alMansur_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Mass_Combat }
		}
	# Sir William Dobbie
		MLY_CoStaff_Sir_William_Dobbie = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_William_Dobbie_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Abdul Hamid Shah
		MLY_CoStaff_Abdul_Hamid_Shah = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abdul_Hamid_Shah_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Tun Abdul Rahman
		MLY_CoStaff_Tun_Abdul_Rahman = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tun_Abdul_Rahman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Harun Putra of Perlis
		MLY_CoArmy_Harun_Putra_of_Perlis = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Harun_Putra_of_Perlis_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Arthur Percival
		MLY_CoArmy_Arthur_Percival = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Arthur_Percival_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# Robert Brooke-Popham
		MLY_CoArmy_Robert_BrookePopham = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_BrookePopham_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Sir Gerald W.R. Templer
		MLY_CoArmy_Sir_Gerald_WR_Templer = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Gerald_WR_Templer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Muhib Sultan Badlishah
		MLY_CoArmy_Muhib_Sultan_Badlishah = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Muhib_Sultan_Badlishah_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Syed ul-Lail of Perlis
		MLY_CoNavy_Syed_ulLail_of_Perlis = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Syed_ulLail_of_Perlis_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Open_Seas_Doctrine }
		}
	# John Atcherley
		MLY_CoNavy_John_Atcherley = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = John_Atcherley_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Mohammed Rahman Ali
		MLY_CoNavy_Mohammed_Rahman_Ali = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_Rahman_Ali_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Hisam II of Selangor
		MLY_CoAir_Hisam_II_of_Selangor = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hisam_II_of_Selangor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Naval_Aviation_Doctrine }
		}
	# G.T. Jarman
		MLY_CoAir_GT_Jarman = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = GT_Jarman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# Wan Suleiman
		MLY_CoAir_Wan_Suleiman = {
			picture = Generic_Portrait
			allowed = { tag = MLY }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Wan_Suleiman_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	}
}
