ideas = {
	high_command = {

		PER_mir_afkhami = {
			
			picture = generic_army_arab_1
			
			allowed = {
				original_tag = PER
			}
			
			traits = { army_regrouping_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		PER_fazlollah_zahedi = {

			picture = generic_army_arab_2
			
			allowed = {
				original_tag = PER
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}


		PER_ali_razmara = {

			picture = generic_air_arab_3
			
			allowed = {
				original_tag = PER
			}
			
			traits = { air_airborne_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		PER_ahmad_mira_khosravani = {

			picture = generic_navy_arab_1
			
			allowed = {
				original_tag = PER
			}
			
			traits = { navy_submarine_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}	

}