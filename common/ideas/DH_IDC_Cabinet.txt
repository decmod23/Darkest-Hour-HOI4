ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Phetsarath Mahaouphatat
		IDC_HoG_Phetsarath_Mahaouphatat = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Phetsarath_Mahaouphatat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Old_General }
		}
	# Ngo Dinh Diem
		IDC_HoG_Ngo_Dinh_Diem_2 = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Ngo_Dinh_Diem_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Flamboyant_Tough_Guy }
		}
	# Sisowath Sirik Matak
		IDC_HoG_Sisowath_Sirik_Matak = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Sirik_Matak_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Naive_Optimist }
		}
	# H.R.H. Phaya Khammao
		IDC_HoG_HRH_Phaya_Khammao = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Phaya_Khammao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Son Sann
		IDC_HoG_Son_Sann = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Son_Sann_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Ngo Dinh Diem
		IDC_HoG_Ngo_Dinh_Diem = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ngo_Dinh_Diem_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# H.R.H. Norodom Sihanouk
		IDC_HoG_HRH_Norodom_Sihanouk = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Norodom_Sihanouk_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_Admiral }
		}
	# Khambulatom Oomau
		IDC_HoG_Khambulatom_Oomau = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Khambulatom_Oomau_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Happy_Amateur }
		}
	# Truong Tchin
		IDC_HoG_Truong_Tchin = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Truong_Tchin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Backroom_Backstabber }
		}
	# Khieu Samphan
		IDC_HoG_Khieu_Samphan = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Khieu_Samphan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Smiling_Oilman }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# H.R.H. Boun Oum
		IDC_FM_HRH_Boun_Oum = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Boun_Oum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_Iron_Fisted_Brute }
		}
	# Nguyen Van Xuan
		IDC_FM_Nguyen_Van_Xuan = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Nguyen_Van_Xuan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_Ideological_Crusader }
		}
	# Krisan Cheda
		IDC_FM_Krisan_Cheda = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Krisan_Cheda_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_General_Staffer }
		}
	# Sasorith Kataydon
		IDC_FM_Sasorith_Kataydon = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sasorith_Kataydon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
	# Phan Sina
		IDC_FM_Phan_Sina = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Phan_Sina_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Biased_Intellectual }
		}
	# Tong Doc Phuong
		IDC_FM_Tong_Doc_Phuong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tong_Doc_Phuong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Sisowath Monipong
		IDC_FM_Sisowath_Monipong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Monipong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Phomvihanh Kaysone
		IDC_FM_Phomvihanh_Kaysone = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Phomvihanh_Kaysone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Iron_Fisted_Brute }
		}
	# Pham Van Dong
		IDC_FM_Pham_Van_Dong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Pham_Van_Dong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_Ideological_Crusader }
		}
	# Leng Sary
		IDC_FM_Leng_Sary = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Leng_Sary_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C FM_The_Cloak_N_Dagger_Schemer }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Phetsarath Mahaouphatat
		IDC_MoS_Phetsarath_Mahaouphatat = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Phetsarath_Mahaouphatat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Prince_Of_Terror }
		}
	# Ngo Dinh Nhu
		IDC_MoS_Ngo_Dinh_Nhu = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Ngo_Dinh_Nhu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Silent_Lawyer }
		}
	# Sakiwat Phirun
		IDC_MoS_Sakiwat_Phirun = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sakiwat_Phirun_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Efficient_Sociopath }
		}
	# Aphay Khou
		IDC_MoS_Aphay_Khou = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aphay_Khou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crooked_Kleptocrat }
		}
	# Sisowath Monirak
		IDC_MoS_Sisowath_Monirak = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Monirak_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Ho Dac Trung
		IDC_MoS_Ho_Dac_Trung = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ho_Dac_Trung_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Sisowath Youtevong
		IDC_MoS_Sisowath_Youtevong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Youtevong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Phomvihanh Kaysone
		IDC_MoS_Phomvihanh_Kaysone = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Phomvihanh_Kaysone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Prince_Of_Terror }
		}
	# The Lu
		IDC_MoS_The_Lu = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = The_Lu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Silent_Lawyer }
		}
	# Pol Pot
		IDC_MoS_Pol_Pot = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Pol_Pot_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C MoS_Efficient_Sociopath }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# H.R.H. Souvanna Rath
		IDC_AM_HRH_Souvanna_Rath_2 = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souvanna_Rath_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Administrative_Genius }
		}
	# Le Van Hoac
		IDC_AM_Le_Van_Hoac = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Le_Van_Hoac_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Theoretical_Scientist }
		}
	# Norodom Phanouvong
		IDC_AM_Norodom_Phanouvong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Norodom_Phanouvong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Submarine_Proponent }
		}
	# H.R.H. Souvanna Rath
		IDC_AM_HRH_Souvanna_Rath = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souvanna_Rath_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Sam An
		IDC_AM_Sam_An = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sam_An_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Battle_Fleet_Proponent }
		}
	# Nguyen Van Tam
		IDC_AM_Nguyen_Van_Tam = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nguyen_Van_Tam_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Sisowath Monireth
		IDC_AM_Sisowath_Monireth = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Monireth_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# H.R.H. Boun Oum
		IDC_AM_HRH_Boun_Oum = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Boun_Oum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Infantry_Proponent }
		}
	# Le Duan
		IDC_AM_Le_Duan = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Le_Duan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Military_Entrepreneur }
		}
	# Nuon Chea
		IDC_AM_Nuon_Chea = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nuon_Chea_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C AM_Corrupt_Kleptocrat }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Phousannani Kone
		IDC_HoI_Phousannani_Kone = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Phousannani_Kone_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Naval_Intelligence_Specialist }
		}
	# Pham Toc Quia
		IDC_HoI_Pham_Toc_Quia = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Pham_Toc_Quia_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Industrial_Specialist }
		}
	# Sisowath Youtevong
		IDC_HoI_Sisowath_Youtevong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Youtevong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Technical_Specialist }
		}
	# Savang Vong
		IDC_HoI_Savang_Vong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Savang_Vong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Hy Weng
		IDC_HoI_Hy_Weng = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hy_Weng_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Tran Gia Thoai
		IDC_HoI_Tran_Gia_Thoai = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tran_Gia_Thoai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Naval_Intelligence_Specialist }
		}
	# Son Ngoc Thanh
		IDC_HoI_Son_Ngoc_Thanh = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Son_Ngoc_Thanh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Khambulatom Oomau
		IDC_HoI_Khambulatom_Oomau = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Khambulatom_Oomau_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Technical_Specialist }
		}
	# Phan Huy Quat
		IDC_HoI_Phan_Huy_Quat = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Phan_Huy_Quat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Political_Specialist }
		}
	# Tea Banh
		IDC_HoI_Tea_Banh = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Tea_Banh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoI_Dismal_Enigma }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Aphay Khou
		IDC_CoStaff_Aphay_Khou = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Aphay_Khou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Fire_Support }
		}
	# Nguyen Huu Coo
		IDC_CoStaff_Nguyen_Huu_Coo = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Nguyen_Huu_Coo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Manoeuvre }
		}
	# Sisowath Monireth
		IDC_CoStaff_Sisowath_Monireth = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Monireth_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# H.R.H. Souphanou Vong
		IDC_CoStaff_HRH_Souphanou_Vong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souphanou_Vong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Chhun Lim
		IDC_CoStaff_Chhun_Lim = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Chhun_Lim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Le Quan
		IDC_CoStaff_Le_Quan = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Le_Quan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Norodom Sihanouk
		IDC_CoStaff_Norodom_Sihanouk = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Norodom_Sihanouk_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Phichith Douangchai
		IDC_CoStaff_Phichith_Douangchai = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Phichith_Douangchai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Defence }
		}
	# Vo Nguyen Giap
		IDC_CoStaff_Vo_Nguyen_Giap = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vo_Nguyen_Giap_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Psychology }
		}
	# Kieng Vang
		IDC_CoStaff_Kieng_Vang = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Kieng_Vang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoStaff_School_Of_Mass_Combat }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Savang Vong
		IDC_CoArmy_Savang_Vong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Savang_Vong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Duong Van Minh
		IDC_CoArmy_Duong_Van_Minh_2 = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Duong_Van_Minh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Decisive_Battle_Doctrine }
		}
	# Lon Nol
		IDC_CoArmy_Lon_Nol = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Lon_Nol_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Phetsarath Mahaouphatat
		IDC_CoArmy_Phetsarath_Mahaouphatat = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Phetsarath_Mahaouphatat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Sisowath Watchayavong
		IDC_CoArmy_Sisowath_Watchayavong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Watchayavong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Duong Van Minh
		IDC_CoArmy_Duong_Van_Minh = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Duong_Van_Minh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Sisowath Monireth
		IDC_CoArmy_Sisowath_Monireth = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Monireth_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Vang Pao
		IDC_CoArmy_Vang_Pao = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Vang_Pao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Static_Defence_Doctrine }
		}
	# Ho Chi Minh
		IDC_CoArmy_Ho_Chi_Minh = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Ho_Chi_Minh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Elastic_Defence_Doctrine }
		}
	# Son Sen
		IDC_CoArmy_Son_Sen = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Son_Sen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Aphay Khou
		IDC_CoNavy_Aphay_Khou = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Aphay_Khou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoNavy_Indirect_Approach_Doctrine }
		}
	# Ho Tan Quyen
		IDC_CoNavy_Ho_Tan_Quyen = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Ho_Tan_Quyen_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Norodom Sihanouk
		IDC_CoNavy_Norodom_Sihanouk = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Norodom_Sihanouk_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Open_Seas_Doctrine }
		}
	# H.R.H. Souvanna Rath
		IDC_CoNavy_HRH_Souvanna_Rath = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souvanna_Rath_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# Hy Weng
		IDC_CoNavy_Hy_Weng = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hy_Weng_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Nguyen Van Thinh
		IDC_CoNavy_Nguyen_Van_Thinh = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nguyen_Van_Thinh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Lon Nol
		IDC_CoNavy_Lon_Nol = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lon_Nol_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# H.R.H. Souphanou Vong
		IDC_CoNavy_HRH_Souphanou_Vong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Souphanou_Vong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Open_Seas_Doctrine }
		}
	# Thrang Tun Doc
		IDC_CoNavy_Thrang_Tun_Doc = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Thrang_Tun_Doc_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Power_Projection_Doctrine }
		}
	# You Hok Kry
		IDC_CoNavy_You_Hok_Kry = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = You_Hok_Kry_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Base_Control_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Sourith Sasorith
		IDC_CoAir_Sourith_Sasorith = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Sourith_Sasorith_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoAir_Carpet_Bombing_Doctrine }
		}
	# Duong Van Minh
		IDC_CoAir_Duong_Van_Minh = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Duong_Van_Minh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoAir_Naval_Aviation_Doctrine }
		}
	# Norodom Sihanouk
		IDC_CoAir_Norodom_Sihanouk = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Norodom_Sihanouk_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# H.R.H. Boun Oum
		IDC_CoAir_HRH_Boun_Oum = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = HRH_Boun_Oum_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# Sisowath Youtevong
		IDC_CoAir_Sisowath_Youtevong = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Sisowath_Youtevong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# Nguyen Van Xuan
		IDC_CoAir_Nguyen_Van_Xuan = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nguyen_Van_Xuan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# Chai Saing Yun
		IDC_CoAir_Chai_Saing_Yun = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Chai_Saing_Yun_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Naval_Aviation_Doctrine }
		}
	# Vang Pao
		IDC_CoAir_Vang_Pao = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Vang_Pao_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Army_Aviation_Doctrine }
		}
	# Nguyen Huu Tho
		IDC_CoAir_Nguyen_Huu_Tho = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nguyen_Huu_Tho_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Air_Superiority_Doctrine }
		}
	# Proum Sokha
		IDC_CoAir_Proum_Sokha = {
			picture = Generic_Portrait
			allowed = { tag = VIN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Proum_Sokha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
