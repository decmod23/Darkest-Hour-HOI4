on_actions = {
	on_startup = {
		effect = {
			CHI = {
				# Populate array with factions
				clear_array = active_factions_array
				set_temp_variable = { faction_id = 1 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 2 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 3 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 4 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 5 }
				add_active_faction = yes
				# Set random loyalty and influence values
				set_temp_variable = { total_influence = 100 }
				for_each_loop = {
					array = active_factions_array
					value = v
					meta_effect = {
						text = {
							set_variable = { faction_loyalty_[FACTION] = random }
							multiply_variable = { faction_loyalty_[FACTION] = 100 }
							set_temp_variable = { influence = random }
							divide_temp_variable = { influence = 2 }
							multiply_temp_variable = { influence = 100 }
							if = {
								limit = {
									NOT = { check_variable = { total_influence < influence } }
								}
								set_variable = { faction_influence_[FACTION] = influence }
								subtract_from_temp_variable = { total_influence = influence }
							}
							else = {
								set_variable = { faction_influence_[FACTION] = total_influence }
								set_temp_variable = { total_influence = 0 }
							}
						}
						FACTION = "[?v]"
					}
				}
				# Calculate total support
				calculate_total_factions_support = yes
				# Set leading faction
				set_temp_variable = { faction_id = 3 }
				set_leading_faction = yes
				# Add correct not in power idea, if necessary
				for_each_loop = {
					array = active_factions_array
					value = v
					set_temp_variable = { faction_id = v }
					add_faction_not_in_power_idea = yes
				}
			}
			JAP = {
				# Populate array with factions
				clear_array = active_factions_array
				set_temp_variable = { faction_id = 6 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 7 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 8 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 9 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 10 }
				add_active_faction = yes
				# Set loyalty and influence values
				set_variable = { faction_loyalty_6 = 89 }
				set_variable = { faction_influence_6 = 21 }
				set_variable = { faction_loyalty_7 = 3 }
				set_variable = { faction_influence_7 = 46 }
				set_variable = { faction_loyalty_8 = 15 }
				set_variable = { faction_influence_8 = 18 }
				set_variable = { faction_loyalty_9 = 67 }
				set_variable = { faction_influence_9 = 10 }
				set_variable = { faction_loyalty_10 = 77 }
				set_variable = { faction_influence_10 = 5 }
				# Calculate total support
				calculate_total_factions_support = yes
				# Set leading faction
				set_temp_variable = { faction_id = 6 }
				set_leading_faction = yes
				# Add correct not in power idea, if necessary
				for_each_loop = {
					array = active_factions_array
					value = v
					set_temp_variable = { faction_id = v }
					add_faction_not_in_power_idea = yes
				}
			}
			GER = {
				# Populate array with factions
				clear_array = active_factions_array
				set_temp_variable = { faction_id = 11 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 12 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 13 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 14 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 15 }
				add_active_faction = yes
				set_temp_variable = { faction_id = 18 }
				add_active_faction = yes
				# Set random loyalty and influence values
				set_temp_variable = { total_influence = 100 }
				for_each_loop = {
					array = active_factions_array
					value = v
					meta_effect = {
						text = {
							set_variable = { faction_loyalty_[FACTION] = random }
							multiply_variable = { faction_loyalty_[FACTION] = 100 }
							set_temp_variable = { influence = random }
							divide_temp_variable = { influence = 2 }
							multiply_temp_variable = { influence = 100 }
							if = {
								limit = {
									NOT = { check_variable = { total_influence < influence } }
								}
								set_variable = { faction_influence_[FACTION] = influence }
								subtract_from_temp_variable = { total_influence = influence }
							}
							else = {
								set_variable = { faction_influence_[FACTION] = total_influence }
								set_temp_variable = { total_influence = 0 }
							}
						}
						FACTION = "[?v]"
					}
				}
				# Calculate total support
				calculate_total_factions_support = yes
				# Set leading faction
				set_temp_variable = { faction_id = 15 }
				set_leading_faction = yes
				# Add correct not in power idea, if necessary
				for_each_loop = {
					array = active_factions_array
					value = v
					set_temp_variable = { faction_id = v }
					add_faction_not_in_power_idea = yes
				}
			}
		}
	}
}