CHI_set_ally_strat_on_current_leader = {
	if = {
		limit = {
			FROM = {
				tag = CHI
			}
		}
		add_ai_strategy = {
            type = alliance
            id = "CHI"
            value = 200
        }
	}
	else_if = {
		limit = {
			FROM = {
				tag = SHX
			}
		}
		add_ai_strategy = {
            type = alliance
            id = "SHX"
            value = 200
        }
	}
	else_if = {
		limit = {
			FROM = {
				tag = GXC
			}
		}
		add_ai_strategy = {
            type = alliance
            id = "GXC"
            value = 200
        }
	}
	else_if = {
		limit = {
			FROM = {
				tag = YUN
			}
		}
		add_ai_strategy = {
            type = alliance
            id = "YUN"
            value = 200
        }
	}
	else_if = {
		limit = {
			FROM = {
				tag = XSM
			}
		}
		add_ai_strategy = {
            type = alliance
            id = "XSM"
            value = 200
        }
	}
	else_if = {
		limit = {
			FROM = {
				tag = SIK
			}
		}
		add_ai_strategy = {
            type = alliance
            id = "SIK"
            value = 200
        }
	}
}
#FOR Political Consultative Assembly
 add_war_support_add_commie_leader = {

 	add_war_support = 0.1
 	create_country_leader = {
 		name = "Mao Zedong"
 		desc = "Mao_Zedong_desc"
 		picture = "P_C_Mao_Zedong.tga"
 		expire = "1965.1.1"
 		ideology = maoism
 		traits = {
 		}
 	}
 }
 annex_from_change_party_name_boost_commie_popularity= {
	 annex_country = {
		 target = FROM
		 transfer_troops = yes
	 }
	 set_party_name = {
		 ideology = communist
		 name = PRC_communist_party
		 long_name = PRC_communist_party_long
	 }
	 add_popularity = {
		 ideology = communist
		 popularity = 0.35
	 }
}